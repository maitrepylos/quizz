== TEST POST HOOK - 12 ==
AST quiz - innovons.be
=
Created by PAPERPIXEL - www.paperpixel.net

1. Run `composer.phar install` in `App\`
2. Compile `public_html\less\front.less` and `public_html\less\back.less` to `public_html\css\*`
3. Copy `App\config\config.php.dist` to a new file: `App\config\config.php` and change values for your config
4. Configure piwik db access in \public_html\piwik\config\config.ini.php (lines 4 to 7)
5. Configure piwik url in \public_html\piwik\config\config.ini.php (line 19)
6. Chmod 744 \public_html\piwik\robots.txt
7. Change login and password in http://*install_path*/public_html/piwik/index.php ( default are root : root )


Access to full statistics is at http://*install_url*/public_html/piwik/index.php

default login/passwd is foo@bar.baz : foo
