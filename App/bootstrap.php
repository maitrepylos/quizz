<?php
/**
 * AST quiz - innovons.be
 * Created by PAPERPIXEL
 * www.paperpixel.net
 */

use App\Controller\CMSController;
use App\Controller\StatisticsController;
use App\Security\User;
use Silex\Application;
use Silex\Provider\SwiftmailerServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\SecurityServiceProvider;
use Silex\Provider\SessionServiceProvider;
use App\Controller\QuizController;
use App\Controller\AdminController;
use App\Controller\UserController;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;


//Notice: Undefined index: env (pivotal #61941990)
$env = getenv('env');
if($env == 'test'){
    require __DIR__.'/config/config-tests.php';
}else{
    require_once __DIR__.'/config/config.php';
}

if(!defined('DOMPDF_ENABLE_AUTOLOAD')) {
    define('DOMPDF_ENABLE_AUTOLOAD',false);
}

require_once __DIR__.'/vendor/dompdf/dompdf/dompdf_config.inc.php';
if(!defined('DOMPDF_UNICODE_ENABLED')) {
    define('DOMPDF_UNICODE_ENABLED',true);
}
$app = new Silex\Application();

$app->register(new \Silex\Provider\MonologServiceProvider(), array(
    'monolog.logfile' =>__DIR__.'/dev.log'
));

$app->register(new UrlGeneratorServiceProvider());

$app->register(new TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/../App/View'
));

$app->register(new SwiftmailerServiceProvider());
$app['swiftmailer.options'] = array(
    'host' => $smtpServer,
    'port' => $smtpPort,
    'username'=>$smtpUser,
    'password'=>$smtpPass,
    'encryption'=>$encryption,
    'auth_mode'=>$auth_mode
);


$app->register(new Silex\Provider\DoctrineServiceProvider(), array(
    'db.options'=>array(
        'driver' => 'pdo_mysql',
        'host'   => $host,
        'dbname' => $dbname,
        'user'   => $user,
        'password' => $pwd,
        'charset' => 'UTF8'
    )
));

$app['security.access_rules'] = array(
    /*array('^/login'=>'ROLE_ADMIN'),*/
    array('^/admin' =>'ROLE_ADMIN'),
    /*array('^.*$'=>'ROLE_ADMIN') */);

$app['security.providers'] = array(
    'main'=>array('entity'=>array('class'=>'App\Security\User', 'property'=>'email'))
);
$app['security.role_hierarchy'] = array(
    'ROLE_ADMIN' => array('ROLE_USER'));

$app['security.firewalls'] = array(
    'admin' => array(
        'pattern' => '^/admin|/api',
        'form' => array(
            'login_path' => '/user/signin',
            'check_path' => '/admin/login_check',
            'default_target_path'=>'/admin',
            'always_use_default_target_path'=>true
        ),
        'logout' => array('logout_path' => '/admin/logout'),
        'users'=>$app->share(function() use($app){
                return new App\Security\UserProvider($app['db'], $app);
            })
    )
);

$app->register(new SecurityServiceProvider());
$app['user_manager'] = new \App\Model\UserRepository($app['db']);

$app->on(\Symfony\Component\Security\Http\SecurityEvents::INTERACTIVE_LOGIN, function (InteractiveLoginEvent $event)use($app) {
    $token = $event->getAuthenticationToken();
    if(null !== $token){
        $user = $token->getUser();

        if($user instanceof User){

            $app['user_manager']->setLastLogin($user->getId());
        }
    }else{
       //  return;
    }


});

$app['slugify'] = $app->protect(function($string){
    // Remove accents from characters
    // $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
    // $string = mb_convert_encoding((string)$string, 'UTF-8', mb_list_encodings());

    // Everything lowercase
    $string = strtolower($string);

    $char_map = array(
        // Latin
        'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'AE', 'Ç' => 'C',
        'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I',
        'Ð' => 'D', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ő' => 'O',
        'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ű' => 'U', 'Ý' => 'Y', 'Þ' => 'TH',
        'ß' => 'ss',
        'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'ae', 'ç' => 'c',
        'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
        'ð' => 'd', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ő' => 'o',
        'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'ű' => 'u', 'ý' => 'y', 'þ' => 'th',
        'ÿ' => 'y',
        // Latin symbols
        '©' => '(c)',
    );

    $string = str_replace(array_keys($char_map), $char_map, $string);

    // Replace all non-word characters by dashes
    // $string = preg_replace('/[^\p{L}\p{Nd}]+/u', '-', $string);
    $string = preg_replace('/[^\w]+/u', '-', $string);

    // Replace double dashes by single dashes
    $string = preg_replace("/-+/", '-', $string);

    // Trim dashes from the beginning and end of string
    $string = trim($string, '-');

    return $string;
});

$app->register(new SessionServiceProvider());
/* @var array[string]\App\Model\QuizRepository */
$app['quiz_manager'] = function() use($app){
   return new \App\Model\QuizRepository($app);
};

$app['cms_manager'] = function() use($app){
  return new \App\Model\CMSRepository($app);
};

$app['json_parser'] = function() use($app){
  $parser = new \App\lib\JsonParser($app);
  $parser->setJsonRestorer(new \App\lib\JsonRestorer($app));
    return $parser;
};

$app['current.user_id'] = function() use($app){
  $token = $app['security']->getToken();
    if(null !== $token){
        $user = $token->getUser();
        if($user instanceof User){
            return $user;
        }
    }

    return new User();
};


$app['params.save_path'] = __DIR__.'/../quizzes/';
$app['params.upload_path'] = __DIR__.'/../public_html/uploads/';
$app->mount("/admin", new AdminController());
$app->mount("/admin", new StatisticsController());
$app->mount("/admin/cms", new CMSController());
$app->mount("/", new QuizController());
$app->mount("/", new UserController());
$app->mount("/json", new \App\Controller\JsonController());
$app->mount("/api", new App\Controller\ApiController());

$app['postmaster_address'] = '<webmaster@innovons.be>';
$app['postmaster_from'] = 'Agence pour l\'Entreprise et l\'Innovation';
$app['postmaster_noreply'] = '<noreply@innovons.be>';

/* affichage par défaut */
$app['default.quiz_id'] = $default_quiz;

$app['get_options'] = function() use($app){
    $query = 'SELECT * FROM config WHERE quiz_id IS NULL';
    $stmt = $app['db']->prepare($query);
    $stmt->execute();
    $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
    return $res;

};

$app['get_option'] = $app->protect(function($key='') use($app){

    $query = "SELECT * FROM  config WHERE `key` = :k AND quiz_id IS NULL";

    $stmt = $app['db']->prepare($query);

    $stmt->bindValue(':k',$key);

    $stmt->execute();
    return $stmt->fetchAll(\PDO::FETCH_ASSOC);

});

$app['set_option'] = function ($key,$value,$quiz_id=null) use($app){

};


return $app;

