<?php
/**
 * Greg Berger
 *
 * Date: 12/04/14
 * Time: 16:33
 */

namespace App\lib;


use Silex\Application;

class JsonParser {

    protected $json;
    /**
     * @var Application
     */
    protected $app;

    /**
     * @var JsonRestorer
     */
    protected $jsonRestorer;

    private $errors=array();

    public function __construct(Application $app){
        $this->app=$app;
    }

    public function retrieveJson($rev){
        // todo retrieve on file system
        $path=$this->app['params.save_path'].$rev['quiz_id'].'/'.$rev["json_filename"];
        $fh = fopen($path, 'r');
        $json = fread($fh,filesize($path));
        fclose($fh);
        $this->json = json_decode($json, true);

    }


    /**
     * @param $rev array (the revision tuple)
     */
    public function restore($rev){
         $this->retrieveJson($rev);
         $this->walk('restore', $this->json);
        $this->jsonRestorer->restore();
    }

    public function validate($quiz_id){

        $tree = $this->app['quiz_manager']->retrieveTree($quiz_id);

        $this->walk('validate',json_decode($tree,true));

        return empty($this->errors)?true:$this->errors;
    }

    /**
     * @param string $action restore | duplicate
     *
     * take the json and create node objects
     */
    private function walk($action='restore',$arr, $depth=0){
        // firstnode
        if(isset($arr['type'])){
            if($arr['type'] == 'quiz'){
                $this->jsonRestorer->processNodeData($arr);
                if(isset($arr['question']) && ! empty($arr['question'])){
                    $this->jsonRestorer->processNodeData($arr['question']);
                }
                $this->walk($action,$arr['choices'], $depth+1);
            }
            if(isset($arr['popup'])){
                $this->jsonRestorer->processNodeData($arr['popup']);
            }
        }else{
            foreach($arr as $a){

                if($action == 'validate'){
                   if($a['type'] == 'choice'){
                       if(empty($a['helps']) && empty($a['choices'])){
                           $this->errors[] = $a['name'];
                       }
                   }
                }elseif($action == 'restore'){
                    $this->jsonRestorer->processNodeData($a);
                }

               if(isset($a['popup']) && count($a['popup'])>0){
                   if($action == 'restore')
                        $this->jsonRestorer->processNodeData($a['popup']);
               }
               if(isset($a['choices']) && count($a['choices'])>0){
                   $this->walk($action,$a['choices'],$depth+1);
               }
                if(isset($a['helps']) && count($a['helps'])>0){
                    $this->walk($action,$a['helps'],$depth+1);
                }
                if(isset($a['related']) && count($a['related'])>0 && isset($a['related'][0]['id']) ){
                    $this->walk($action,$a['related'],$depth+1);
                }
                if(isset($a['urls']) && count($a['urls'])>0){
                    $this->walk($action,$a['urls'],$depth+1);
                }
            }
        }


    }

    private function println($str){
        echo $str.'<br />';
    }

    /**
     * @param \App\lib\JsonRestorer $jsonRestorer
     */
    public function setJsonRestorer($jsonRestorer) {
        $this->jsonRestorer = $jsonRestorer;
    }

    /**
     * @return \App\lib\JsonRestorer
     */
    public function getJsonRestorer() {
        return $this->jsonRestorer;
    }


} 