<?php
/**
 * Greg Berger
 *
 * Date: 22/04/14
 * Time: 10:04
 */

namespace App\lib;


use Silex\Application;

class JsonRestorer {

    /**
     * @var Application
     */
    private $app;


    private $navigation=array();
    private $nodes=array();
    private $meta=array();
    private $helpContents=array();

    private $help_contents=array();
    private $helps = array();


    private $navQuery='INSERT INTO navigation(ancestor, descendant, depth) ';
    private $nodesQuery='INSERT INTO nodes (id, meta_id, position, status, quiz_id) VALUES ';
    private $metaQuery='INSERT INTO node_properties (id, type, name, shortname, text, path) VALUES ';
    private $helpContentsQuery='INSERT INTO help_contents (id, type, name, text, path, shortname, position, meta_id) VALUES ';



    public function __construct(Application $app){
        $this->app = $app;
    }

    public function addData($table, $data){
        if(property_exists($this,$table)){
            array_push($this->$table,$data);
        }
    }

    private function buildQueries(){
        /*
        if(! empty($this->navigation)){
            $this->navQuery.=implode(',',$this->navigation);
        }else{
            $this->navQuery='';
        }
        */
        if(! empty($this->nodes)){

            $this->nodesQuery.=implode(',',$this->nodes);
        }else{
            $this->nodesQuery='';
        }
        if(!empty($this->meta)){

            $this->metaQuery.=implode(',',$this->meta);
        }else{
            $this->metaQuery='';
        }
        if(!empty($this->helpContents)){

            $this->helpContentsQuery.=implode(',',$this->helpContents);
        }else{
            $this->helpContentsQuery='';
        }
    }

    private function executeQueries(){
        $this->app['db']->beginTransaction();
        if($this->metaQuery!='')
            $this->app['db']->executeQuery($this->metaQuery);
        if($this->nodesQuery!='')
            $this->app['db']->executeQuery($this->nodesQuery);
        if($this->helpContentsQuery != '')
            $this->app['db']->executeQuery($this->helpContentsQuery);
        if($this->navQuery != ''){
            foreach($this->navigation as $n){
                $this->app['db']->executeQuery($this->navQuery.$n);
            }
        }
        $this->app['db']->commit();
    }

    private function displayQueries(){
        echo $this->metaQuery;
        echo '<br />';
        echo $this->nodesQuery;
        echo '<br />';
        echo $this->helpContentsQuery;
        echo '<br />';
        foreach($this->navigation as $n){
            echo $this->navQuery.$n;
            echo '<br />';
        }
    }

    public function processNodeData($a){
        if(substr($a['id'],0,1) != 'c' ){
            // $str = sprintf('INSERT INTO nodes (id, meta_id, position, status, quiz_id) VALUES( %s,%s,%s,%s,%s)', $a['id'], $a['meta_id'], $a['position'],'',$a['quizId']);
            $str = sprintf('( %s,%s,%s,%s,%s)', $a['id'], $a['meta_id'], $a['order'],'NULL',$a['quizId']);
            $this->addData('nodes',$str);

            // related helps data should only be inserted once
            if(! in_array($a['meta_id'],$this->helps)){
                $str = sprintf("(%s,'%s',%s,%s,%s,%s)",
                    $a['meta_id'],
                    $a['type'],
                    $this->app['db']->quote($a['name'],\PDO::PARAM_STR),
                    $this->app['db']->quote($a['shortname']),
                    $this->app['db']->quote($a['text']),
                    $this->app['db']->quote($a['path'])
                );
                $this->addData('meta',$str);
                $this->helps[] = $a['meta_id'];
            }
            if(isset($a['breadcrumbs'])){

                if($a['type'] == 'quiz'){
                    // it's the first node => doesn't have a parent in navigation
                    $parent = 'null';
                }else{

                    $parent = $a['breadcrumbs'][count($a['breadcrumbs'])-1];
                }
                $this->addData('navigation', sprintf('SELECT n.ancestor, %2$s, n.depth+1
                        FROM navigation AS n
                        WHERE n.descendant = %1$s
                        UNION ALL
                        SELECT %2$s, %2$s, 0',
                     $parent,$a['id']));

            }
        }else{
            // multiple used help contents should only be inserted once
            if(! in_array($a['id'], $this->help_contents)){
                $str = sprintf("(%s,'%s',%s,%s,%s,%s,%s,%s)",
                    str_replace('c_','',$a['id']),
                    $a['type'],
                    $this->app['db']->quote($a['name'],\PDO::PARAM_STR),
                    $a['text']==NULL?'NULL':$this->app['db']->quote($a['text']),
                    $a['path']==null?'NULL':$this->app['db']->quote($a['path']),
                    $a['shortname']==null?'NULL':$this->app['db']->quote($a['shortname']),
                    $a['position']==null?0:$a['position'],
                    $a['meta_id']);
                $this->addData('helpContents', $str);

                $this->help_contents[] = $a['id'];
            }
        }
    }

    public function restore(){
        $this->buildQueries();
        // $this->displayQueries();
        $this->executeQueries();
    }
} 