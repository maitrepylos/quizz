<?php
/**
 * Created by Paperpixel.
 * www.paperpixel.net
 */

namespace App\Controller;

use App\Model\QuizRepository;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Silex\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class AdminController implements ControllerProviderInterface {

    /**
     * Returns routes to connect to the given application.
     *
     * @param Application $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(Application $app)
    {
        $index = $app['controllers_factory'];

        //$index->get("/quiz/{qSlug}/stats", 'App\Controller\AdminController::stats')->bind('admin_quiz_stats');
        
        $index->get("/quiz/{qSlug}/export", 'App\Controller\AdminController::export')->bind('admin_quiz_export');
        $index->get("/quiz/{qSlug}/revisions", 'App\Controller\AdminController::revision')->bind('admin_quiz_revision');
        $index->get("/quiz/{qSlug}", 'App\Controller\AdminController::quiz')->bind('admin_quiz');

        $index->get("/", 'App\Controller\AdminController::index')->bind('admin_home');
        return $index;
    }


    public function index(Application $app) {
        $params = array(
            'quizzes' => $app['quiz_manager']->getQuizzes()
        );
        return $app['twig']->render('back\index.html.twig', $params);
    }

    public function quiz(Application $app, $qSlug) {
        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];
        $quizzes = $qm->getQuizzes();
        $q = $qm->getQuizBySlug($qSlug);

        if(! $q){
            throw new NotFoundHttpException(sprintf('Le quiz %s n\'existe pas', $qSlug));
        }

        $params = array(
            'quiz' => array(
                'id' => $q['id'],
                "name" => $q['name'],
                'slug'=>$q['slug'],
                'description'=>$q['description'],
                'published' => $q['published']
            ),
            'quizzes' => $quizzes
        );
        return $app['twig']->render('back\quiz_navigation.html.twig', $params);
    }

    public function stats(Application $app, $qSlug) {
        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];
        $quizzes = $qm->getQuizzes();
        $q = $qm->getQuizBySlug($qSlug);

        if(! $q){
          throw new NotFoundHttpException(sprintf('Le quiz %s n\'existe pas', $qSlug));
        }

        $params = array(
          'quiz' => array('id' => $q['id'], "name" => $q['name'], 'slug'=>$q['slug'], 'description'=>$q['description']),
            'quizzes' => $quizzes
        );
        return $app['twig']->render('back\quiz_statistics.html.twig', $params);
    }


    public function export(Application $app, $qSlug) {
        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];
        $quiz = $qm->getQuizBySlug($qSlug);
        $quizzes = $qm->getQuizzes();
        if(! $quiz){
            throw new NotFoundHttpException(sprintf("Le quiz %s n'existe pas", $qSlug));
        }
        $params = array(
            'quiz' => $quiz,
            'quizzes' => $quizzes
        );
        return $app['twig']->render('back\quiz_export.html.twig', $params);
    }

    public function revision(Application $app, $qSlug){
        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];

        $revs = $qm->getQuizRevisions($qSlug);

        $quizzes = $qm->getQuizzes();
        $quiz = $qm->getQuizBySlug($qSlug);
        $params = array(
          'revisions' => $revs,
          'quizzes' => $quizzes,
          'quiz'=>$quiz
        );
        return $app['twig']->render('back\quiz_revisions.html.twig', $params);
    }

    /**
     * get colors for front UI based on a string (eg. FF0000||00FF00|0000FF|)
     * @param $request
     * 0|node
     * 1|nodeHover
     * 2|nodeSelected
     * 3|action
     * 4|actionSelected
     * @return array|null
     */
    private function getColorsFromRequest($request) {
        $colors = '';
        $colorsRequest = $request->get('colors') ? explode('|', $request->get('colors')) : null;
        if(isset($colorsRequest)) {
            $colors = array();
            $colors['request'] = $request->get('colors');
            $colorsRequest[0] != '' ? $colors['node'] = $colorsRequest[0] : null;
            $colorsRequest[1] != '' ? $colors['nodeHover'] = $colorsRequest[1] : null;
            $colorsRequest[2] != '' ? $colors['nodeSelected'] = $colorsRequest[2] : null;
            $colorsRequest[3] != '' ? $colors['action'] = $colorsRequest[3] : null;
            $colorsRequest[4] != '' ? $colors['actionSelected'] = $colorsRequest[4] : null;
        }
        return $colors;
    }
}