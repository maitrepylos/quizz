<?php
namespace App\Controller;

use App\Security\User;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * AST quiz - innovons.be
 * Created by PAPERPIXEL
 * www.paperpixel.net
 */

class UserController implements ControllerProviderInterface {

    /**
     * Returns routes to connect to the given application.
     *
     * @param Application $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(Application $app)
    {
        $ctrl = $app['controllers_factory'];
        $ctrl->match("/user/signin", 'App\Controller\Usercontroller::login')->bind('login');
        $ctrl->match('/user/register', '\App\Controller\UserController::registerUser')->bind('register');
        $ctrl->match('/user/recover', '\App\Controller\UserController::recoverPassword')->bind('recover');
        $ctrl->match('/user/thankyou', '\App\Controller\UserController::thankYou');
        $ctrl->get("/admin/users", 'App\Controller\UserController::users')->bind('admin_users');
        $ctrl->match("/admin/users/add", "\App\Controller\UserController::addUser")->bind('users_add');
        $ctrl->post("/admin/users/delete", "\App\Controller\UserController::deleteUser")->bind('users_delete');
        $ctrl->post("/admin/users/update", "\App\Controller\UserController::updateUser")->bind('users_update');
        $ctrl->post("/admin/users/resendMail", "\App\Controller\UserController::resendMail")->bind('users_send_email');
        return $ctrl;
    }

    public function login(Application $app) {
        return $app['twig']->render('login/signin.html.twig', array(
            'error'         => $app['security.last_error']($app['request']),
            'last_username' => $app['session']->get('_security.last_username'),
        ));
    }

    // fixme validation!
    public function users(Application $app) {
        if(! $app['security']->isGranted('ROLE_ADMIN')){
            return $app->json(array('message'=>'Accès interdit', 403));
        }

        $token = $app['security']->getToken();
        if(null !=  $token){
            $usr = $token->getUser();
        }
        $users =  $app['user_manager']->getAllUsers($usr->getId());

        $params = array(
            'quizzes' => $app['quiz_manager']->getQuizzes(),
            'users'=>$users,
            'me'=>$usr->toArray()
        );
        return $app['twig']->render('back\users.html.twig', $params);
    }

    public function addUser(Application $app, Request $req){

        if(! $app['security']->isGranted('ROLE_ADMIN')){
            return $app->json(array('message'=>'Accès interdit', 403));
        }

        $data=array();
        $data['name'] = $req->get('name');
        $data['firstname'] = $req->get('firstname');
        $data['roles'] = $req->get('roles');
        $data['telephone'] = $req->get('telephone');
        $data['email'] = $req->get('email');
        if($data['email'] == ''){
            return $app->json(array('message'=>'entrer email'), 400);
        }

        try{
            $user = $app['user_manager']->createUser($data);
        }catch(\Exception $e){
            return $app->json(array('message' => 'Erreur: impossible de créer l\'utilisateur'), 500);
        }
        if(false !== $user){
            return  $this->generateKeyAndSendMail($app, $user->getId());
        }else{
            return $app->json(array('message' => 'Erreur: L\'adresse e-mail est déjà enregistrée'), 500);
        }
    }

    public function updateUser(Application $app, Request $req){
        if(! $app['security']->isGranted('ROLE_ADMIN')){
            return $app->json(array('message'=>'Accès interdit', 403));
        }
        $user_id = $req->get('id');
        /** @var User $user */
        $user = $app['user_manager']->getUserById($user_id);

        if($user === false|| $user === null || empty($user)){
            return $app->json(array('message'=>'utilisateur non trouvé'));
        }

        $name = $req->get('name');
        $firstname=$req->get('firstname');
        $tel = $req->get('telephone');
        $email = $req->get('email');
        $roles = $req->get('roles');
        $id = $req->get('id');
        if($email == ''){
            return $app->json(array('message'=>'email obligatoire'),400);
        }
        if($roles && !is_array($roles)){
            $roles = array($roles);
        }
        $u2 = new User($name,"",$roles,$email,$id,"","",$name,$firstname,$tel);

        if(! $user->equals($u2)){
            $user->setName($name);
            $user->setFirstname($firstname);
            $user->setEmail($email);
            if($roles)
                $user->setRoles($roles);
            $user->setTelephone($tel);

            try{
                $app['user_manager']->updateUser($user);
            }catch(Exception $e){
                return $app->json(array('message'=>'un problème est survenu pendant la modification de l\'utilisateur'), 500);
            }

        }
        return $app->json($user->toArray());
    }


    public static function deleteUser(Application $app, Request $req) {
        if(! $app['security']->isGranted('ROLE_ADMIN')){
            return $app->json(array('message'=>'Accès interdit', 403));
        }
        $res = $app['user_manager']->deleteUser($req->get('id'));
        if($res > 0){
            return $app->json(array('message'=>'ok'));
        }else{
            return $app->json('Something went wrong', 500);
        }
    }



    public function generateKeyAndSendMail(Application $app, $user_id=""){
        if($user_id instanceof Request){
            $user_id = $user_id->get('id');
        }
        $user = $app['user_manager']->getUserById($user_id);
        if($user == null || !($user instanceof User)){
            $app['session']->getFlashBag()->set('error', 'Impossible d\'envoyer le courriel, veuillez vérifier votre adresse ou contacter votre administrateur');
            return $app->redirect($app['url_generator']->generate('recover'));
        }
        $key = $this->generatePwd($user->getEmail());
        $url = $app['url_generator']->generate('register', array(), true )."?key=".$key."&mail=".urlencode($user->getEmail());

        if($this->sendMail($user->getEmail(), $url, $app)) {
            $user->setRegKey($key);
            $user->setActivated(0);
            $user->eraseCredentials();
            $res = $app['user_manager']->updateUser($user);
            if(! $res ){
                return new JsonResponse(array('message' => 'Impossible de mettre à jour les informations de l\'utilisateur.'), 500);
            }

            return $app->json($user->toArray(), 200);
        } else {
            return new JsonResponse(array('message' => 'L\'e-mail n\'a pas été envoyé.'), 500);
        }
    }

    private function generatePwd($salt=""){
        $salt  = $_SERVER['QUERY_STRING'].sha1($_SERVER['REQUEST_TIME']).md5($salt);
        $salt = hash("sha512", $salt);
        $pwd = md5($salt);
        return $pwd;
    }

    private function sendMail($email, $key, $app=null){
        $mail = "Bonjour,\n\n\nVoici vos informations de connexion.\n".$key."\n";
        $mail.= "\n\nUtilisez votre adresse email pour vous enregistrer sur notre site";
        $mail.="\n\n\nMerci.";
        $res = mail($email,"Innovons.be", $mail, sprintf('From: "%1$s"%2$s', $app['postmaster_from'],$app['postmaster_address']));
        return $res;
    }



    public function registerUser(\Silex\Application $app) {
        $key = '';
        $mail = '';
        $err = false;
        $noform = false;
        if('GET' === $app['request']->getMethod()){
            $key = $app['request']->get('key');
            $mail = $app['request']->get('mail');
            $err = false;
            if(! $key || ! $mail){
                $err = true;
                $msg = "Invalid GET credentials";
                $noform = true;
            }else{
                $user = $app['user_manager']->getByMailAndKey($mail, $key);
                if(! $user instanceof \App\Security\User){
                    $err = true;
                    $msg = "Adresse email invalide";
                    $noform = true;
                }
            }
        }else if('POST' === $app['request']->getMethod()){
            $pwd = $app['request']->get('pwd');
            $pwd2 = $app['request']->get('pwd2');
            $mail = $app['request']->get('mail');
            $key = $app['request']->get('key');
            if($pwd != $pwd2){
                $err = true;
                $msg = "Les mots de passe doivent être identiques";
            }
            $user = $app['user_manager']->getByMailAndKey($mail, $key);
            if($user instanceof \App\Security\User ){
                $user->setRegKey(null);
                $user->setActivated(1);
                $user->setPwd($app, $pwd);
                $res = $app['user_manager']->updateUser($user);
                if(! $res){
                    $err  = true;
                    $msg  = "Something went terribly wrong. If world hasn't collapsed yet, give another try or contact site administrator";
                    $noform = true;
                }else{
                    return $app->redirect($app['url_generator']->generate('login', array('email'=>$user->getEmail())));
                }

            }else{
                $err = true;
                $msg = "Invalid credentials";
                $noform = true;
            }
        }
        if($err === true ){
            return $app['twig']->render('login/login_register.twig', array('error'=>array('message'=>$msg, 'noform'=>$noform)));
        }
        return $app['twig']->render('login/login_register.twig',array('vars'=>array('key'=>$key, 'mail'=>$mail)));
    }

    public function recoverPassword(\Silex\Application $app) {
        if('POST' === $app['request']->getMethod()){
            $mail = $app['request']->get('email');

            $user = $app['user_manager']->getUserByMail($mail);

            if($user != null && $user instanceof User) {
                $res = $this->generateKeyAndSendMail($app, $user->getId());
                if(! $res->isServerError()) {
                    $app['session']->getFlashBag()->set('success', 'Un courriel vient de vous être envoyé');
                    return $app->redirect($app['url_generator']->generate('login'));
                }
            }
            // error !
            $app['session']->getFlashBag()->set('error', 'Impossible d\'envoyer le courriel, veuillez vérifier votre adresse ou contacter votre administrateur');
            return $app->redirect($app['url_generator']->generate('recover'));
        }
        return $app['twig']->render('login/login_forgot.twig');
    }


    public function resendMail(Application $app) {
        if('POST' === $app['request']->getMethod()){
            $user_id = $app['request']->get('user_id');
            $user = $app['user_manager']->getUserById($user_id);
            if($user) {
                return $this->generateKeyAndSendMail($app, $user_id);
            } else {
                /*TODO handle error*/
            }
        }
    }


    public function thankYou(Application $app) {
        /** @var $token \Symfony\Component\Security\Core\Authentication\Token\TokenInterface */
        $token = $app['security']->getToken();
        $user = null;
        if(null != $token){
            $user = $token->getUser();
        }else{
            throw new \Exception('User not found');
        }

        return $app['twig']->render('login/thank_you.twig', array('organization' => '$organization'));
    }


}