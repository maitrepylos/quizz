<?php
namespace App\Controller;

use App\Model\CMSRepository;
use App\Security\User;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;


use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * AST quiz - innovons.be
 * Created by PAPERPIXEL
 * www.paperpixel.net
 */

class CMSController implements ControllerProviderInterface {

    /**
     * Returns routes to connect to the given application.
     *
     * @param Application $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(Application $app)
    {
        $ctrl = $app['controllers_factory'];
        $ctrl->match("/", 'App\Controller\CMSController::index')->bind('cms');
        $ctrl->post("/addLink", 'App\Controller\CMSController::addLink')->bind('cms_link_add');
        $ctrl->post("/updateLink", 'App\Controller\CMSController::updateLink')->bind('cms_link_update');
        $ctrl->post("/deleteLink", 'App\Controller\CMSController::deleteLink')->bind('cms_link_delete');
        $ctrl->post("/changeDefaultQuiz", 'App\Controller\CMSController::changeDefaultQuiz')->bind('cms_default_quiz');
        $ctrl->post("/displayToolLinks", 'App\Controller\CMSController::displayToolLinks')->bind('cms_links_display');
        $ctrl->post("/displayFooter", 'App\Controller\CMSController::displayFooterEverywhere')->bind('cms_footer_display');
        $ctrl->post("/updateFooter", 'App\Controller\CMSController::updateFooter')->bind('cms_footer_update');
        $ctrl->post("/uploadImage", 'App\Controller\CMSController::uploadImage')->bind('cms_image_upload');
        return $ctrl;
    }

    public function index(Application $app) {
        try{
            $default_quiz = $app['cms_manager']->getOption('default_quiz');
            $display_footer = filter_var($app['cms_manager']->getOption('display_footer'), FILTER_VALIDATE_BOOLEAN );
            $display_tools = filter_var($app['cms_manager']->getOption('display_tools'), FILTER_VALIDATE_BOOLEAN );
        }catch(\Exception $e){
            $default_quiz = 0;
            $display_footer=false;
            $display_tools=false;

        }
        $footer = $app['cms_manager']->getContentByType('footer',0);

        return $app['twig']->render('back/cms.html.twig', array(
            'quizzes' => $app['quiz_manager']->getQuizzes(true),
            'cms' => array(
                "baseline" => "trouvons, collaborons, innovons !",
                "footer_text" => $footer[0]['content'],
                "footer_id" => $footer[0]['id'],
                "links" => $this->_get_links($app),
            ),
            'default_quiz' => $default_quiz,
            'display_tools' => $display_tools,
            'display_footer' => $display_footer
        ));
    }

    public function addLink(Application $app, Request $req) {

        $title = $req->get('tool_link_title');
        $url = $req->get('tool_link_url');
        $desc = $req->get('tool_link_desc');

        $message = 'Lien correctement ajouté';
        $code = 200;

        if($url === "" || ! $url ){
            $message = "Entrez au minimum l'url";
            $code = 500;
        }

        $link = array(
            "title" => $req->get('tool_link_title'),
            "description" => $req->get('tool_link_desc'),
            "url" => $req->get('tool_link_url')
        );

        $data = array( 'type'=>'link','content'=>serialize($link), 'quiz_id'=>0);

        try{
            $app['cms_manager']->createContent($data);
        }catch (\Exception $e){
            $message = 'Une erreur est survenue';
            $code = 500;
        }


        return json_encode($this->_get_links($app));
    }

    public function updateLink(Application $app, Request $req) {
        $id = $req->get('tool_link_id');

        $title = $req->get('tool_link_title');
        $url = $req->get('tool_link_url');
        $desc = $req->get('tool_link_desc');

        $message = 'Lien correctement ajouté';
        $code = 200;

        if($url === "" || ! $url ){
            $message = "Entrez au minimum l'url";
            $code = 500;
        }

        $link = array(
            "title" => $req->get('tool_link_title'),
            "description" => $req->get('tool_link_desc'),
            "url" => $req->get('tool_link_url')
        );

        $data = array( 'type'=>'link','content'=>serialize($link), 'quiz_id'=>0);

        try{
            $app['cms_manager']->updateContent($id, $data);
        }catch (\Exception $e){
            $message = 'Une erreur est survenue';
            $code = 500;
        }


        return json_encode($this->_get_links($app));
    }

    public function deleteLink(Application $app, Request $req) {
        $id = $req->get('link_id');
        if(! $id){
            return new JsonResponse(array('message'=>'Une erreur est survenue. Veuillez recherger la page et réessayeer'),500);
        }

        $app['cms_manager']->deleteContent($id);

        return json_encode($this->_get_links($app));
    }

    public function changeDefaultQuiz(Application $app, Request $req) {
        $quiz_id = $req->get('value');
        /** @var CMSController $cmsRep */
        $cmsRep = $app['cms_manager'];

        $data['key'] = 'default_quiz';
        $data['value'] = $quiz_id;

        if($cmsRep->addOption($data)){

            return new Response(json_encode(array('message' => "Le quiz par défaut a bien été mis à jour.")), 200);

        }else{
            return new JsonResponse(array('message'=> 'Une erreur est survenue pendant la mise à jour du quiz'), 500);
        }



    }

    public function displayToolLinks(Application $app, Request $req) {
        $isDisplay = filter_var($req->get('value'), FILTER_VALIDATE_BOOLEAN);
        $data['key'] = 'display_tools';

        $err_code = 200;
        if($isDisplay) {
            $data['value'] = 'true';
            $message = "La barre d'outils est maintenant affichée.";
        } else {
            $data['value'] = 'false';
            $message = "La barre d'outils est maintenant cachée.";
        }

        try{
            /** @var CMSRepository $manager */
            $manager = $app['cms_manager'];
            $manager->addOption($data);
        }catch (\Exception $e){
            $message = 'Une erreur est survenue pendant l\'enregistrement';
            $err_code = 500;
        }

        return new Response(json_encode(array('message' => $message)), $err_code);
    }

    public function displayFooterEverywhere(Application $app, Request $req) {
        $isDisplay = filter_var($req->get('value'), FILTER_VALIDATE_BOOLEAN);
        $data['key'] = 'display_footer';

        $err_code = 200;
        if($isDisplay) {
            $message = "Le pied de page sera affiché partout.";
            $data['value'] = 'true';
        } else {
            $message = "Le pied de page ne sera pas affiché dans les iframe.";
            $data['value'] = 'false';
        }

        try{
            /** @var CMSRepository $manager */
            $manager = $app['cms_manager'];
            $manager->addOption($data);
        }catch (\Exception $e){
            $message = 'Une erreur est survenue pendant l\'enregistrement';
            $err_code = 500;
        }

        return new Response(json_encode(array('message' => $message)), $err_code);
    }

    public function updateFooter(Application $app, Request $req) {
        $footer_content = $req->get('cms_footer_input');
        $footer_id = $req->get('cms_footer_id');

        $data['type'] = 'footer';
        $data['content'] = $footer_content;

        $message = "Le pied de page a bien été mis à jour.";
        $code = 200;
        try{
            if($footer_id != ""){
               $app['cms_manager']->updateContent($footer_id,$data);
            }else{
                $app['cms_manager']->createContent($data);
            }
        }catch(\Exception $e){
            $message = 'Une erreur est survenue '.$e->getMessage() ;
            $code = 500;
        }
        return new Response(json_encode(array('message' =>$message )), $code);
    }

    public function uploadImage(Application $app, Request $req) {
        $funcNum = $req->get('CKEditorFuncNum') ;

        $file = $req->files->get('upload');

        $fileName = $app['slugify']($file->getClientOriginalName());

        $file->move(dirname(__DIR__).'/../public_html/images',$fileName);


        $url = $req->getBasePath() . "/images/".$fileName;

        return "<script type='text/javascript'>window.parent.CKEDITOR.tools.callFunction(".$funcNum.", '".$url."', '');</script>";
    }

    private function _get_links(Application $app) {
        $links_data = $app['cms_manager']->getContentByType('link');
        $links = array();
        foreach($links_data as $link){
            $l = unserialize($link['content']);
            $l['id'] = $link['id'];
            $links[] = $l;
        }

        return $links;

    }
}