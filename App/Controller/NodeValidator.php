<?php
/**
 * Greg Berger
 *
 * Date: 11/01/14
 * Time: 00:20
 */

namespace App\Controller;


use Symfony\Component\HttpFoundation\Request;

class NodeValidator {

    static function validate(Request $request, $update=false){
        $type = $request->get('type');
        $name = $request->get('name');
        $text = $request->get('text');
        $ret = array('message'=>'ok');
        if($type == ''){
            return array('error'=>'Le type est obligatoire');
        }

        if($name == ''){
            return array('error'=>'Le nom du noeud est obligatoire');
        }



        switch($type){
            case 'file':{
                break;
            }
            case 'help':{
                if($text == ''){
                    $ret = array('error'=>'Le texte de l\'aide est obligatoire');
                }
                break;
            }
            case 'related':{
                if($text == ''){
                    $ret = array('error'=>'Le texte du contenu relatif est obligatoire');
                }
                break;
            }
            case 'url':{
                if($text == ''){
                    $ret = array('error'=>'Le texte de l\'url est obligatoire');
                }
                break;
            }
            case 'popup':{
                if($text == ''){
                    $ret = array('error'=>'Le texte de la popup est obligatoire');
                }
                break;
            }
            case 'choice':{
                break;
            }
            case 'question': {
                break;
            }
            default:{
                if($text == ''){
                    $ret = array('error'=>'Le texte est obligatoire');
                }
                break;
            }
        }
        return $ret;
    }



} 