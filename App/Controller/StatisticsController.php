<?php

namespace App\Controller;

use App\Model\QuizRepository;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * AST quiz - innovons.be
 * Created by PAPERPIXEL
 * www.paperpixel.net
 */

class StatisticsController implements ControllerProviderInterface {

    /**
     * Returns routes to connect to the given application.
     *
     * @param Application $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(Application $app)
    {
        /** @var ControllerCollection $ctrl */
        $ctrl = $app['controllers_factory'];

        $ctrl->match("/quiz/{qSlug}/stats", 'App\Controller\StatisticsController::index')->bind('admin_quiz_stats');
        $ctrl->match("/quiz/{qSlug}/getFromPiwik", 'App\Controller\StatisticsController::getStatsFromPiwik')->bind('from_piwik');
       	$ctrl->match("/stats", 'App\Controller\StatisticsController::stats')->bind("admin_stats");

        return $ctrl;
    }

    public function index(Application $app, $qSlug) {
        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];
        $quizzes = $qm->getQuizzes();
        $q = $qm->getQuizBySlug($qSlug);

        if(! $q){
            throw new NotFoundHttpException(sprintf('Le quiz %s n\'existe pas', $qSlug));
        }

        $params = array(
            'quiz' => array('id' => $q['id'], "name" => $q['name'], 'slug'=>$q['slug'], 'description'=>$q['description'], 'published' => $q['published']),
            'quizzes' => $quizzes
        );
        return $app['twig']->render('back\quiz_statistics.html.twig', $params);
    }

    public function stats(Application $app) {
    	$qm = $app['quiz_manager'];
        $quizzes = $qm->getQuizzes();
        $params = array(
            'quizzes' => $quizzes
        );
    	return $app['twig']->render('back\statistics.html.twig', $params);
    }

    function getStatsFromPiwik(Application $app, Request $req, $qSlug) {
        if($req->get('startDate') && $req->get('endDate')) {
            $response = $this->retrieveStats($app, $req->get('startDate'), $req->get('endDate'), $qSlug);
            error_log("slug " . $qSlug);

            return $app->json($response, 201);
        }
        return new Response(json_encode(array('message' => 'Paramètres manquants')), 500);
    }

    private function retrieveStats(Application $app, $startDate, $endDate, $slug){
        $basePath = $app['request']->isSecure() ? "https://" : "http://";
        $basePath .= $app['request']->getHttpHost();
        $basePath .= $app['request']->getBasePath();

	// C. Gustin - 20150916 : Paramètre segment laise a sa valeur par
	// defaut (vide) parce que le quizz principal n'inclut pas le slug
	// dans l'URL.
	//
	// Todo : permettre les stats sur les autres quizzes - e�viter que 
	// idSite soit hardcode avec la valeur 1
    	$url = $basePath."/piwik/index.php?module=API&method=CustomVariables.getCustomVariables&idSite=1&format=json&&token_auth=8a440e649b37e46600ba2b4c6aaa18c4&period=range&date=" . $startDate . "," . $endDate . "&segment=pageUrl=@" . $slug;
        $json = file_get_contents($url, true);
        $array = json_decode($json, true);


        $idSubtable = false;
        foreach($array as $k => $v)
        {
            if($v['label'] === "NodeId")
            {
                $idSubtable = $v['idsubdatatable'];
            }
        }

        if(!$idSubtable)
        {
            // si pas de retour pour la plage donnée,
            // renvoyer un array vide pour afficher 0 partout
            return json_encode(array());
        }

        $url = $basePath."/piwik/index.php?module=API&method=CustomVariables.getCustomVariablesValuesFromNameId&idSite=1&format=json&&token_auth=8a440e649b37e46600ba2b4c6aaa18c4&period=range&date=" . $startDate . "," . $endDate . "&idSubtable=" . $idSubtable . "&segment=pageUrl=@" . $slug;
        // $app['monolog']->addDebug("*** URL *** ".$url);
        $json = file_get_contents($url);
        return $json;
    }
}
