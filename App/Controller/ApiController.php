<?php
/**
 * Greg Berger
 *
 * Date: 17/11/13
 * Time: 04:50
 */

namespace App\Controller;

use App\Model\DataVizRepository;
use App\Model\QuizRepository;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Symfony\Component\Routing\RequestContext;

// [John] J'ai des notices avant l'objet json dans /json/last,
// pas trouvé le moyen de les enlever autrement
ini_set( 'display_errors', 0 );

class ApiController implements ControllerProviderInterface{
    /**
     * @var $qm QuizRepository
     */
    private $qm;
    private $params = array('node_id','quiz_id', 'parent_id','position','status','text',
        'question','description','type','name','shortname','path', 'meta_id');
    public function connect(Application $app){
        /**
         * @var $index ControllerCollection
         */
        $index = $app['controllers_factory'];
        $index->match("/", "App\Controller\ApiController::index")->bind('api_index');
        $index->match("/quiz/{quizId}", "App\Controller\ApiController::retrieveTree")->bind('api_tree');
        $index->match("/quiz/{quizId}/node/{nodeId}", "App\Controller\ApiController::retrieveNode");
        $index->match('/quiz/{quizId}/getChoiceWordings','App\Controller\ApiController::getChoiceWordings')->bind('wording_choices');
        $index->get('/quiz/{quizId}/getHelpList', 'App\Controller\ApiController::getHelpList')->bind('api_help_list');
        $index->match('/quiz/{quizId}/edit', 'App\Controller\ApiController::editQuiz')->bind('quiz-edit');

        $index->post("/createNode", "App\Controller\ApiController::addNode");//->method('post');
        $index->post("/deleteNode", "App\Controller\ApiController::deleteNode");
        $index->post("/updateNode", "App\Controller\ApiController::updateNodeMeta");
        $index->post("/updateOrder","App\Controller\ApiController::updateOrder");
        // $index->post("/moveSubTree", "App\Controller\ApiController::moveSubTree");
        $index->post("/duplicateMetaData", "App\Controller\ApiController::duplicateMetaData");
        $index->post("/publishQuiz", "App\Controller\ApiController::publish");
        $index->post("/unpublishQuiz", "App\Controller\ApiController::unpublish");

        $index->post("/duplicateHelp","App\Controller\ApiController::duplicateHelp");

        $index->post("/createQuiz", "App\Controller\ApiController::createQuiz");
        $index->post("/editQuiz", "App\Controller\ApiController::editQuiz");
        $index->post("/deleteQuiz", "App\Controller\ApiController::deleteQuiz");

        $index->post("/duplicateQuiz", "App\Controller\ApiController::duplicateQuiz");
        $index->post("/restoreRevision", "App\Controller\ApiController::restoreRevision");
        $index->post("/deleteRevision", "App\Controller\ApiController::deleteRevision");

        $index->post("/uploadFile", "App\Controller\ApiController::uploadFile")->bind('api_upload_file');
        $index->post("/updateFileNode", "App\Controller\ApiController::updateFileNode")->bind('api_update_file');

        $index->match('/test/{quizId}/{nodeId}', "App\Controller\ApiController::test");//->after(function($quizId){$quizId = ltrim($quizId,"0");});

        $index->post('/addChoice','App\Controller\ApiController::addNode')->before(function(Request $req, Application $app){$req->attributes->set('type', 'choice');});
        $index->post('/addQuestion','App\Controller\ApiController::addNode')->before(function(Request $req, Application $app){$req->attributes->set('type', 'question');});
        $index->post('/addHelp','App\Controller\ApiController::addNode')->before(function(Request $req, Application $app){$req->attributes->set('type', 'help');});
        $index->post('/addUrl','App\Controller\ApiController::addNode')->before(function(Request $req, Application $app){$req->attributes->set('type', 'url');});
        $index->post('/addFile','App\Controller\ApiController::addNode')->before(function(Request $req, Application $app){$req->attributes->set('type', 'file');});
        $index->post('/addRelated','App\Controller\ApiController::addNode')->before(function(Request $req, Application $app){$req->attributes->set('type', 'related');});

        $index->match("/quizViz/{quizId}", "App\Controller\ApiController::retrieveTreeViz")->bind('api_tree');
        return $index;

    }

    public function index(Application $app){
        /** @var $qm QuizRepository */
        $qm = $app['quiz_manager'];
        $res = $qm->getQuizzes();

        return $app->json($res,200);
    }

    public function retrieveTree(Application $app, $quizId){
        /** @var $qm QuizRepository */
        $qm = $app['quiz_manager'];
        $res =$qm->retrieveTree($quizId);

        return new Response($res, 200, array('Content-Type'=>'text/json;charset=UTF-8'));
    }
    public function retrieveTreeViz(Application $app, $quizId){
        /** @var $qm QuizRepository */

        $dvr = new DataVizRepository($app['db']);
        $res =$dvr->retrieveTree($quizId);

        return new Response($res, 200, array('Content-Type'=>'text/json;charset=UTF-8'));
    }
    /**
     * @param Application $app
     * @param Request $req
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     *
     *
     * Adds a node in database - mandatory parameters are
     *  - type ['question','choice','file','url','help']
     *  - parent_id : the only node without a parent is automatically created within the quiz creation process.
     *  - name
     */
    public function addNode(Application $app, Request $req){
        /** @var $qm QuizRepository */
        $qm = $app['quiz_manager'];
        $valid = NodeValidator::validate($req);
        if(isset($valid['error'])){
            if($app['debug']) sleep(3);
            return $this->returnError($valid['error'],400);
        }
        $data = $this->retrieveParamsFromRequest($req);

        // $app['monolog']->addDebug('parent id '.$req->get('parent_id'));

        if($req->get('parent_id') == "" ){
            return $this->returnError("pas de parent id",400);
        }else{

            if($req->get('type') == "file"){
                return $this->uploadFile($app, $req);
            }elseif($data['type'] == "url"){
                return $this->addUrl($app,$data);
            }

            $newNodeId = $qm->addNode($req->get('parent_id'),$data); 
            try{
                if(isset($data['meta_id']) && $data['meta_id'] != ""){
                    $this->duplicateMetaData($newNodeId,$data['meta_id']);
                }else{
                    $req->attributes->set('node_id', $newNodeId);
                    $this->insertMetaData($app, $req);
                }
            }catch (Exception $e){
                if($app['debug'])$app['monolog']->addError($e->getMessage());
                return $this->returnError('un problème abominable est survenu',500);
            }
        }
        $node = $qm->retrieveNode($newNodeId,$app);

        if(intval($newNodeId) != intval($node->getId())){
            if($app['debug'])$app['monolog']->addError('New node id est différent de l\'id de l\'objet node');
            return $this->returnError(500, 'Erreur à la création du noeud ');
        }

        if($app["debug"]) sleep(3);
        return $app->json($node->serializeJson(), 201);
    }

    /**
     *
     * Updates a node metadata
     * @param Application $app
     * @param Request $req
     *
     * request needs to have the following parameters:
     *          - node_id : the node we want to update the data (mandatory)
     *          - name : the name of the node (optional)
     *          - text:  the text (optional)
     *          - description: the description (optional)
     *          - path: url or uri (for files) (optional)
     *
     * @return if either of [name,text,description, path] is found a 400 error will be returned
     * else a 200 will be returned
     */
    public function updateNode(Application $app, Request $req){
        /** @var $qm QuizRepository */
        $qm = $app['quiz_manager'];

        try{
            $data =  $this->retrieveParamsFromRequest($req);
            if($data['name'] == '' && $data['text'] == ''){
                return $this->returnError('Aucune info à mettre à jour (pas de nom, pas de texte)',400);
            }

            $qm->updateNodeMeta($data['node_id'], $data);
        }catch(Exception $e){
            return $this->returnError($e->getMessage(),500);
        }
        return $this->returnSuccess(array('message'=>'ok'));
    }

    /**
     * @param Application $app
     * @param $quizId
     * @return Response
     *
     * returns a list of meta_id-name to link wording to a choice node.
     */
    public function getChoiceWordings(Application $app, $quizId){
        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];
        $res = $qm->getChoiceWordings($quizId);
        return new Response($res,200,array('Content-Type'=>'application/json'));
    }

    /**
     * @param Application $app
     * @param Request $req
     * @return Response
     *
     * duplicates an help content to a new help subtree attached to another parent
     *
     * Request needs to have the following parameters:
     *
     *          - help_id (mandatory)
     *          - parent_id (mandatory) the id of the new parent
     */
    public function duplicateHelp(Application $app, Request $req){
        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];
        $help_id = $req->get('help_id');
        $parent_id= $req->get('parent_id');
        if($help_id == ""){
            return $this->returnError('help_id introuvable',400);
        }elseif($parent_id == ""){
            return $this->returnError('parent_id introuvable',400);
        }else{
            try{
                $new_id = $qm->duplicateHelp($help_id,$parent_id);
            }catch (Exception $e){
                return $this->returnError($e->getMessage(),500);
            }
        }

        return $this->returnSuccess(array("id" => $new_id),200);
    }

    /**
     * @param Application $app
     * @param Request $req
     * @return Response
     *
     * @deprecated
     */
    public function moveSubTree(Application $app, Request $req){
        /** @var $qm QuizRepository */
        $qm = $app['quiz_manager'];
        $data = $this->retrieveParamsFromRequest($req, array('node_id', 'parent_id'));
        if(count($data)<2){
            return $this->returnError('',400);
        }
        try{
            $qm->moveSubTree($data['node_id'], $data['parent_id']);
        }catch(Exception $e){
            return $this->returnError($e->getMessage(),500);
        }

        return $this->returnSuccess(array('message'=>"ok"),200);
    }

    public function duplicateMetaData(Application $app, Request $req){
        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];

        $data = $this->retrieveParamsFromRequest($req, array('node_id','meta_id'));
        if(! isset($data['node_id']) || $data['node_id'] == ''){
            return $this->returnError(array('message'=>'pas de node_id'), 400);
        }
        if(! isset($data['meta_id']) || $data['meta_id'] == ''){
            return $this->returnError(array('message'=>'pas de meta_id'), 400);
        }
        $qm->duplicateMetaData($data['node_id'], $data['meta_id']);
    }

    public function insertMetaData(Application $app, Request $req){
        /** @var QuizRepository $qm */
        try{
            $qm = $app['quiz_manager'];
            $valid = NodeValidator::validate($req);
            $data = $this->retrieveParamsFromRequest($req);

            if((!isset($data['node_id']) || $data['node_id'] == "") || (!isset($data['type']) || $data['type'] == "" )  ){
                $this->returnError('Impossible d\'insérer les méta-données: type:'.$data['type'].' id: '.$data['node_id'],400);
            }
            $qm->insertMetaData($data['node_id'],$data);
        }catch(Exception $e){

        }
        return $this->returnSuccess(array('message'=>'ok'),201);

    }

    public function updateNodeMeta(Application $app, Request $req){
        /** @var $qm QuizRepository */
        $qm = $app['quiz_manager'];
        $valid = NodeValidator::validate($req);
        if(isset($valid['error'])){
            return $this->returnError($valid['error'],400);
        }
        $data = $this->retrieveParamsFromRequest($req);

        if(!$data['node_id']){
            return $this->returnError(400);
        }
        try{
            if($data['type'] == 'url'){

                $data['node_id'] = intval(substr($data['node_id'],2));

                if($app['debug']) $app['monolog']->addDebug($data['node_id']);

                $qm->updateHelpContent($data);
            }else{
                $qm->updateNodeMeta($data['node_id'], $data);
            }
        }catch(Exception $e){
            if($app['debug'])$app['monolog']->addError($e->getTraceAsString());
            return $this->returnError($e->getMessage(),500);
        }
        return $this->returnSuccess('ok', 200);
    }

    public function updateOrder(Application $app, Request $req){
        $positions = $req->get('order');
        /** @var $qm QuizRepository */
        $qm = $app['quiz_manager'];
        try{
            for($i = 0; $i < count($positions); $i++){
                if($app['debug']){
                    if($app['debug'])$app['monolog']->addDebug('id:'.$positions[$i].' position:'.$i);
                }
                $qm->updateOrder($positions[$i],$i);
            }
            return $this->returnSuccess(array('message'=>"ok"),200);
        }catch(Exception $e){
            return $this->returnError($e->getMessage(),500);
        }
    }

    public function deleteNode(Application $app, Request $req){
        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];
        $node_id = $req->get('node_id');
        $cascade = $req->get('cascade');
        if($node_id == "") return $this->returnError('Node id non trouvé',400);

        // if node is additional content its id should be prefixed
        if(strpos($node_id, 'c_')!==false){
            // remove prefix from node_id
            $node_id = intval(substr($node_id,2));
            $node = $qm->retrieveContentAsNode($node_id);
        }else{
            $node = $qm->retrieveNode($node_id);
        }
        try{
            if($node->getType() == "file") {
                $parent  = $qm->retrieveHelpNodeFromContent($node_id);

                unlink($app['params.upload_path'].$parent->getQuizId().'/'.$parent->getId().'/'.$node->getId().'.pdf');
                return $this->deleteHelpContent($app,$node->getId());
            }elseif($node->getType() == 'url'){
                return $this->deleteHelpContent($app,$node->getId());
            }
            $qm->deleteNode($node_id,($cascade=="")?true:$cascade);
        }catch(Exception $e){
            return $this->returnError($e->getMessage(),500);
        }

        return $this->returnSuccess(array('node_id'=>$node_id), 200);
    }

    public function createQuiz(Application $app, Request $req){

        // todo check slug unicity ^^
        $name = $req->get('name');
        if($name == '') return $this->returnError(array('message'=>'Nom de quiz non trouvé'), 400);
        try{

            $slug = $app['slugify']($name);
            $description = $req->get('description');
            $res = $app['quiz_manager']->createQuiz($name, $slug, $description);
        }catch (\Exception $e){
            if($app['debug']){
                $app['monolog']->addError($e->getTraceAsString());
            }
            $res = false;
        }
        if($res){
            return $app->json($res);
        }else{
            return $this->returnError(array('message'=>'Problème à la création du quiz'), 500);
        }

    }

    public function deleteQuiz(Application $app, Request $req){
        if(! $app['security']->isGranted('ROLE_ADMIN')){
            return $app->json('Vous n\'avez pas les droits nécéssaires pour effectuer cette action', 403);
        }
        $quiz_id = $req->get('id');
        $app['monolog']->addInfo("deleting quiz ".$quiz_id);
        if($quiz_id == ''){
            return $this->returnError(array('message'=>'Pas de quiz_id dans la requête'), 400);
        }

        try{
            $app['quiz_manager']->deleteQuiz($quiz_id);
            // removes backup files
            $dir = $app['params.save_path'].$quiz_id;
            if(is_dir($dir)){
                $it = new \RecursiveDirectoryIterator($dir,\RecursiveDirectoryIterator::SKIP_DOTS);
                $files= new \RecursiveIteratorIterator($it,\RecursiveIteratorIterator::CHILD_FIRST);
                foreach($files as $file){
                    if($file->getFileName() != '.' && $file->getFileName() != '..'){
                        if($file->isDir()){
                            rmdir($file->getRealPath());
                        }else{
                            unlink($file->getRealPath());
                        }
                    }
                }
                rmdir($dir);
            }


            return $this->returnSuccess(array('message'=>'ok'));
        }catch (\Exception $e){
            if($app['debug'])$app['monolog']->addError($e->getTraceAsString());
            return $this->returnError('problème lors de la suppression du quiz '.$quiz_id, 500);
        }
    }

    public function editQuiz(Application $app, Request $req){
        $id = $req->get('id');
        $name = $req->get('name');
        $description = $req->get('description');

        if($name=='' || $id==''){
            return $this->returnError('Le quiz doit avoir un nom', 500);
        }
        $data = array('id'=>$id,'name'=>$name,'description'=>$description);
        try{

            $res = $app['quiz_manager']->updateQuiz($data);
            return $app->json($res);
        }catch(\Exception $e){
            if($app['debug']){
                if($app['debug'])$app['monolog']->addError($e->getTraceAsString());
            }
            return $this->returnError(array('message'=>'Impossible de mettre le quiz à jour'), 500);
        }
    }

    public function duplicateQuiz(Application $app, Request $req){
        $quizId = $req->get('quiz_id');
        if($quizId == ''){
            return $this->returnError('Pas d\'id pour le quiz à dupliquer', 400);
        }
        try{
            $new_quiz_id = $app['quiz_manager']->duplicateQuiz($quizId);
            if($new_quiz_id != '' && $new_quiz_id != null){
                $quiz = $app['quiz_manager']->getQuizById($new_quiz_id);
                if($quiz != '' && $quiz != null && !empty($quiz)){
                    return $this->returnSuccess($quiz, 200);
                }else{

                }
            }
        }catch(Exception $e){
            if($app['debug']){
                $app['monolog']->addError($e->getMessage());
                $app['monolog']->addError($e->getTraceAsString());
            }
        }
        return $this->returnError('problème à la duplication du quiz',500);

    }

    public function restoreRevision(Application $app, Request $req){
        $revId = $req->get('rev_id');

        if($revId == null || $revId=='' ){
            return $this->returnError('id de révision manquant',400);
        }
        $rev = $app['quiz_manager']->retrieveRevision('',$revId,"");

        if(empty($rev) || $rev == null || $rev == ''){
            return $this->returnError('pas de révision pour cet id', 400);
        }


        $app['quiz_manager']->clearTablesForRestore($rev['quiz_id']);

        $res = $app['json_parser']->restore($rev);
        $app['quiz_manager']->updateProdRevision($rev['id'], $rev['quiz_id']);
        return $this->returnSuccess();
    }

    public function deleteRevision(Application $app, Request $req){
        $revId = $req->get('rev_id');

        if($revId == null || $revId==''){
            return $this->returnError('impossible de récupérer l\'id', 400);
        }

        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];
        $rev = $qm->retrieveRevision("",$revId,"");

        if($rev['prod'] == 1){
            return $this->returnError('Impossible de supprimer la révision actuellement utilisée',400);
        }

        $res = $app['quiz_manager']->deleteRevision($revId);

        if($res == false){
            return $this->returnError('Impossible de supprimer cette révision',500);
        }

        return $this->returnSuccess();
    }

    /**
     * @param Application $app
     * @param Request $req
     * @param bool $update
     * @return Response
     *
     * Creates the node in DB and saves the file on disk
     */
    protected function uploadFile(Application $app, Request $req, $update=false){

        $name = $req->get('name');
        $parentId = $req->get('parent_id');
        $quizId = $req->get('quiz_id');
        if($name == '' || $quizId==''){
            return new Response(
                json_encode(array('message'=>'Le nom et/ou le quizId sont obligatoire')), 400,
                array('Content-Type'=>'text/html')
            );
        }
        $file = $req->files->get('file_text');

        if($file == ''){
            return new Response(json_encode(array('message'=>'Pas de fichier joint')),400);
        }

        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];

        $quiz = $qm->getQuizById($quizId);
        if(! isset($quiz['id'])){
            $app['monolog']->addError('quiz id '.$quizId);

            return new Response(json_encode(array('message'=>'Impossible de retrouver le quiz avec l\'id '.$quizId)),500);
        }
        $position = $req->get('position') == '' ? 0:$req->get('position');


        $newNodeId = $qm->insertHelpContents(array('type'=>'file','position'=>$position, 'parent_id'=>$parentId,'name'=>$name));


        if(! is_dir($app['params.upload_path'].$quizId.'/'.$parentId)){
            mkdir($app['params.upload_path'].$quizId.'/'.$parentId, 0777,true);
        }
        $savePath = $app['params.upload_path'].$quizId.'/'.$parentId;
        $filename = $app['slugify']($name);

        try{
            // move file on disk
            $file->move($savePath, $newNodeId.'.pdf');


            // create route for node object
            $context = new RequestContext();
            $context->fromRequest($req);
            $ugen = new UrlGenerator($app['routes'], $context);
            $route = $ugen->generate('front_file', array('quizSlug'=>$quiz['slug'],'parent_id'=>$parentId,'id'=>$newNodeId));
            if($route == null){
                $this->deleteHelpContent($app,$newNodeId);
                return new Response(json_encode(array("message"=>"impossible de générer l'url du fichier")),500);
            }
            // data parameters
            $data['path']=$route;
            $data['shortname']=$filename;
            $data['node_id'] = $newNodeId;
            // adds additional info to help_content
            $qm->updateHelpContent($data);


        }catch(FileException $e){
            if($app['debug'])$app['monolog']->addError($e->getMessage());
            $this->deleteHelpContent($app,$newNodeId);
            return new Response(json_encode(array('message'=>'Un problème est survenu pendant l\'upload du fichier')), 500, array('Content-Type'=>'text/html'));
        }catch(Exception $e){
            if($app['debug'])$app['monolog']->addError($e->getMessage());
            $this->deleteHelpContent($app,$newNodeId);
            return new Response(json_encode(array('message'=>'une erreur terrible est survenue pendant l\'envoi du fichier')), 500, array('Content-Type'=>'text/html'));
        }
        //ATTENTION : La réponse DOIT être de type text/html et non pas application/json même si c'est du json dedans !
        //CF : http://stackoverflow.com/questions/4859502/upload-files-by-extjs-ajax-request-and-asp-net-mvc-without-reload-page-ajax-sty
        $response = array(
            'file_name' =>$newNodeId.'.pdf',
            'name' => $name,
            'parent_id' => $req->get('parent_id'),
            'node_id' => 'c_'.$newNodeId, //$req->get('node_id'),
            'path' => $route,
            'slug'=>$filename.'.pdf'
        );

        return new Response(json_encode($response),201);
    }

    public function updateFileNode(Application $app, Request $req){
        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];
        $nodeId = $req->get('id');

        $name = $req->get('name');
        $quizId = $req->get('quiz_id');
        $file = $req->files->get('file_text');

        if($nodeId == ''){
            return $this->returnError('No node id',400);
        }
        if($quizId == ''){
            return $this->returnError('No quiz id',400);
        }

        if(strpos($nodeId,'c_')!== false){
            $nodeId = intval(substr($nodeId,2));
        }

        try{
            if($name != ''){
                $shortname = $app['slugify']($name);
                $qm->updateHelpContent(array('node_id'=>$nodeId, 'name'=>$name, 'shortname'=>$shortname));
            }

            if($file != ''){
                $help_node = $qm->retrieveHelpNodeFromContent($nodeId);
                // $parent = $qm->retrieveParent($help_node->getId());
                $path = $app['params.upload_path'].$quizId.'/'.$help_node->getId();
                if(!is_dir($path)){
                    if($app['debug'])$app['monolog']->addError($path.' n\'existe pas');
                    return new Response(json_encode(array('message'=>'Impossible d\'envoyer le fichier')),500);
                }
                $file->move($path, $nodeId.'.pdf');
            }

            $fileNode = $qm->retrieveContentAsNode($nodeId);
        }catch(Exception $e){
            if($app['debug']){
                $app['monolog']->addError($e->getMessage());
                $app['monolog']->addError($e->getTraceAsString());
            }
            return new Response(json_encode(array('message'=>'Une erreur est survenue pendant la mise à jour du fichier')),500);
        }


        return new Response(json_encode(array(
                'file_name' =>$nodeId.'.pdf',
                'name' => $name,
                'parent_id' => $help_node->getId(),
                'node_id' => 'c_'.$nodeId,
                'path' => $fileNode->getPath(),
                'slug'=>$nodeId.'.pdf'
            )
        ));
    }

    /**
     * Specific method to add a link to an help node (using new help_contents table)
     * @param Application $app
     * @param array $req
     * @return a "false" node (help_content node which isn't in the tree)
     */
    public function addUrl(Application $app, $data){
        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];

        try{
            $id = $qm->insertHelpContents($data);

            // return help_content as Node
            $node = $qm->retrieveContentAsNode($id);
            $node->setId('c_'.$node->getId());
        }catch(Exception $e){
            if($app['debug'])$app['monolog']->addError($e->getTraceAsString());
            return $this->returnError("impossible de retrouver le contenu ", 500);
        }

        return $app->json($node->serializeJson(),201);
    }

    public function  deleteHelpContent(Application $app, $id){
        /** @var QuizRepository $qr */
        $qr = $app['quiz_manager'];
        try{
            $res = $qr->deleteHelpContent($id);
        }catch (Exception $e){
            if($app['debug'])$app['monolog']->addError($e->getTraceAsString());
            return $this->returnError('Impossible de supprimer ce contenu', 500);
        }
        return $this->returnSuccess(array('node_id'=>$id),200);
    }

    public function publish(Application $app, Request $req){
        $id = $req->get('quiz_id');
        if($id == ''){
            return $this->returnError('pas de quiz_id',500);
        }
        $jp = $app['json_parser'];
        // 1. validate
        $valid = $jp->validate($id); //QuizValidator::validate($app, $id);
        if(is_array($valid)){

            $str = implode(', ', $valid);
            return $this->returnError('Le quiz n\'est pas valide: ces noeuds devraient avoir des aides ou des choix '.$str,400);
        }
        // 2. publish
        try{
            $app['quiz_manager']->publishQuiz($id);
        }catch(Exception $e){
            return $this->returnError($e->getMessage(),500);
        }


        // 3. dump json file and save on the file system
        $json = $app['quiz_manager']->retrieveTree($id);
        $dirname = $app['params.save_path'].$id.'/';
        $filename = 'quiz_data_'.time().'.json';

        if(!is_dir($dirname)){
            mkdir($dirname);
        }
        $h = fopen($dirname.$filename,'w+');

        if($h === false){
            return $this->returnError('impossible de sauvegarder le fichier', 500);
        }
        $res = fwrite($h,$json);
        fclose($h);
        if(file_exists($dirname.$filename)){
            $msg = 'ok';
        }else{
            $err = error_get_last();
            return $this->returnError($err['message'],500);
        }

        // 4. add revision row in db
        $rev = $app['quiz_manager']->addQuizRevision($id,$filename,'',1);

        if($rev === false){
            return $this->returnError('impossible d\'enregistrer la révision',500);
        }
        return $this->returnSuccess($filename);
    }

    public function unpublish(Application $app, Request $req){
        $quizId = $req->get('quiz_id');
        if($quizId == null || ! $quizId){
            return $this->returnError('Quiz non trouvé avec cet identifiant, impossible de le repasser en brouillon', 500);
        }
        $app['quiz_manager']->unpublishQuiz($quizId);

        return $this->returnSuccess('Opération effectuée');

    }

    /**
     * @param Application $app
     * @param $quizId - l'id du quiz
     *
     * fetches all help wordings for a quiz
     */
    public function getHelpList(Application $app, $quizId){
        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];
        try{
            $res = $qm->getHelpList($quizId);
        }catch(Exception $e){
            if($app['debug'])$app['monolog']->addError($e->getTraceAsString());
            return $app->json(array());
        }
        return $app->json($res);
    }

    public function isAuthenticated(Application $app) {

    }


    /**
     * @param Request $req
     * @param array $params
     * @return array
     *
     * fetches needed parameters from request object
     * $params acts as a filter
     */
    private function retrieveParamsFromRequest(Request $req, $params=array()){
        if(empty($params)){
            $params = $this->params;
        }
        $parameters = array();

        foreach($params as $param){
            if($req->get($param) != "" || $param == 'shortname'){
                $parameters[$param] = $req->get($param);
            }

        }

        return $parameters;
    }

    private function returnError($msg="", $err){
        switch($err){
            case 400:{
                $msg = ($msg=="")?"Pas assez de paramètres":$msg;
                break;
            }
            case 500:{
                $msg = ($msg=="")?"Erreur":$msg;
                break;
            }
        }
        return new Response(json_encode(array('message'=>$msg)),$err,array('Content-Type'=>'application/json'));
    }

    private function returnSuccess($data = null, $code = 200) {
        if($data) {
            if(is_array($data))
                return new Response(json_encode($data), $code);
            if(is_string($data))
                return new Response(json_encode(array('message' => $data)), $code);
        }

        return new Response(json_encode(array('message' => 'ok')), $code);
    }
}