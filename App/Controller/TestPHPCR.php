<?php
/**
 * Greg Berger
 *
 * Date: 26/12/13
 * Time: 16:40
 */

namespace App\Controller;

use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;

class TestPHPCR implements ControllerProviderInterface {


    /**
     * Returns routes to connect to the given application.
     *
     * @param Application $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */
    public function connect(Application $app) {
        $index = $app['controllers_factory'];
        $index->get('/','App\Controller\TestPHPCR::index');
        $index->get('/create/{parent}/{name}','App\Controller\TestPHPCR::create');
        return $index;
    }

    public function create(Application $app, $parent, $name){
        /** @var \Jackalope\Session $session */
        $session = $app['phpcr'];

        $root = $session->getNode('/caca');
        // echo $root->getName();

        $child = $root->addNode($name);
        $app['phpcr']->save();

        return $this->index($app);

    }

    public function index(Application $app){
        /** @var \Jackalope\Node $root */
        $root = $app['phpcr']->getNode('/');
        echo '<ul>';
        foreach($root->getNodes() as $node){
            echo '<li>'.$node->getName().'</li>';
        }
        echo '</ul>';
        /*
        $child = $root->addNode('test');
        $app['phpcr']->save();
        */

        return $app->json('ok');
    }
}