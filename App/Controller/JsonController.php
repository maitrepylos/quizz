<?php
/**
 * Created by Paperpixel.
 * www.paperpixel.net
 */

namespace App\Controller;

use App\Model\DataVizRepository;
use App\Model\QuizRepository;
use Silex\Application;
use Silex\ControllerCollection;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class JsonController implements ControllerProviderInterface {
    /**
     * Returns routes to connect to the given application.
     *
     * @param Application $app An Application instance
     *
     * @return ControllerCollection A ControllerCollection instance
     */

    private $jsonStoragePath = '/../../public_html/data/json/';

    public function connect(Application $app)
    {
        $index = $app['controllers_factory'];

        $index->get("/{quizName}/last", 'App\Controller\JsonController::retrieveLast')->bind('jsonRetrieveLast');
        $index->get("/{quizName}/edit", 'App\Controller\JsonController::edit')->bind('jsonEdit');
        $index->get("/{quizName}/{timestamp}", 'App\Controller\JsonController::retrieveByTs')->bind('jsonRetrievebyTimestamp');
        $index->get("/{quizId}/last", 'App\Controller\JsonController::retrieveLast')->bind('retrieveLast');
        $index->get("/{timestamp}", 'App\Controller\JsonController::retrieveByTs')->bind('retrievebyTimestamp');
        $index->get('/dataViz/{quizName}/{rev_id}', 'App\Controller\JsonController::getForVizualisation')->bind('getForViz');

//        $index->get("/", 'App\Controller\JsonController::retrieveLast')->bind('default');
        return $index;
    }

    /**
    *    Returns content for the last json
    *    @param quizName
    *    @return http200 containing the json
    */

    public function retrieveLast(Application $app, Request $req, $quizName){
        $qm = $app['quiz_manager'];

        error_log("retrieve");

        $res = $qm->editQuiz($quizName);
        return new Response($res, 200, array("Content-Type"=>'application/json'));       
        // if(file_exists(__DIR__ . $this->jsonStoragePath . $quizName . ".json"))
        //     $response = new Response(
        //         file_get_contents(__DIR__ . $this->jsonStoragePath . $quizName . ".json"),
        //         200,
        //         array("Content-Type" => 'application/json')
        //     );
        // else
        //     $response = new Response(
        //         "Quiz does not exist",
        //         404
        //     );

     //   return $response;
     //}
    
    // public function retrieveLast(Application $app){
    //     /** @var QuizRepository $qm */
         
       
    //     $subRequest =Request::create($app['url_generator']->generate('api_tree', array('quizId'=>$quizId)), 'GET', $req->request->all(), $req->cookies->all(), $req->files->all(),$req->server->all());
    //     return $app->handle($subRequest, HttpKernelInterface::SUB_REQUEST);
       
        
    //     //TODO retrieve correct json ( last one )
    //     $response = new Response(
    //         file_get_contents(__DIR__ . $this->jsonStoragePath . "20131122.json"),
    //         200,
    //         array("Content-Type" => 'application/json')
    //     );
        
    //     return $response;
        
    }

    public function edit(Application $app, Request $req, $quizName)
    {
        //TODO : whatever
        $app['quiz_manager']->editQuiz($quizName);
    }

    private function slugify($quizName)
    {
        $quizSlug = $quizName;
        return $quizSlug;
    }

    /**
    *    Returns URI for the specified json based on timestamp
    *    @param timestamp
    *    @return URI for specfied json
    */
    public function retrieveByTS(Application $app, $quizName, $timestamp, $viz=false)
    {


        if($timestamp == 'prod'){
            $data = $app['quiz_manager']->retrieveRevision($quizName, 'prod', '');
        }else{
            $data = $app['quiz_manager']->retrieveRevision($quizName,'',$timestamp);
        }
        $json = file_get_contents($app['params.save_path'].$data['quiz_id'].'/'.$data['json_filename']);
        $json = json_decode($json,true);
        if($viz){
         return $json;
        }
        return new JsonResponse($json, 200, array('Cache-Control'=>'no-cache, must-revalidate', 'Expires'=>'Mon, 26 Jul 1997 05:00:00 GMT'));

        /*
        //TODO retrieve correct json
        if(isset($timestamp))
        {
            if( true ) //TODO : check if timestamp mach a json
            {
                $response = new Response();
                $response->setContent(readfile(__DIR__ . $this->jsonStoragePath . "20131122.json"));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }
        }
        return new Response("Aint find that json !", 404);
        **/
    }

    public function getForVizualisation(Application $app, $quizName, $rev_id){
        $dvr = new DataVizRepository($app['db']);
        $json = $this->retrieveByTS($app,$quizName,$rev_id,true);

       // $resp = $dvr->mktree_array($json);

        return new JsonResponse($json, array('Cache-Control'=>'no-cache, must-revalidate'));
    }
}