<?php
/**
 * Created by Paperpixel.
 * www.paperpixel.net
 */

namespace App\Controller;

use App\Model\Entity\Node;
use App\Model\QuizRepository;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class QuizController implements ControllerProviderInterface {
    public function connect(Application $app)
    {
        $index = $app['controllers_factory'];
        $index->match("/", 'App\Controller\QuizController::index')->bind('home');
        $index->match("/admin", 'App\Controller\AdminController::index')->bind('admin_fwd');
        $index->get('/pdf', 'App\Controller\QuizController::getPdf');
        $index->match("/{quizSlug}", 'App\Controller\QuizController::quiz')->bind('front_quiz');
        $index->match("/{quizSlug}/selection", 'App\Controller\QuizController::selection')->bind('front_selection');
        $index->match("/{quizSlug}/selection/sendMail", 'App\Controller\QuizController::sendMail')->bind('selection_send_mail');
        $index->match("/{quizSlug}/selection/exportPDF/", 'App\Controller\QuizController::exportPDF')->bind('selection_export_PDF');
        $index->match("/file/{quizSlug}/{parent_id}/{id}", 'App\Controller\QuizController::showFile')->bind('front_file');
        return $index;
    }

    public function index(Application $app) {
        /** @var Request  $req */
        $req = $app['request'];

        $quizzes = $app['quiz_manager']->getQuizzes(true);
        $quiz = $app['quiz_manager']->getQuizById($app['cms_manager']->getOption('default_quiz'));
	// C. Gustin - 20150916
	// Redirection explicite vers l'URL du quiz par defaut.
	// Permet d'utiliser le paramètre segment=pageUrl dans les
	// statistiques noeud par noeud
	$redirect=$app["url_generator"]->generate('front_quiz',array('quizSlug'=>$quiz['slug']));
        return $app->redirect($redirect);
        // return $this->quiz($app, $quiz['slug']);
    }

    public function quiz(Application $app, $quizSlug) {

        $quiz = $app['quiz_manager']->getQuizBySlug($quizSlug);
        $preview = $app['request']->get('preview');
        // serves the quiz in preview mode only
        if (!$quiz || ($quiz['published'] == 0 && ! $preview)) {
            throw new NotFoundHttpException('Quiz not found');
        }
        $variables = array(
            'quiz' => $quiz,
            'colors' => $this->getColorsFromRequest($app['request'])
        );

        // if in admin and showing statistics
        if($app['request']->get('showstats') == "true") {
            $variables["showstats"] = true;
            $variables["startdate"] = $app['request']->get('startdate');
            $variables["enddate"] = $app['request']->get('enddate');
        }

        // hotfix to get the js retrieve the quiz being edited in case of previsualization [in iframe export]
        $variables['preview'] = $preview;

        /**
         * test for CMS page.
         * TODO dynamic variables
         */
        $variables['is_iframe'] = $app['request']->get('is_iframe');
        $variables['cms'] = $this->get_cms_variables($app);

        return $app['twig']->render('front\quiz.html.twig', $variables);
    }

    public function selection(Application $app, $quizSlug) {
        $quiz = $app['quiz_manager']->getQuizBySlug($quizSlug);
        if (!$quiz) {
            throw new NotFoundHttpException('Quiz not found');
        }
        return $app['twig']->render('front\selection.html.twig', array(
            'quiz' => $quiz,
            'colors' => $this->getColorsFromRequest($app['request']),
            'is_iframe' => $app['request']->get('is_iframe'),
            'returnUrl' => $this->getReturnUrl($app, $quizSlug),
            'cms' => $this->get_cms_variables($app)
        ));
    }

    public function showFile(Application $app, $quizSlug,$parent_id,$id){

        $quiz = $app['quiz_manager']->getQuizBySlug($quizSlug);
        $parent = $app['quiz_manager']->retrieveNode($parent_id);
        /** @var Node $file */
        $file = $app['quiz_manager']->retrieveNode($id);

        $filePath  = $app['params.upload_path'].$quiz['id'].'/'.$parent_id.'/'.$id;

        $help = $app['slugify']($parent->getName());

        if(! file_exists($filePath.'.pdf')){
            throw new FileNotFoundException($file->getShortname());
        }
        $stream = function () use($filePath){
            readfile($filePath.'.pdf');
        };
        $filename = $quizSlug.'-'.$help.'-'.$file->getShortname().'.pdf';
        return $app->stream($stream, 200,
            array('Content-Type'=> 'application/pdf', 'Content-Disposition'=>'filename='.$filename));

    }

    public function sendMail(Application $app, Request $req) {
        $helpIds = $req->get('helps');
        $email = $req->get('email');

        if(empty($email) || !$email) {
            return $app->json(array('message' => 'Pas d\'adresse e-mail renseignée.'), 400);
        }
        // TODO checker la validité de l'e-mail

        if(empty($helpIds)) {
            return $app->json(array('message' => 'Pas d\'aides sélectionnées'), 400);
        }

        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];
        $helps = array();
//            $helpIds = array(299,300,304);
        for($i = 0; $i < count($helpIds); $i++) {
            try{
                $helps[] = $qm->getHelpById($helpIds[$i]);
            }catch(Exception $e){
                $app['monolog']->addError($e->getTraceAsString());
                return $app->json(array('message'=>'Pas d\'aide trouvée avec cet id: '.$helpIds[$i]), 500);
            }
        }

        $html = $app['twig']->render('front\selection_export.html.twig', array('helps' => $helps));

        try{
            // $headers = sprintf('From: "%1$s"%1$s\r\n',$app['postmaster_from'] ,$app['postmaster_address']);
            $headers = sprintf('From: %s <%s>\r\n',$app['postmaster_from'],$app['postmaster_noreply']);
            $headers  .= 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $result = mail($email,'Votre sélection d\'aides', $html, $headers);
        }catch(Exception $e){
            $app['monolog']->addError($e->getMessage());
            $app['monolog']->addError($e->getTraceAsString());
            $result = false;
        }
        if($result) {
            return $app->json(array('message' => 'ok'));
        } else {
            return $app->json(array('message' => 'E-mail non envoyé (adresse: '.$email.').'), 500);
        }
    }


    /**
     * La méthode qui permet d'envoyer le PDF des aides
     *
     * @use classe DOMPDF v0.6.0-b3
     * @author : https://github.com/dompdf/dompdf
     *
     *
     * @param Application $app
     * @param $quizSlug - le nom du quiz
     * @param Request $req
     *
     *
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function exportPDF(Application $app, $quizSlug, Request $req) {
        $nodeIds = $req->get('node_ids');
        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];
        if(!empty($nodeIds)) {
            for($i = 0; $i < count($nodeIds); $i++) {
                try{
                    $helps[] = $qm->getHelpById($nodeIds[$i]);
                }catch(Exception $e){
                    $app['monolog']->addError($e->getTraceAsString());
                    throw new HttpException('Problème lors de la récupération des aides');
                }
            }
            $html = $app['twig']->render('front\selection_export.html.twig', array('helps' => $helps));
            try{
                $dompdf = new \DOMPDF();
                $dompdf->load_html($html, 'utf-8');
                $dompdf->render();
            }catch(Exception $e){
                $app['monolog']->addError($e->getTraceAsString());
                throw new HttpException('Problème lors de la génération du PDF');
            }
            // callback function that streams the content of the PDF
            $cb = function() use($dompdf, $quizSlug){
                return $dompdf->stream($quizSlug.'_'.date('d-m-Y', time()).'_selections.pdf', array('Attachment'=>1));
            };

            return $app->stream($cb,200,array('Content-Type'=> 'application/pdf', 'Content-Disposition'=>'filename='.$quizSlug.'_'.strftime('d-M-Y', time()).'_selections.pdf'));
        }else{
            throw new NotFoundHttpException('Aucune aide sélectionnée');
        }
    }


    public function getHelp(Application $app, $helpId){
        /** @var QuizRepository $qm */
        $qm = $app['quiz_manager'];
        try{
            $res = $qm->getHelpById($helpId);
        }catch(Exception $e){
            $app['monolog']->addError($e->getTraceAsString());
            return $app->json(array('message'=>'Pas d\'aide trouvée avec cet id: '.$helpId), 500);
        }
        return $app->json($res);
    }

    /**
     * get colors for front UI based on a string (eg. FF0000||00FF00|0000FF|)
     * @param $request
     * 0|node
     * 1|nodeHover
     * 2|nodeSelected
     * 3|action
     * 4|actionSelected
     * @return array|null
     */
    private function getColorsFromRequest($request) {
        $colors = '';
        $colorsRequest = $request->get('colors') ? explode('|', $request->get('colors')) : null;
        if(isset($colorsRequest)) {
            $colors = array();
            $colors['request'] = $request->get('colors');
            $colorsRequest[0] != '' ? $colors['node'] = $colorsRequest[0] : null;
            $colorsRequest[1] != '' ? $colors['nodeHover'] = $colorsRequest[1] : null;
            $colorsRequest[2] != '' ? $colors['nodeSelected'] = $colorsRequest[2] : null;
            $colorsRequest[3] != '' ? $colors['action'] = $colorsRequest[3] : null;
            $colorsRequest[4] != '' ? $colors['actionSelected'] = $colorsRequest[4] : null;
        }
        return $colors;
    }


    private function getReturnUrl($app, $quizSlug) {
        $referer = $app['request']->headers->get('referer');
        $req = array('quizSlug' => $quizSlug);
        if($referer) {
            preg_match_all('/([^?&=#]+)=([^&#]*)/',$referer,$m);
            $data=array_combine( $m[1], $m[2]);
            if($data['n'] && intval($data['n'])) {
                $req['n'] = $data['n'];
            }
        }
        if($app['request']->get('colors')) {
            $req['colors'] = $app['request']->get('colors');
        }
        if($app['request']->get('is_iframe')) {
            $req['is_iframe'] = $app['request']->get('is_iframe');
        }
        return $app['url_generator']->generate('front_quiz', $req);
    }

    private function get_cms_variables(Application $app) {

        $footer = $app['cms_manager']->getContentByType('footer');
        $links_data = $app['cms_manager']->getContentByType('link');
        $links = array();
        foreach($links_data as $link){
            $l = unserialize($link['content']);
            $l['id'] = $link['id'];
            $links[] = $l;

        }
        return array(
            "baseline" => "trouvons, collaborons, innovons !",
            "footer_text" => $footer[0]['content'],
            "links" => $links,
            'display_tools' => filter_var($app['cms_manager']->getOption('display_tools'),FILTER_VALIDATE_BOOLEAN),
            'display_footer' => filter_var($app['cms_manager']->getOption('display_footer'),FILTER_VALIDATE_BOOLEAN)
        );
    }
}
