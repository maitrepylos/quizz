<?php
/**
 * AST quiz - innovons.be
 * Created by PAPERPIXEL 
 * www.paperpixel.net
 * ${year}
 */

namespace App\Security;


use Doctrine\DBAL\Connection;
use Silex\Application;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface{
    private $conn;
    private $app;
    public function __construct(Connection $conn, Application $app){
        $this->conn = $conn;
        $this->app = &$app;
    }

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @return UserInterface
     *
     * @see UsernameNotFoundException
     *
     * @throws UsernameNotFoundException if the user is not found
     *
     */
    public function loadUserByUsername($username) {
        $sql  = "SELECT * FROM users WHERE ( email = :email OR username = :username ) AND register_key IS NULL AND activated = 1";
        $stmt = $this->conn->prepare($sql);
        $stmt->bindValue(':username', $username);
        $stmt->bindValue(':email', $username);
        $stmt->execute();
        if(! $user = $stmt->fetch()){

            throw new UsernameNotFoundException(sprintf("Username %s doesn't exist", $username));
        }
        return new \App\Security\User($user['username'], $user['password'], explode(',', $user['roles']), $user['email'], $user['id'], $user['register_key'], $user['activated'], $user['name'], $user['firstname'], $user['telephone']);
    }

    /**
     * Refreshes the user for the account interface.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the account is not supported
     */
    public function refreshUser(UserInterface $user) {
        if (!$user instanceof \App\Security\User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * Whether this provider supports the given user class
     *
     * @param string $class
     *
     * @return Boolean
     */
    public function supportsClass($class) {
        return $class === '\App\Security\User';
    }
}