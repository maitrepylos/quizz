<?php
/**
 * AST quiz - innovons.be
 * Created by PAPERPIXEL 
 * www.paperpixel.net
 * ${year}
 */

namespace App\Security;




class User implements \Symfony\Component\Security\Core\User\UserInterface {

    protected $id;
    protected $username;
    protected $pwd;
    protected $roles;
    protected $email;
    protected $key;
    protected $activated;
    protected $name;
    protected $firstname;
    protected $telephone;


    public function __construct($username='', $pwd='', $roles=array(), $email='', $id='', $key="", $activated="", $name="",$firstname="", $telephone=""){
        if(is_array($username)){
            $this->setFromArray($username);
        }else{
            $this->username = $email;
            $this->pwd = $pwd;
            $this->roles = $roles;
            $this->email = $email;
            $this->id = $id;
            $this->key = $key;
            $this->activated = $activated;
            $this->name=$name;
            $this->firstname=$firstname;
            $this->telephone=$telephone;
        }

    }

    public function setFromArray($params){
        foreach($params as $param=>$val){
            if($param == 'password') $this->pwd = $val;
            if(property_exists($this, $param)){
                $this->$param = $val;
            }
        }
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname) {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getFirstname() {
        return $this->firstname;
    }

    /**
     * @param string $key
     */
    public function setKey($key) {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getKey() {
        return $this->key;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $telephone
     */
    public function setTelephone($telephone) {
        $this->telephone = $telephone;
    }

    /**
     * @return string
     */
    public function getTelephone() {
        return $this->telephone;
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[] The user roles
     */
    public function getRoles() {
        return $this->roles !== null ? $this->roles : array('ROLE_USER');
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword() {
        return $this->pwd;
    }

    public function setPwd(\Silex\Application $app, $string){
        $encoder = $app['security.encoder_factory']->getEncoder($this);
        $this->pwd = $encoder->encodePassword($string, $this->getSalt());
    }

    /**
     * @param string $email
     */
    public function setEmail($email) {
        $this->email = $email;
    }

    /**
     * @param array $roles
     */
    public function setRoles($roles) {
        $this->roles = $roles;
    }

    /**
     * @param string $username
     */
    public function setUsername($username) {
        $this->username = $username;
    }


    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string The salt
     */
    public function getSalt() {
        //return md5($this->username.$this->email);
        return null;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername() {
        return $this->username;
    }
    public function getId(){
        return $this->id;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     *
     * @return void
     */
    public function eraseCredentials() {
//        $this->pwd = null;
    }

    public function getEmail(){
        return $this->email;
    }
    public function getRegKey(){
        return $this->key;
    }
    public function setRegKey($key){
        $this->key = $key;
    }
    public function isActivated(){
        return $this->activated;
    }
    public function setActivated($val){
        if($val === true || $val == 1){
            $this->activated = 1;
        }else{
            $this->activated = 0;
        }
    }


    public function toArray() {
        $roles = (is_array($this->roles))?$this->roles[0]:$this->roles;
        return array(
            "id" => $this->getId(),
            "activated" => intval($this->isActivated()),
            "email" => $this->getEmail(),
            "roles" => $roles,
            "name"=>$this->getName(),
            "firstname"=>$this->getFirstname(),
            "telephone"=>$this->getTelephone(),

        );
    }

    private function getUserHash(User $u){
        if(is_array($u->getRoles())){
            $roles = implode(',',$u->getRoles());
        }else{
            $roles = $u->getRoles();
        }
        $s = $u->getId().$u->getName().$u->getFirstname().$u->getEmail().$u->getTelephone().$roles;
        return md5($s);
    }

    public function equals(User $u){
        return $this->getUserHash($u) == $this->getUserHash($this);
    }

}
