<?php
/**
 * AST quiz - innovons.be
 * Created by PAPERPIXEL 
 * www.paperpixel.net
 */

namespace App\Model;
use Silex\Application;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

/**
 * Class JsonRepository
 * @package App\Model
 *
 * Class in charge of operations on json files (save, retrieve, etc)
 */
class JsonRepository {
    /** @var  Application $app */
    private $app;
    public function __construct(Application $app){
        $this->app = $app;
    }
    /**
     * Serves  json file containing a quiz
     *
     * @param $quizName
     * @param string $ts (optional) if no timestamp, the last published file will be retrieved
     */
    public function retrieveJSON($quizName, $ts=""){
        if(! $this->checkDirExists($quizName)){
            throw new InvalidArgumentException('can\'t find directory for quiz '.$quizName);
        }
    }

    /**
     * @param $quizName name of the quiz (directory)
     * @param mixed $ts timestamp, date object or "last"
     * @param String $json the json to be saved
     */
    public function saveJSON($quizName, $ts, $json){

    }

    private function checkDirExists($quizName){
        return is_dir($this->app['params.save_path'].$quizName);
    }

    private function checkFileExists($quizName, $ts){
        return file_exists($this->app['params.save_path'].$quizName.'/'.$ts.'.json');
    }

}