<?php
/**
 * Created by PhpStorm.
 * User: gberger
 * Date: 5/07/14
 * Time: 23:20
 */

namespace App\Model;


use Doctrine\DBAL\Statement;
use Doctrine\Tests\DoctrineTestCase;
use Silex\Application;
use Symfony\Component\Security\Acl\Exception\Exception;

class CMSRepository {
    /** @var  \Doctrine\DBAL\Connection */
    private $dbal;
    public function __construct(Application $app){
        $this->dbal = $app['db'];
    }

    /**
     * @return array
     */
    public function getOptions($quiz_id=""){
        $query = "SELECT * FROM config WHERE quiz_id ";
        /** @var \Doctrine\DBAL\Statement $stmt */
        $stmt = null;
        if($quiz_id){
            $query.=" = :q_id";
            $stmt = $this->dbal->prepare($query);
            $stmt->bindParam(':q_id',$quiz_id);
        }else{
            $query.= " IS NULL";
            $stmt = $this->dbal->prepare($query);
        }
        $stmt->execute();
        $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $res;
    }

    public function getOption($key,$quiz_id=0){
        $query = 'SELECT `value` FROM config WHERE `key` = :k AND quiz_id = :q_id';
        /** @var Statement $stmt */
        $stmt = $this->dbal->prepare($query);

        $stmt->bindParam(':k',$key);
        $stmt->bindParam(':q_id',$quiz_id);


        $stmt->execute();

        $res = $stmt->fetchColumn();

        return $res;
    }
    public function addOption(/** array */ $data){

        if(!isset($data['quiz_id']) || $data['quiz_id'] == null) $data['quiz_id'] = 0;

        $query = 'INSERT INTO config (`key`,`value`,quiz_id) VALUES (:k,:v,:q_id) ON DUPLICATE KEY UPDATE `value` = :v';
        $stmt = $this->dbal->prepare($query);

        $stmt->bindParam(':k',$data['key']);
        $stmt->bindParam(':v',$data['value']);
        $stmt->bindParam(':q_id',$data['quiz_id']);

        try{
            $stmt->execute();

        }catch(Exception $e){
            return false;
        }

        return true;
    }

    public function updateOption($id, /** array */ $data){

    }

    /**
     * @param string $opt_id
     * @return bool
     */
    public function deleteOption($opt_id){
        try{
            $this->dbal->delete('config', array('id'=>$opt_id));
        }catch(Exception $e){
            return false;
        }
        return true;
    }

    /**
     * @param string $quiz_id
     * @param array $data
     */
    public function createContent(/** array */$data){
        $fields = array('type','content','slug','parent', 'quiz_id');

        if(!isset($data['quiz_id'])) $data['quiz_id'] = 0;

        foreach($fields as $key => $v){
            // if field is in data array
            if(array_key_exists($v,$data)){

                // add named parameter to query
                $query_data[] = ':'.$v;
            }else{
                unset($fields[$key]);
            }

        }

        $query = 'INSERT INTO cms_content ('.implode(',',$fields).') VALUES (:'.implode(',:',$fields).')';

        $stmt = $this->dbal->prepare($query);

        foreach($query_data as $qd){
            $d = explode(':',$qd);
            $stmt->bindParam($qd,$data[$d[1]]);
        }


        $stmt->execute();
    }



    public function getContentByType($type, $quiz_id=0){
        $query = "SELECT * FROM cms_content WHERE type = :t";

        if($quiz_id !== "" && $quiz_id != null){
            $query .=" AND quiz_id = :q_id";
            $stmt = $this->dbal->prepare($query);
            $stmt->bindParam(':q_id', $quiz_id);
            $stmt->bindParam(':t', $type);
            $stmt->execute();
        }else{
            $stmt = $this->dbal->prepare($query);
            $stmt->bindParam(':t', $type);
            $stmt->execute();
        }

        $res = $stmt->fetchAll(\PDO::FETCH_ASSOC);


        return $res;
        }

    public function deleteContent($content_id){
        try{
            $this->dbal->delete('cms_content', array('id'=>$content_id));
        }catch(Exception $e){
            error_log($e->getMessage());
            return false;
        }
        return true;

    }

    public function updateContent($id, /** array */$data){
        $fields = array('type','content','quiz_id','slug','parent');

        for($i = 0; $i < count($fields); $i++){
            // if field is in data array
            if(array_key_exists($fields[$i],$data)){
                // add named parameter to query
                $query_data[] = $fields[$i].' = :'.$fields[$i];
            }
        }

        $query = 'UPDATE cms_content SET '.implode(',',$query_data).' WHERE id = :id';

        $stmt = $this->dbal->prepare($query);

        foreach($query_data as $qd){
            $d = explode(' = :',$qd);
            $stmt->bindParam(':'.$d[0],$data[$d[0]]);
        }
        $stmt->bindParam(':id',$id);
        $stmt->execute();


    }

} 