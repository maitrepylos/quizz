<?php
/**
 * AST quiz - innovons.be
 * Created by PAPERPIXEL
 * www.paperpixel.net
 * ${year}
 */

namespace App\Model;


use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\PDOStatement;

class UserRepository {
    /** @var Connection $connection */
    protected $connection;

    public function __construct(Connection $conn){
        $this->connection = $conn;
    }
    public function getUserById($user_id){
        $db = $this->connection;
        $sql = "SELECT * FROM users WHERE id = ?";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(1, $user_id);
        $stmt->execute();
        $user =  $stmt->fetch(\PDO::FETCH_ASSOC);

        if(!empty($user)){
            return new \App\Security\User($user);
        }else{
            return false;
        }
    }


    public function getAllUsers($id=""){
        $sql = "SELECT id, `name`, firstname, email, telephone, roles, activated  FROM users";

        if($id != ""){
            $sql.=" WHERE id <> :id";
            $users = $this->connection->fetchAll($sql, array(':id'=>$id));
        }else{

            $users = $this->connection->fetchAll($sql);
        }

        return $users;

    }

    public function getUserByMail($mail) {
        $db = $this->connection;
        $sql = "SELECT * FROM users WHERE email = ?";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(1, $mail);
        $stmt->execute();
        $user =  $stmt->fetch(\PDO::FETCH_ASSOC);
        return new \App\Security\User($user);
    }

    public function createUser(array $data){


        $db = $this->connection;

        $sql = "INSERT INTO users (name, firstname, telephone, email, roles) VALUES (:name, :firstname, :telephone, :email, :roles)";

        $stmt = $db->prepare($sql);
        $stmt->bindParam(':name', $data['name']);
        $stmt->bindValue(':firstname', $data['firstname']);
        $stmt->bindValue(':telephone', $data['telephone']);
        $stmt->bindValue(':email', $data['email']);
        $stmt->bindValue(':roles', $data['roles']);

        $res = $stmt->execute();

        if($res){
            return $this->getUserById($db->lastInsertId());
        } else{
            return false;
        }
    }

    public function createAdmin(array $data){
        $db = $this->connection;

        $sql = "INSERT INTO users (email, roles) VALUES (:mail,'ROLE_ADMIN')";
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':mail', $data['email']);
        $res = $stmt->execute();
        if($res){
            return $db->lastInsertId();
        } else{
            return false;
        }
    }


    public function updateUser($user){
        if(is_array($user)){
            $data = $user;

        }else if($user instanceof \App\Security\User){
            $data['username'] = $user->getUsername();
            $data['password'] = $user->getPassword();
            if(is_array($user->getRoles())){
                $data['roles'] = implode(',',$user->getRoles());
            }else{
                $data['roles'] = $user->getRoles();
            }
            $data['email'] = $user->getEmail();
            $data['name'] = $user->getName();
            $data['telephone'] = $user->getTelephone();
            $data['firstname'] = $user->getFirstname();
            $data['register_key'] = $user->getRegKey();
            $data['activated'] = $user->isActivated();
            $data['id'] = $user->getId();
        }else{
            return false;
        }
        $db = $this->connection;
        $res =  $db->update('users',$data,array("id"=>$user->getId()));

        if($res){
            return $this->getUserById($data['id']);
        }else{
            return false;
        }

    }

    public function getByMailAndKey($email, $key){
        $db = $this->connection;
        $sql = "SELECT * FROM users WHERE email LIKE :mail AND register_key LIKE :reg_key";
        /** @var PDOStatement $stmt */
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':mail', $email);
        $stmt->bindValue(':reg_key', $key);
        // $stmt->setFetchMode(\PDO::FETCH_CLASS);
        $stmt->execute();

        $res = $stmt->fetch();

        if($res){
            if(is_array($res) && ! empty($res)){
                // return new \App\Security\User($res['username'],$res['password'],explode(',',$res['roles']),$res['email'],$res['id'],$res['register_key'],$res['activated']);
                return new \App\Security\User($res);
            }elseif($res instanceof \App\Security\User){
                return $res;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }



    public function registerUser($email, $key, $pwd){

    }

    public function setLastLogin($user_id){
        $db = $this->connection;
        $last_login = new \DateTime('now');
        $last_login = $last_login->format('Y-m-d H:i:s');
        return $db->update('users', array('last_login'=>$last_login ), array('id'=>$user_id));
    }

    public function deleteUser($user_id) {
        $db = $this->connection;
        return  $db->delete('users', array('id'=>$user_id));
    }

    /* encodage du mot de passe
        $user = \App\Model\UserDAO::getUserById($app, 1);
        var_dump($user);
        $encoder = $app['security.encoder_factory']->getEncoder($user);
        $password = $encoder->encodePassword('pwd', $user->getSalt());
        echo $password;
        die();
    */

}