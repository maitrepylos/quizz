<?php
/**
 * Greg Berger
 *
 * Date: 17/11/13
 * Time: 04:36
 */

namespace App\Model;
require_once __DIR__.'/../Model/Entity/Node.php';

use App\Model\Entity\Node;
use App\Model\Entity\Tree;
use App\Security\User;
use Doctrine\DBAL\Statement;
use Silex\Application;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\CssSelector\Parser\Token;

use App\lib\JsonParser;

class QuizRepository {
    /** @var  \Doctrine\DBAL\Connection */
    private $dbal;
    /** @var  Application */
    private $app;
    /** @var  JsonParser */
    private $jsonParser;

    public function __construct(Application $app){
        // error_log('constructing quizrepo');
        $this->dbal = $app['db'];
        $this->app = $app;
        $this->jsonParser = $app['json_parser'];
        // $this->setupRevNumberAndUser();
    }

    public function retrieveNodes(){
        $sql = "
            SELECT nav.ancestor as parent, nav.descendant as child, n.*, p.type,p.name,p.text
            FROM nodes n
            JOIN navigation nav ON n.id = nav.descendant
            JOIN node_properties p ON n.meta_id = p.id
            WHERE nav.ancestor = 1
            ";
        $res = $this->dbal->fetchAll($sql);
        return $res;
    }

    /**
     * @param $nodeId
     * @return Node
     */
    public function retrieveNode($nodeId){
        $sql = 'SELECT n.id,n.position,n.quiz_id, p.type, p.name, p.shortname, p.text,p.path FROM nodes n JOIN node_properties p ON n.meta_id = p.id WHERE n.id = :id ';
        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(':id', $nodeId);
        //$stmt->setFetchMode(\PDO::FETCH_CLASS, '\App\Model\Entity\Node');
        $stmt->execute();
        $res = $stmt->fetchAll();
        $n=new Node($res[0]);
        return $n;//->serializeJson();
    }

    public function retrieveParent($nodeId){
        $sql = "SELECT n.*, np.* FROM navigation na
                JOIN nodes n ON na.ancestor = n.id
                JOIN node_properties np ON n.meta_id = np.id
                WHERE na.descendant = :id  AND depth = 1; ";

        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(':id',$nodeId);
        $stmt->execute();
        $res = $stmt->fetchAll();

        if(! empty($res)){
            return new Node($res[0]);
        }else{
            return null;
        }

    }

    /**
     * @param $quizId
     * @param bool $json
     * @return array|string
     */
    public function retrieveTree($quizId, $json=true){
        $sql = "
        SELECT n.id, n.quiz_id as quizId, nav.ancestor as root_node, n.position, n.meta_id, p.type,p.name,p.shortname,p.path,p.text,
            (SELECT COUNT(ancestor)-1 FROM navigation WHERE descendant = n.id GROUP BY descendant) as depth,
            GROUP_CONCAT(crumbs.`ancestor` ORDER BY crumbs.depth DESC) AS breadcrumbs,
            (SELECT GROUP_CONCAT(n2.id)
              FROM nodes n2 JOIN node_properties np
              ON n2.meta_id = np.id
              WHERE (np.type = 'url' OR np.type = 'help' OR np.type = 'file') AND n2.meta_id = n.meta_id AND n2.id <> n.id
            ) as related
            FROM nodes n
              JOIN navigation nav ON n.id = nav.descendant
              JOIN navigation AS crumbs ON crumbs.`descendant` = nav.`descendant`
              JOIN node_properties p ON n.meta_id = p.id
            WHERE nav.ancestor IN (
                  SELECT n.ancestor
                  FROM navigation n
                  LEFT OUTER JOIN navigation n1 ON n1.descendant = n.descendant AND n1.ancestor <> n.ancestor
                  WHERE n1.ancestor IS NULL
            )
            AND n.quiz_id = ?
            GROUP BY n.id
              ORDER BY depth,breadcrumbs;
        ";

        $res = $this->dbal->fetchAll($sql, array((int)$quizId));


        if($json){
            $nestedNodes = $this->mktree_array($res);

            if(defined('JSON_PRETTY_PRINT'))
                return json_encode($nestedNodes[0], JSON_PRETTY_PRINT); // pas dispo avant PHP 5.4
            else
                return json_encode($nestedNodes[0]);
        }
        return ($res);

    }

    /*
     * Formats the result set recursively
     */
    public function mktree_array(&$arr, $id = 0)
    {
        $result = array();

        foreach ($arr as $a) {
            $breadcr = explode(',',$a['breadcrumbs']);

            $related = explode(',',$a['related']);

            if(isset($breadcr[$a['depth']-1])){
                $parentId = $breadcr[$a['depth']-1];
            }else{
                $parentId = null;
            }
            $del = array_pop($breadcr);
            $a['breadcrumbs'] = array_map('intval',$breadcr);

            // if sql field was null
            if(empty($related[0])){
                $a['related'] = array();
            }else{
                $a['related'] = array_map('intval',$related);
            }
            $a['order'] = $a['position']+1;
            $a['id'] = intval($a['id']);
            unset($a['position']);
            if ($id == $parentId) {
                $children = $this->mktree_array($arr, $a['id']);
                uasort($children, function($a,$b){
                    if(! isset($a['order']) || !isset($b['order']) || ($a['order'] == $b['order'])){
                        return 0;
                    }
                    return ($a['order'] < $b['order']) ? -1:1;
                });
                foreach($children as $child){

                    if($child['type'] == 'question' || $child['type'] == 'popup'){
                        $a[$child['type']] = $child;

                    }else if($child['type'] == 'related'){
                        $a[$child['type']][]  = $child;
                    }elseif($child['type']=='help'){
                         // additional processing to manage help_contents
                        $contents = $this->getHelpContentForMeta($child['meta_id']);
                        foreach($contents as &$content){
                            $content['breadcrumbs']= array_merge($child['breadcrumbs'],(array)$child['id']);
                            $content['depth'] = $a['depth']+1;
                            $child[$content['type'].'s'][] = $content;

                        }
                         $a['helps'][] = $child;
                    }else {
                        $a[$child['type'].'s'][]  = $child;
                    }

                }
                if(! isset($a['choices'])) $a['choices'] = array();
                $result[] = $a;
            }
        }

        return $result;
    }


    private function getHelpContentForMeta($meta){
        $sql = "SELECT CONCAT('c_',id) as id,type,name,text,position,meta_id,shortname,path FROM help_contents WHERE meta_id = :meta";

        $res = $this->dbal->fetchAll($sql,array(':meta'=>$meta));

        return $res;
    }

    public function addNode($parent_id=null, $data){
        $sql = '
            INSERT INTO nodes (position, status, quiz_id, meta_id)
              VALUES (:pos,:status,:quiz, :meta_id);
        ';
        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(':pos',$data['position']);
        $stmt->bindParam(':status',$data['status']);
        $stmt->bindParam(':quiz',$data['quiz_id']);
        // if node is a duplicate, we take meta id as a parameter
        $metaId = isset($data['meta_id'])?$data['meta_id']:null;
        $stmt->bindParam(':meta_id',$metaId);
        try{
            $this->dbal->beginTransaction();
            $stmt->execute();
            $newNodeId = $this->dbal->lastInsertId();
            $this->dbal->commit();
        }catch(\PDOException $e){
            $this->dbal->rollback();
            throw new Exception('Une erreur s\'est produite durant l\'ajout du noeud');
        }
        if($parent_id == null || $parent_id == ''){
            $parent_id = $newNodeId;
        }

        $this->updateNavigation($newNodeId, $parent_id);
        return $newNodeId;
    }

    public function updateNavigation($new_node_id, $parent_id){
        $sql = '
          INSERT INTO navigation (ancestor, descendant, depth)
          SELECT n.ancestor, :new_node, n.depth+1
          FROM navigation AS n
          WHERE n.descendant = :parent_id
          UNION ALL
          SELECT :new_node, :new_node, 0;

        ';

        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(':new_node', $new_node_id);
        $stmt->bindParam(':parent_id',$parent_id);

        $stmt->execute();
    }

    /**
     * @param $id   int
     * @param $cascade boolean
     */
    public function deleteNode($node_id,$cascade){

        if($cascade){
            $sql = 'DELETE n,nav FROM nodes n JOIN navigation nav  ON n.id = nav.descendant WHERE nav.ancestor = :node_id';
        }else{
            $sql = 'DELETE FROM nodes WHERE id = :node_id';
        }

        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(":node_id",$node_id);
        $stmt->execute();

    }

    /**
     *
     * Creates a new node and copy meta data in it
     * @param $node_id
     * @param $new_parent
     */
    public function duplicateNode($node_id){
        $sql = "INSERT INTO nodes (meta_id, quiz_id, position) (SELECT meta_id,quiz_id,position FROM nodes WHERE id = :node_id)";
        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(':node_id',$node_id);
        $stmt->execute();
        $last_id = $this->dbal->lastInsertId();
        /*
        $sql = "UPDATE nodes n SET n.meta_id = (SELECT n2.meta_id FROM nodes n2 WHERE n2.id = :node_id ) WHERE n.id = :new_node";
        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(':node_id', $node_id);
        $stmt->bindParam(':new_node', $last_id);
        */
        $stmt->execute();

        return $last_id;
    }

    /**
     * @param $node_id
     * @param $new_parent
     */
    public function moveSubTree($node_id, $new_parent){
        // unlink from tree
        $sql = '
            DELETE a FROM navigation AS a
            JOIN navigation AS d ON a.descendant = d.descendant
            LEFT JOIN navigation AS x
            ON x.ancestor = d.ancestor AND x.descendant = a.ancestor
            WHERE d.ancestor = :node_id AND x.ancestor IS NULL;
        ';
        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(':node_id',$node_id);
        $stmt->execute();
        // insert with new parent as ancestor
        $sql = '
            INSERT INTO navigation (ancestor, descendant, depth)
            SELECT supertree.ancestor, subtree.descendant,
            supertree.depth+subtree.depth+1
            FROM navigation AS supertree JOIN navigation AS subtree
            WHERE subtree.ancestor = :node_id
            AND supertree.descendant = :new_parent;
        ';
        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(':node_id', $node_id);
        $stmt->bindParam('new_parent', $new_parent);
        $stmt->execute();
    }

    /**
     * adds meta data for a node (should be called after insert node if data is available)
     * @param $node_id
     * @param $data
     *
     */
    public function insertMetaData($node_id, $data){
        $meta = array('type','name','shortname','text','path');
        for($i = 0; $i < count($meta); $i++){
            if(! array_key_exists($meta[$i], $data)){
                unset($meta[$i]);
            }
        }
        $sql = '
            INSERT INTO node_properties ('.implode(',',$meta).')
            VALUES (:'.implode(',:',$meta).')
        ';
        $stmt = $this->dbal->prepare($sql);
        foreach($meta as $meta_name){
            $stmt->bindParam(':'.$meta_name, $data[$meta_name]);
        }
        $stmt->execute();
        $meta_id = $this->dbal->lastInsertId();
        $sql = '
            UPDATE nodes SET meta_id = :meta_id WHERE id = :node_id
        ';
        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(':meta_id',$meta_id);
        $stmt->bindParam(':node_id',$node_id);
        $stmt->execute();
    }

    public function duplicateHelp($help_id, $parent_id){
        // 1. fetches helps and its descendants
        $sql = "SELECT node.id, node.meta_id, node.quiz_id, node.position, meta.type, n.depth
                FROM nodes AS node
                JOIN navigation AS n ON node.id = n.descendant
                JOIN node_properties as meta ON node.meta_id = meta.id
                WHERE n.ancestor = :help_id AND (n.depth = 1 OR n.depth = 0)
                GROUP BY node.id ORDER BY FIELD(meta.type,'help','url','file')";
        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(':help_id', $help_id);
        $stmt->execute();
        $results = $stmt->fetchAll();

        // 2. create new nodes with help as direct ancestor
        foreach($results as $res){
            if($res['type'] == 'help'){
                // first we attach new help to the given parent
                $new_help_id = $this->addNode($parent_id,$res);
            }else{
                $this->addNode($new_help_id,$res);
            }
        }

        return $new_help_id;
    }

    public function duplicateMetaData($node_id, $meta_id){
        $sql = 'UPDATE nodes n1 SET meta_id = :meta WHERE n1.id = :node_id';
        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(':meta', $meta_id);
        $stmt->bindParam(':node_id',$node_id);
        $stmt->execute();
    }

    public function updateNodeMeta($node_id,$data){
        $meta = array('type','name','shortname','text','path');
        $fields = array();

        for($i = 0; $i < count($meta); $i++){
            if(array_key_exists($meta[$i], $data)){
               $fields[] = $meta[$i].' = :'.$meta[$i];
            }
        }
        $sql = 'UPDATE node_properties np JOIN nodes n ON n.meta_id=np.id SET '.implode(',',$fields).' WHERE n.id = :node_id';


        /** @var Statement $stmt */
        $stmt = $this->dbal->prepare($sql);
        foreach($fields as $field){
            $f = explode(' = :',$field);
            $stmt->bindParam(':'.$f[0], $data[$f[0]]);
        }
        $stmt->bindParam(':node_id',$node_id);
        $stmt->execute();
    }

    public function updateOrder($node_id, $position){
        $table = 'nodes';
        // if node_id is prefixed, it comes from help_contents table
        if(strpos($node_id,'c_')!==false){
            $node_id = intval(substr($node_id,2));
            $table = 'help_contents';
        }
        $sql = "UPDATE ".$table." SET position=:pos WHERE id=:node_id";
        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(':pos', $position);
        $stmt->bindParam(':node_id',$node_id);
        $stmt->execute();
    }

    public function insertHelpContents($data){
        $this->setupRevNumberAndUser();
        $sql = 'INSERT INTO help_contents (name,text,type,position, meta_id,shortname,path)
                VALUES (:name,:text,:type,:position,
                        (SELECT np.id FROM node_properties np JOIN nodes n ON n.meta_id = np.id WHERE n.id = :parent_id),
                        :shortname,:path
                        )';

        $this->dbal->executeQuery($sql,
            array(':name'=>$data['name'],
                  ':text'=>isset($data['text'])?$data['text']:'',
                  ':type'=>$data['type'],
                  ':position'=>$data['position'],
                  ':parent_id'=>$data['parent_id'],
                  ':shortname'=>isset($data['shortname'])?$data['shortname']:null,
                  ':path'=>isset($data['path'])?$data['path']:null
            )
        );

        $id = $this->dbal->lastInsertId();

        return $id;
    }

    public function retrieveContentAsNode($id){
        if(strpos($id,'c_') !== false){
            $id = intval(substr($id,2));
        }
        $sql = "SELECT id, type, position, name, shortname, path, text FROM help_contents WHERE id = :content_id";
        $res = $this->dbal->fetchAll($sql,array(':content_id'=>$id));
        $n = new Node($res[0]);
        return $n;
    }

    public function deleteHelpContent($id){
        if(strpos($id,'c_') !== false){
            $id = intval(substr($id,2));
        }

        $this->setupRevNumberAndUser();

        $sql = 'DELETE FROM help_contents WHERE id = :id';
        $res = $this->dbal->executeQuery($sql, array(':id'=>$id));
        return $res;
    }


    public function updateHelpContent($data){
        if(! isset($data['node_id'])){
            throw new Exception("pas d'id !");
        }else{

            $id = $data['node_id'];
        }

        $user = $this->app['security']->getToken()->getUser();
        $this->dbal->executeQuery('SET @rev_nr=1');
        $this->dbal->executeQuery('SET @usr_id=:u', array('u'=>$user->getId()));
        $sql = 'UPDATE help_contents
                SET name=IFNULL(:name,name), shortname=IFNULL(:shortname,shortname), text=IFNULL(:text,text), path=IFNULL(:path,path)
                WHERE id = :id
                ';

        if(strpos($id,'c_') !== false){
            $id = intval(substr($id,2));
        }
        $this->dbal->executeQuery($sql,
            array(
                ':id'=>$id,
                ':name'=>isset($data['name'])?$data['name']:null,
                ':shortname'=>isset($data['shortname'])?$data['shortname']:null,
                ':text'=>isset($data['text'])?$data['text']:null,
                ':path'=>isset($data['path'])?$data['path']:null
            )
        );
    }

    /**
     * retrieves a parent node from help_content id
     *
     * @param $id
     *
     * @return Node : the first node in the data set
     */
    public function retrieveHelpNodeFromContent($id){
        $sql = 'SELECT n.*, np.* FROM nodes n JOIN node_properties np ON n.meta_id = np.id JOIN help_contents c ON np.id = c.meta_id WHERE c.id = :id';
        $res = $this->dbal->fetchAll($sql, array(':id'=>$id));
        return new Node($res[0]);
    }

    public function getChoiceWordings($quizId, $json=true){
        $sql = "SELECT id as meta_id, name FROM node_properties WHERE id IN (SELECT meta_id FROM nodes WHERE quiz_id = :quiz_id)";
        $res = $this->dbal->fetchAll($sql, array(':quiz_id'=>$quizId));
        if($json){
            return json_encode($res);
        }else{
            return $res;
        }
    }

    public function deleteQuiz($quizId) {
           //  $sql = "DELETE np FROM node_properties AS np JOIN nodes AS  n ON n.meta_id = np.id WHERE n.quiz_id = :quiz_id";
            $sql = "DELETE FROM nodes WHERE quiz_id = :quiz_id";
            $this->dbal->executeQuery($sql,array(':quiz_id'=>$quizId));
            $sql = "DELETE FROM quizzes WHERE id = :quiz_id";
            $this->dbal->executeQuery($sql, array(':quiz_id'=>$quizId));
    }

    public function getQuizBySlug($slug){
        $sql = "
            SELECT
            q.id,q.name,q.slug,'quiz' AS type,q.published,q.locked,q.description
            FROM quizzes q
            WHERE q.slug = :slug
        ";
        $res = $this->dbal->fetchAll($sql, array(':slug'=>$slug));
        if(empty($res)){
            return false;
        }
        return $res[0];
    }

    public function createQuiz($name, $slug, $description){

        $sql = "INSERT INTO quizzes (name,slug) VALUES(:name,:slug)";
        $res = $this->dbal->insert('quizzes', array('slug'=>$slug, 'name'=>$name, 'description'=>$description));
        //       error_log('********************* res: '.$res."\n\n");
        if($res > 0){
            $quiz = $this->getQuizById($this->dbal->lastInsertId());
            $node_id  = $this->addNode(null, array('position'=>'0','status'=>'visible','quiz_id'=>$quiz['id']));
            $this->insertMetaData($node_id,array('type'=>'quiz', 'name'=>$quiz['name']));
            return $quiz;
        }else{
            return false;
        }
    }
    public function updateQuiz($data){
        $sql = "UPDATE quizzes SET name = :name, description=:desc WHERE id = :id";
        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(':name',$data['name']);
        $stmt->bindParam(':desc', $data['description']);
        $stmt->bindParam(':id',$data['id']);
        $res = $stmt->execute();

        $sql ="UPDATE node_properties AS np JOIN nodes n ON np.id = n.meta_id SET np.name = :name
         WHERE np.type='quiz' AND n.quiz_id=:quizId";
        $stmt = $this->dbal->prepare($sql);
        $stmt->bindparam(':name',$data['name']);
        $stmt->bindParam(':quizId',$data['id']);
        $stmt->execute();
        return $this->getQuizById($data['id']);
    }



    public function getQuizById($id){
        $sql = "SELECT q.* FROM quizzes q WHERE id = :id";
        $quiz = $this->dbal->fetchAssoc($sql, array(':id'=>$id));
        return $quiz;
    }

    public function lockQuiz(){

    }

    public function publishQuiz($id){
        $this->dbal->update('quizzes', array('published'=>1),array('id'=>$id));
        return true;
    }

    public function unpublishQuiz($id){

        $this->dbal->update('quizzes', array('published'=>0), array('id'=>$id));
        return true;

    }

    public function duplicateQuiz($quizId){
        $sql = "CALL duplicateQuiz(:quizId,@ret)";
        $stmt = $this->dbal->prepare($sql);
        $stmt->bindParam(':quizId', $quizId);
        $stmt->execute();
        $new_id = $this->dbal->fetchColumn("SELECT @ret");

        return $new_id;
    }

    public function clearTablesForRestore($quiz_id){
        $sql = 'DELETE FROM node_properties WHERE id IN (SELECT meta_id FROM nodes WHERE quiz_id = ?)';
        $this->dbal->executeQuery($sql,array($quiz_id));
        $sql = 'DELETE FROM navigation WHERE ancestor IN (SELECT id FROM nodes WHERE quiz_id = ?)';
        $this->dbal->executeQuery($sql,array($quiz_id));
        $sql = 'DELETE FROM nodes WHERE quiz_id = ?';
        $this->dbal->executeQuery($sql,array($quiz_id));
    }

    public function editQuiz($slug){
        // todo
        //  retrouver quizzes avec ce slug.
        //  si un seul => dupliquer le quiz et mettre unpublished
        //  si plusieurs, renvoyer la version unpublished
        //  vérifier si le quiz est locked renvoyer une erreur (403) avec nom/email de l'utilisateur
        // quid si user essaye d'éditer plusieurs fois le quiz
        // délocker le quiz en  fin de session

        // TODO locker quiz = mettre last access et user id + verrou
        // fixme requête de test
        $sql = "SELECT id FROM quizzes WHERE slug = :slug ORDER BY id";
        $ids = $this->dbal->fetchAll($sql,array(':slug'=>$slug));
        if(empty($ids) || $ids == ''){
            throw new Exception('Impossible de trouver le quiz');
        }

        return $this->retrieveTree($ids[0]['id']);
    }

    public function getHelpList($quizId){
        $sql = "SELECT n.id, np.name FROM node_properties np JOIN nodes n ON np.id = n.meta_id WHERE np.type='help' AND n.quiz_id = :quizId GROUP BY np.id ORDER BY np.name";
        $res = $this->dbal->fetchAll($sql, array('quizId'=>$quizId));
        if(empty($res)){
            throw new Exception('Pas d\'aides associées à ce quiz');
        }
        return $res;
    }

    /**
     * retrieves all quizzes
     *
     * @param bool $published_only [only published quizzes]
     * @return array
     */
    public function getQuizzes($published_only=false){
        $published = '';
        if($published_only){
            $published = 'WHERE q.published = 1 ';
        }
        $sql = 'SELECT DISTINCT
                    q.id, q.name, q.slug, q.locked, q.description, q.published, q.current_editor, q.created, q.updated
                FROM
                    quizzes q '.$published.'
                ORDER BY q.name;
                ';
        return $this->dbal->fetchAll($sql);

    }

    public function getHelpById($helpId) {
        $sql = 'SELECT n.id as node_id, np.type, np.name, np.path, np.text FROM nodes n
                JOIN navigation nav ON n.id = nav.ancestor
                JOIN node_properties np ON n.meta_id = np.id
                WHERE nav.ancestor IN (SELECT descendant FROM navigation WHERE ancestor = :helpId)
                GROUP BY n.id
                UNION SELECT hc.id as node_id, hc.type, hc.name,hc.path, hc.text
                FROM help_contents hc
                WHERE hc.meta_id = (SELECT n.meta_id FROM nodes n WHERE n.id = :helpId)
                ';
        $set =  $this->dbal->fetchAll($sql,array('helpId'=>$helpId));
        $res = array();
        foreach($set as $row){
            if($row['type'] != 'help'){

                $res[$row['type'].'s'][] = array('name'=>$row['name'],'text'=>$row['text'], 'path' => $row['path']);

            }else{
                $res['name'] = $row['name'];
                $res['text'] = $row['text'];
            }
        }
        if(empty($res)){
            throw new Exception('Pas d\'aide trouvée avec cet id: '.$helpId);
        }
        return $res;
    }


    public function getQuizRevisions($slug){
        $sql = 'SELECT rq.id, rq.revision, rq.timestamp, q.id as quiz_id, rq.prod as prod,
                    u.name as user_name, u.firstname as user_firstname, u.email as user_email
                    FROM _revision_quizzes rq JOIN users u ON rq.user_id = u.id
                    JOIN quizzes q ON rq.quiz_id = q.id
                    WHERE q.slug = :slug
                    ORDER BY rq.timestamp DESC
                ';

        $ret = $this->dbal->fetchAll($sql, array(':slug'=>$slug));
        return $ret;
    }

    public function addQuizRevision($quiz_id, $filename, $comment, $prod=0){
        $this->dbal->beginTransaction();
        if($prod == 1){
            // all revisions are set to non production
            $this->dbal->update('_revision_quizzes', array('prod'=>0),array('quiz_id'=>$quiz_id));
        }
        $sql = 'INSERT INTO _revision_quizzes (revision, json_filename,comment,prod,user_id,quiz_id)
                VALUES(IFNULL((SELECT max(rq1.revision)+1 FROM _revision_quizzes rq1 WHERE rq1.quiz_id = :id2 ),1),:file,:comment,:prod,:user,:id)';
        $stmt = $this->dbal->prepare($sql);

        $stmt->bindParam(':file',$filename);
        $stmt->bindParam(':comment',$comment);
        $stmt->bindParam(':prod',$prod);
        $user_id = $this->app['current.user_id']->getId();
        $stmt->bindValue(':user',$user_id);
        $stmt->bindParam(':id',$quiz_id);
        $stmt->bindParam(':id2',$quiz_id);

        try{

            $stmt->execute();
            $id = $this->dbal->lastInsertId();
            $this->dbal->commit();
        }catch(Exception $e){
            $this->dbal->rollback();
            return false;
        }

        return $id;
    }

    public function retrieveRevision($qSlug="", $rev_id="",$timestamp=""){
        if($rev_id == 'prod'){
            $sql = 'SELECT * FROM _revision_quizzes rq JOIN quizzes q ON rq.quiz_id = q.id WHERE rq.prod = 1 AND q.slug = :slug';
            /** @var Statement $stmt */
            $stmt = $this->dbal->prepare($sql);
            $stmt->bindParam(':slug',$qSlug);

        }else if($timestamp != ''){
            $sql = 'SELECT * FROM _revision_quizzes rq JOIN quizzes q ON rq.quiz_id = q.id WHERE rq.timestamp = :ts OR rq.id = :rev_id AND q.slug = :slug';
            $stmt = $this->dbal->prepare($sql);
            $stmt->bindParam(':ts',$timestamp);
            $stmt->bindParam(':rev_id',$timestamp);
            $stmt->bindParam(':slug',$qSlug);

        }else if($rev_id>0){
            $sql = 'SELECT * FROM _revision_quizzes rq WHERE id = :id';
            $stmt = $this->dbal->prepare($sql);
            $stmt->bindParam(':id', $rev_id);

        }

        $stmt->execute();
        $res = $stmt->fetchAll();

        return $res[0];

    }

    public function updateProdRevision($rev_id, $quiz_id){

        try{
            $this->dbal->beginTransaction();
            $this->dbal->update('_revision_quizzes',array('prod'=>0),array('quiz_id'=>$quiz_id));
            $this->dbal->update('_revision_quizzes',array('prod'=>1),array('id'=>$rev_id));
        }catch(\Exception $e){

            $this->dbal->rollback();
        }
        $this->dbal->commit();
    }

    public function deleteRevision($rev_id){
        try{
            $this->dbal->delete('_revision_quizzes', array('id'=>$rev_id));
        }catch(Exception $e){
            error_log($e->getMessage()." ".$e->getTraceAsString());
            return false;
        }

        return true;
    }

    private function setupRevNumberAndUser(){
        $token = $this->app['security']->getToken();
        if(null !== $token){
            $user = $token->getUser();
            if($user instanceof User){
                $this->dbal->executeQuery('SET @usr_id=:u', array('u'=>$user->getId()));
            }
        }
        // todo: find the right rev_nr
        $this->dbal->executeQuery('SET @rev_nr=1');
    }
}