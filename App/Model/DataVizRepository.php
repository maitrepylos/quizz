<?php
/**
 * Greg Berger
 *
 * Date: 27/01/14
 * Time: 22:56
 */

namespace App\Model;


class DataVizRepository {

    private $dbal;
    public function __construct($dbal){
        $this->dbal = $dbal;
    }


    /**
     * @param $quizId
     * @param bool $json
     * @return array|string
     */
    public function retrieveTree($quizId, $json=true){
        $sql = "
        SELECT n.id, n.quiz_id as quizId, nav.ancestor as root_node, n.position, n.meta_id, p.type,p.name,p.shortname,p.description,p.path,p.text,
            (SELECT COUNT(ancestor)-1 FROM navigation WHERE descendant = n.id GROUP BY descendant) as depth,
            GROUP_CONCAT(crumbs.`ancestor` ORDER BY crumbs.depth DESC) AS breadcrumbs,
            (SELECT GROUP_CONCAT(n2.id)
              FROM nodes n2 JOIN node_properties np
              ON n2.meta_id = np.id
              WHERE (np.type = 'url' OR np.type = 'help' OR np.type = 'file') AND n2.meta_id = n.meta_id AND n2.id <> n.id
            ) as related
            FROM nodes n
              JOIN navigation nav ON n.id = nav.descendant
              JOIN navigation AS crumbs ON crumbs.`descendant` = nav.`descendant`
              JOIN node_properties p ON n.meta_id = p.id
            WHERE nav.ancestor IN (
                  SELECT n.ancestor
                  FROM navigation n
                  LEFT OUTER JOIN navigation n1 ON n1.descendant = n.descendant AND n1.ancestor <> n.ancestor
                  WHERE n1.ancestor IS NULL
            )
            AND n.quiz_id = ?
            GROUP BY n.id
              ORDER BY depth,breadcrumbs;
        ";

        $res = $this->dbal->fetchAll($sql, array((int)$quizId));


        if($json){
            $nestedNodes = $this->mktree_array($res);

            if(defined('JSON_PRETTY_PRINT'))
                return json_encode($nestedNodes[0], JSON_PRETTY_PRINT); // pas dispo avant PHP 5.4
            else
                return json_encode($nestedNodes[0]);
        }
        return ($res);

    }

    /*
     * Formats the result set recursively
     */
    public function mktree_array(&$arr, $id = 0)
    {
        $result = array();

        foreach ($arr as $a) {
            $breadcr = explode(',',$a['breadcrumbs']);

            $related = explode(',',$a['related']);

            if(isset($breadcr[$a['depth']-1])){
                $parentId = $breadcr[$a['depth']-1];
            }else{
                $parentId = null;
            }
            $del = array_pop($breadcr);
            $a['breadcrumbs'] = array_map('intval',$breadcr);

            // if sql field was null
            if(empty($related[0])){
                $a['related'] = array();
            }else{
                $a['related'] = array_map('intval',$related);
            }
            $a['order'] = $a['position']+1;
            $a['id'] = intval($a['id']);
            unset($a['position']);
            if ($id == $parentId) {
                $children = $this->mktree_array($arr, $a['id']);
                uasort($children, function($a,$b){
                    if(! isset($a['order']) || !isset($b['order']) || ($a['order'] == $b['order'])){
                        return 0;
                    }
                    return ($a['order'] < $b['order']) ? -1:1;
                });

                foreach($children as $child){

                    $a['children'][] = $child;
                }

                if(! isset($a['choices'])) $a['choices'] = array();
                $result[] = $a;
            }
        }

        return $result;
    }

} 