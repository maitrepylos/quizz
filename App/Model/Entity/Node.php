<?php
/**
 * Greg Berger
 *
 * Date: 20/11/13
 * Time: 09:34
 */

namespace App\Model\Entity;


class Node {
    private $breadcrumbs;
    private $children;
    private $siblings;
    private $id;
    private $name;
    private $shortname;
    private $type;
    private $description;
    private $text;
    private $path;
    private $status;
    private $position;
    private $depth;
    private $quiz_id;
    private $meta_id;

    private $fields = array('id','position','type','name','shortname','description','path','text','depth','breadcrumbs','status', 'quiz_id','meta_id');

    public function __construct($data_set){
        foreach($this->fields as $field){
            $this->$field = isset($data_set[$field]) ? $data_set[$field]: "";
        }

    }

    public function serializeJson(){
        $data = array();
        foreach($this->fields as $field){
            if($field=='position'){
                $data['order'] = $this->$field;
            }else{
                $data[$field] = $this->$field;
            }

        }
        return (json_encode($data));
    }

    public function getParentId(){
        $bread = explode(',', $this->breadcrumbs);
        return $this->depth > 0 ? ltrim($bread[count($bread)-2],"0"):0;
    }

    public function __toString(){
        return "Node id : ".$this->id." name: ".$this->name." type: ".$this->type;
    }

    /**
     * @param mixed $breadcrumbs
     */
    public function setBreadcrumbs($breadcrumbs) {
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * @return mixed
     */
    public function getBreadcrumbs() {
        return $this->breadcrumbs;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children) {
        $this->children = $children;
    }

    /**
     * @return mixed
     */
    public function getChildren() {
        return $this->children;
    }

    /**
     * @param mixed $depth
     */
    public function setDepth($depth) {
        $this->depth = $depth;
    }

    /**
     * @return mixed
     */
    public function getDepth() {
        return $this->depth;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param mixed $field
     */
    public function setField($field) {
        $this->field = $field;
    }

    /**
     * @return mixed
     */
    public function getField() {
        return $this->field;
    }

    /**
     * @param array $fields
     */
    public function setFields($fields) {
        $this->fields = $fields;
    }

    /**
     * @return array
     */
    public function getFields() {
        return $this->fields;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path) {
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getPath() {
        return $this->path;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position) {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getPosition() {
        return $this->position;
    }

    /**
     * @param mixed $shortname
     */
    public function setShortname($shortname) {
        $this->shortname = $shortname;
    }

    /**
     * @return mixed
     */
    public function getShortname() {
        return $this->shortname;
    }

    /**
     * @param mixed $siblings
     */
    public function setSiblings($siblings) {
        $this->siblings = $siblings;
    }

    /**
     * @return mixed
     */
    public function getSiblings() {
        return $this->siblings;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status) {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param mixed $text
     */
    public function setText($text) {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getText() {
        return $this->text;
    }

    /**
     * @param mixed $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param mixed $quiz_id
     */
    public function setQuizId($quiz_id) {
        $this->quiz_id = $quiz_id;
    }

    /**
     * @return mixed
     */
    public function getQuizId() {
        return $this->quiz_id;
    }

    public function getMetaId(){
        return $this->meta_id;
    }

    public function setMetaId($id){
        $this->meta_id = $id;
    }
}
