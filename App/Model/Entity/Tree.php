<?php
/**
 * Greg Berger
 *
 * Date: 22/11/13
 * Time: 10:44
 */

namespace App\Model\Entity;


class Tree {

    /**
     * @var Node[]
     */
    private $nodes;
    /**
     * @param $nodes Node[]
     */
    public function __construct($nodes){
      $this->nodes = $nodes;
    }

    public function buildTree($nodes=array()){
        if(! empty($nodes)){
            $this->nodes = $nodes;
        }

        $fields = array();
        /** @var $node Node */
        foreach($this->nodes as $node){
            if($node->getDepth() != 0){

                $fields[$node->getType()][$node->getDepth()][] = $node->getId();

            }else{

            }
        }

        var_dump($fields);
        die();
        return $fields;

    }

    public function serializeTree($type="json"){
        switch($type){
            case "json":{
                return $this->serializeJson();
            }

        }
    }


    private function serializeJson(){
        return json_encode($this->nodes);
    }



} 