SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Table `node_properties`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `node_properties` ;
DROP TABLE IF EXISTS `help_contents`;
DROP TABLE IF EXISTS `quizzes` ;
DROP TABLE IF EXISTS `nodes` ;
DROP TABLE IF EXISTS `navigation` ;
-- DROP TABLE IF EXISTS `users` ;


CREATE  TABLE IF NOT EXISTS `node_properties` (
`id` BIGINT NOT NULL AUTO_INCREMENT ,
`type` VARCHAR(45) NULL ,
`name` VARCHAR(400) NULL ,
`shortname` VARCHAR(45) NULL ,
`description` VARCHAR(400) NULL ,
`text` TEXT NULL ,
`path` VARCHAR(256) NULL ,
PRIMARY KEY (`id`) )
ENGINE = InnoDB;


CREATE TABLE IF NOT EXISTS `help_contents`(
`id` INT NOT NULL AUTO_INCREMENT ,
`type` VARCHAR (45) NOT NULL ,
`name` VARCHAR (100) NULL ,
`text` VARCHAR (400) NULL ,
`path` VARCHAR (256) NULL,
`shortname` VARCHAR (45) NULL,
`position` INT NULL,
`meta_id` BIGINT NULL,
PRIMARY KEY (`id`),
CONSTRAINT `meta`
FOREIGN KEY (`meta_id`)
REFERENCES `node_properties` (`id`)
ON DELETE CASCADE
ON UPDATE NO ACTION
)ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `quizzes`
-- -----------------------------------------------------

CREATE  TABLE IF NOT EXISTS `quizzes` (
`id` INT NOT NULL AUTO_INCREMENT,
`name` VARCHAR(256) NULL,
`slug` VARCHAR (64) NOT NULL UNIQUE ,
`description` VARCHAR (128) NULL,
`created` TIMESTAMP DEFAULT NOW(),
`updated` TIMESTAMP NULL,
`accessed` TIMESTAMP NULL,
`current_editor` INT NULL,
`locked` BOOLEAN DEFAULT FALSE ,
`published` BOOLEAN DEFAULT FALSE,
PRIMARY KEY (`id`),
CONSTRAINT `user`
FOREIGN KEY (`current_editor`)
REFERENCES `users` (`id`)
ON DELETE NO ACTION
ON UPDATE NO ACTION
)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `nodes`
-- -----------------------------------------------------

CREATE  TABLE IF NOT EXISTS `nodes` (
`id` BIGINT (14) ZEROFILL NOT NULL AUTO_INCREMENT ,
`meta_id` BIGINT NULL ,
`position` INT NULL DEFAULT 0,
`status` VARCHAR(45) NULL ,
`quiz_id` INT NULL ,
PRIMARY KEY (`id`) ,
INDEX `props_idx` (`meta_id` ASC) ,
INDEX `quiz_idx` (`quiz_id` ASC) ,
CONSTRAINT `props`
FOREIGN KEY (`meta_id` )
REFERENCES `node_properties` (`id` )
ON DELETE CASCADE
ON UPDATE NO ACTION,
CONSTRAINT `quiz`
FOREIGN KEY (`quiz_id` )
REFERENCES `quizzes` (`id` )
ON DELETE CASCADE
ON UPDATE NO ACTION)
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `navigation`
-- -----------------------------------------------------


CREATE  TABLE IF NOT EXISTS `navigation` (
`ancestor` BIGINT(14) ZEROFILL NOT NULL ,
`descendant` BIGINT(14) ZEROFILL NOT NULL ,
`depth` INT NULL DEFAULT 0 ,
PRIMARY KEY (`ancestor`, `descendant`) ,
INDEX `ancestor_idx` (`ancestor` ASC) ,
INDEX `descendant_idx` (`descendant` ASC) ,
CONSTRAINT `ancestor`
FOREIGN KEY (`ancestor` )
REFERENCES `nodes` (`id` )
ON DELETE CASCADE
ON UPDATE NO ACTION,
CONSTRAINT `descendant`
FOREIGN KEY (`descendant` )
REFERENCES `nodes` (`id` )
ON DELETE CASCADE
ON UPDATE NO ACTION
)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `users`
-- -----------------------------------------------------

CREATE  TABLE IF NOT EXISTS `users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(64) NULL ,
  `name` VARCHAR(64) NULL ,
  `firstname` VARCHAR(64) NULL ,
  `telephone` VARCHAR(12) NULL ,
  `email` VARCHAR(128) NULL ,
  `password` VARCHAR(256) NULL ,
  `roles` VARCHAR(256) NULL ,
  `register_key` VARCHAR(256) NULL ,
  `activated` TINYINT NULL ,
  `last_login` DATETIME NULL ,
  PRIMARY KEY (`id`),
  CONSTRAINT `unique_mail`
    UNIQUE (email)

)
  ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
