/*
CREATE TRIGGER keep_quizzes_updated AFTER UPDATE ON quizzes FOR EACH ROW
  BEGIN
    UPDATE quizzes set updated = NOW();
  END;
*/
START TRANSACTION ;
DROP TRIGGER IF EXISTS navigation_insert;
DROP TRIGGER IF EXISTS navigation_update;
DROP TRIGGER IF EXISTS navigation_delete;
DROP TRIGGER IF EXISTS create_node;
DROP TRIGGER IF EXISTS delete_node;
DROP TRIGGER IF EXISTS update_node;
DROP TRIGGER IF EXISTS insert_meta;
DROP TRIGGER IF EXISTS delete_meta;
DROP TRIGGER IF EXISTS update_meta;
DROP TRIGGER IF EXISTS update_slug;
DROP TRIGGER IF EXISTS delete_help_content;
DROP TRIGGER IF EXISTS insert_help_content;
DROP TRIGGER IF EXISTS update_help_content;

-- revisions

DROP TRIGGER IF EXISTS quiz_after_update;

DROP PROCEDURE IF EXISTS duplicateQuiz;

-- TODO: revision triggers in quizzes tables

DELIMITER ||

-- Navigation
CREATE TRIGGER navigation_insert AFTER INSERT ON navigation FOR EACH ROW
  BEGIN
    -- DECLARE CONTINUE HANDLER FOR 1442 BEGIN END;
    UPDATE quizzes SET updated = NOW() WHERE id = (SELECT n.quiz_id FROM nodes n WHERE n.id = new.ancestor);
    /*
    INSERT IGNORE INTO _revision_navigation(
      ancestor, descendant, depth, _revision, _revision_previous, _revision_action)
      VALUES (new.ancestor,new.descendant,new.depth, @rev_nr, new._revision, 'INSERT');
    */
  END
||

CREATE TRIGGER navigation_delete AFTER DELETE ON navigation FOR EACH ROW
  BEGIN
    UPDATE quizzes SET updated = NOW() WHERE id = (SELECT n.quiz_id FROM nodes n WHERE n.id = old.ancestor);
    /*
    REPLACE INTO _revision_navigation
    (ancestor, descendant, depth, _revision, _revision_previous, _revision_action)
      VALUES(old.ancestor,old.descendant,old.depth,@rev_nr,old._revision,'DELETE');
    */
  END
||

CREATE TRIGGER navigation_update AFTER UPDATE ON navigation FOR EACH ROW
  BEGIN

    UPDATE quizzes SET updated = NOW() WHERE id = (SELECT n.quiz_id FROM nodes n WHERE n.id = new.ancestor);
    /*
    REPLACE INTO _revision_navigation
      (ancestor, descendant, depth, _revision, _revision_previous, _revision_action)
      VALUES(new.ancestor,new.descendant,new.depth,@rev_nr,new._revision,'UPDATE');
    */

  END
||

-- Nodes
CREATE TRIGGER create_node AFTER INSERT ON nodes FOR EACH ROW
  BEGIN
    -- DECLARE CONTINUE HANDLER FOR 1442 BEGIN END;
    UPDATE quizzes SET updated = NOW() WHERE id = new.quiz_id;
/*
    INSERT IGNORE INTO _revision_nodes(
      id, meta_id, position, status, quiz_id, _revision, _revision_previous, _revision_action
    )VALUES (new.id, new.meta_id,new.position,new.status,new.quiz_id,@rev_nr,new._revision, 'INSERT');
*/
  END
||

CREATE TRIGGER delete_node AFTER DELETE ON nodes FOR EACH ROW
  BEGIN
  --  DECLARE CONTINUE HANDLER FOR 1442 BEGIN END;
    UPDATE quizzes SET updated = NOW() WHERE id = old.quiz_id;
    DELETE FROM node_properties
    WHERE (
            (SELECT count(meta_id) FROM nodes GROUP BY meta_id HAVING meta_id = old.meta_id )  < 1
            OR (SELECT count(meta_id) FROM nodes GROUP BY meta_id HAVING meta_id = old.meta_id ) IS NULL
          )
          AND id = old.meta_id ;
/*
    REPLACE INTO _revision_nodes SET id=old.id,meta_id=old.meta_id,position=old.position,
        status=old.status,quiz_id=old.quiz_id,_revision=@rev_nr,_revision_previous=old._revision, _revision_action='DELETE';
*/
  END

||

CREATE TRIGGER update_node AFTER UPDATE ON nodes FOR EACH ROW
  BEGIN
    UPDATE quizzes SET updated = NOW() WHERE id = new.quiz_id;
  /*
    REPLACE INTO _revision_nodes(
      id, meta_id, position, status, quiz_id, _revision, _revision_previous, _revision_action
    )VALUES( new.id, new.meta_id,new.position, new.status, new.quiz_id,@rev_nr,old._revision,'UPDATE');
  */
  END
||


-- node_properties
CREATE TRIGGER update_meta AFTER UPDATE ON node_properties FOR EACH ROW
  BEGIN
    DECLARE qid INTEGER ;
    SET qid = (SELECT DISTINCT n.quiz_id FROM nodes n WHERE n.meta_id = new.id);
    UPDATE quizzes SET updated = NOW() WHERE id = qid ; -- (SELECT DISTINCT n.quiz_id FROM nodes n WHERE n.meta_id = new.id);
    /*
    REPLACE INTO _revision_node_properties(
      id, type, name, shortname, text, path, _revision, _revision_previous, _revision_action
    )SELECT id, type,name,shortname,text,path, @rev_nr, old._revision,'UPDATE'
      FROM node_properties WHERE id=new.id;
    */
  END
||

CREATE TRIGGER delete_meta AFTER DELETE ON node_properties FOR EACH ROW
  BEGIN
    DECLARE qid INTEGER ;
  --  DECLARE CONTINUE HANDLER FOR 1442 BEGIN END;

    SET qid = (SELECT DISTINCT n.quiz_id FROM nodes n WHERE n.meta_id = old.id);
    UPDATE quizzes SET updated = NOW() WHERE id = qid;
/*
    REPLACE INTO _revision_node_properties SET id=old.id,name=old.name,path=old.path,shortname=old.shortname,text=old.text,
                _revision=@rev_nr,_revision_previous = old._revision, _revision_action = 'DELETE';
*/
  END
||

CREATE TRIGGER insert_meta AFTER INSERT ON node_properties FOR EACH ROW
  BEGIN
    DECLARE qid INTEGER;
  --  DECLARE CONTINUE HANDLER FOR 1442 BEGIN END;

    SET qid = (SELECT DISTINCT n.quiz_id FROM nodes n WHERE n.meta_id = new.id);
    UPDATE quizzes SET updated = NOW() WHERE id = (SELECT DISTINCT n.quiz_id FROM nodes n WHERE n.meta_id = new.id);
/*
    INSERT IGNORE INTO _revision_node_properties(
      id, type, name, shortname, text, path,
      _revision, _revision_previous, _revision_action
    )VALUES(new.id,new.type,new.name,new.shortname,new.text,new.path,@rev_nr,new._revision,'INSERT');
*/
  END
||

-- help contents

CREATE TRIGGER update_help_content AFTER UPDATE ON help_contents FOR EACH ROW
  BEGIN
    DECLARE qid INTEGER;
    SET qid = (SELECT DISTINCT n.quiz_id FROM nodes n WHERE n.meta_id = new.meta_id);
    UPDATE quizzes SET updated = NOW() WHERE id = qid;
/*
    REPLACE INTO _revision_help_contents (
      id, type, name, text, path, shortname, position, meta_id,
      _revision, _revision_previous, _revision_action
    )
      VALUES (new.id,new.type,new.name,new.text,new.path,new.shortname,new.position,new.meta_id,
      @rev_nr,old._revision, 'UPDATE');
*/
  END;

||

CREATE TRIGGER insert_help_content AFTER INSERT ON help_contents FOR EACH ROW
  BEGIN
    DECLARE qid INTEGER;
--    DECLARE CONTINUE HANDLER FOR 1442 BEGIN END;

    SET qid = (SELECT DISTINCT n.quiz_id FROM nodes n WHERE n.meta_id = new.meta_id);
    UPDATE quizzes SET updated = NOW() WHERE id = qid;
/*
    INSERT IGNORE INTO _revision_help_contents (
      id, type, name, text, path, shortname, position, meta_id,
      _revision, _revision_previous, _revision_action
    ) VALUES( new.id,new.type,new.name,new.text,new.path,new.shortname,new.position,new.meta_id,
        @rev_nr, new._revision, 'INSERT');
*/
  END;
||

CREATE TRIGGER delete_help_content AFTER DELETE ON help_contents FOR EACH ROW
  BEGIN
    DECLARE qid INTEGER;
  --  DECLARE CONTINUE HANDLER FOR 1442 BEGIN END;

    SET qid = (SELECT DISTINCT n.quiz_id FROM nodes n WHERE n.meta_id = old.meta_id);
    UPDATE quizzes SET updated = NOW() WHERE id = qid;
/*
    REPLACE INTO _revision_help_contents SET id=old.id,type=old.type,name=old.name,text=old.text,path=old.path,
      shortname=old.shortname,position=old.position,meta_id=old.meta_id,
      _revision=@rev_nr,_revision_previous=old._revision, _revision_action='DELETE';
*/
  END;

||



-- quizzes
CREATE TRIGGER update_slug BEFORE INSERT ON quizzes FOR EACH ROW
  BEGIN
    declare original_slug varchar (255);
    declare new_name varchar (255);
    declare new_slug varchar (255);
    declare slug_counter int;
    set slug_counter = 1;
    set original_slug = new.slug;
    set new_name = '';
    SELECT SUBSTRING_INDEX(new.slug, '-',1) INTO new_slug;
    SELECT SUBSTRING_INDEX(new.name,' - Copie',1) INTO new_name;
    while exists (SELECT true FROM quizzes WHERE slug = new.slug ) do
      set new.slug = concat(new_slug,'-',slug_counter);
      set slug_counter = slug_counter + 1;
    end while;

    IF(slug_counter > 1) THEN
      set new.name = concat(new_name,' - Copie ',slug_counter -1);
    END IF ;

  END;
||

CREATE TRIGGER quiz_after_update AFTER UPDATE ON quizzes FOR EACH ROW
  BEGIN
    -- SET new._revision = (SELECT MAX(_revision)+1 FROM _revhistory_quizzes);
   /*
    UPDATE _revision_quizzes
      SET id = new.id, name = new.name, slug = new.slug, description = new.description,
          updated = new.updated, created = new.created,
          _revision = new._revision, _revision_previous = new._revision,
          _revision_action = 'UPDATE' = NOW() WHERE id = new.id;
    INSERT IGNORE INTO _revhistory_quizzes
      SET id = new.id, _revision = new._revision, _revhistory_user_id = @usr_id, _revhistory_timestamp=NOW();
      */
  END;

||


CREATE PROCEDURE `duplicateQuiz`(IN q_id INT, INOUT ret INT(255))
  BEGIN
    declare new_quiz_id BIGINT;
    declare first_quiz_node_id BIGINT;
    declare var_meta_id BIGINT;
    declare var_new_meta_id BIGINT;
    declare var_position INT;
    declare var_status VARCHAR(45);
    declare var_node_id INT;
    declare count_map INT;
    declare last_help_contents_id INT;

-- gestion de la boucle sur le curseur
    declare done INT DEFAULT 0;
    declare curs1 CURSOR FOR SELECT id,meta_id,position,status FROM nodes WHERE quiz_id=q_id;
    declare CONTINUE HANDLER FOR SQLSTATE '02000' SET done = 1;

    set ret = 0;

    DROP TABLE IF EXISTS nodes_mapping;
    CREATE TABLE IF NOT EXISTS nodes_mapping(
      original_id BIGINT,
      clone_id BIGINT NOT NULL
    );

    CREATE TEMPORARY TABLE IF NOT EXISTS meta_mapping(
      meta_id BIGINT,
      new_meta_id BIGINT
    );

    START TRANSACTION;
-- duplication du quiz
    INSERT INTO quizzes (name, slug, description,created,accessed)
      (SELECT name,slug,description,now(),now() FROM quizzes WHERE id = q_id);
    set new_quiz_id = (SELECT last_insert_id());
    SET first_quiz_node_id = (SELECT n.id FROM nodes n JOIN node_properties np ON n.meta_id = np.id WHERE np.type = 'quiz' AND n.quiz_id=q_id );
    OPEN curs1;
    REPEAT
      FETCH curs1 INTO var_node_id,var_meta_id, var_position,var_status;
      IF done=0 THEN
-- créer new meta si n'existe pas déja
        IF ((SELECT count(meta_id) FROM meta_mapping WHERE meta_id = var_meta_id) < 1) THEN
          INSERT INTO node_properties (type,name,shortname,path,text) SELECT type,name,shortname,path,text FROM node_properties WHERE id = var_meta_id;
          SET var_new_meta_id =  last_insert_id();
-- mapper old_meta new meta
          INSERT INTO meta_mapping (meta_id, new_meta_id) VALUES (var_meta_id, var_new_meta_id);
        ELSE
-- si meta est déjà dans meta_mapping
          IF (SELECT count(new_meta_id) FROM meta_mapping WHERE meta_id = var_meta_id > 0) THEN
-- récuperer new meta pour insertion
            SELECT new_meta_id FROM meta_mapping WHERE meta_id = var_meta_id INTO var_new_meta_id;
          END IF ;
        END IF;
        IF (var_new_meta_id IS NOT null) THEN
          INSERT INTO nodes (meta_id, position, status, quiz_id) VALUES(var_new_meta_id, var_position, var_status, new_quiz_id);
          INSERT INTO nodes_mapping (original_id, clone_id) (SELECT var_node_id, last_insert_id());
        END IF ;
      END IF ;
    UNTIL done
    END REPEAT;
    CLOSE curs1;
    SELECT count(*) FROM nodes_mapping INTO count_map;
-- si rien n'a été ajouté, on annule la transaction
    IF count_map = 0 THEN
      ROLLBACK ;

    ELSE
      COMMIT ;
-- récupère le dernier id de la table help_contents pour éviter de modifier les meta_id précédents
      SELECT max(id) FROM help_contents INTO last_help_contents_id;
-- copie des contenus pour ce quiz
      INSERT INTO help_contents (type,name,text,path,shortname,position,meta_id) SELECT type,name,text,path,shortname,position,meta_id FROM help_contents WHERE meta_id IN (SELECT meta_id FROM nodes WHERE quiz_id = q_id);
-- remplacement des anciens meta_id
      UPDATE help_contents hc SET meta_id = (SELECT mm.new_meta_id FROM meta_mapping mm WHERE mm.meta_id = hc.meta_id) WHERE id > last_help_contents_id;
-- recréation de la table navigation
      INSERT INTO navigation (ancestor, descendant, depth)
        SELECT nm1.clone_id, nm2.clone_id, n.depth
        FROM navigation n
          JOIN nodes_mapping nm1 ON (n.ancestor=nm1.original_id)
          JOIN nodes_mapping nm2 ON (n.descendant = nm2.original_id)
        WHERE n.ancestor IN (SELECT id FROM nodes WHERE quiz_id = q_id);

      set ret = new_quiz_id;
    END IF;
-- nettoyage des tables temporaires
    DROP TABLE nodes_mapping;
    DROP TEMPORARY TABLE meta_mapping;
  END;
||

DELIMITER ;

COMMIT ;

