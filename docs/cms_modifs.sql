CREATE TABLE IF NOT EXISTS `config`(
  `id` int not null primary key auto_increment ,
  `key` varchar(64) not null,
  `value` varchar(128) not null,
  `quiz_id` int null default null,
  UNIQUE (`key`, quiz_id)
)ENGINE=InnoDb;

CREATE TABLE IF NOT EXISTS `cms_content`(
  `id` int not null primary key auto_increment ,
  `type` VARCHAR(48) not null ,
  `content` TEXT not null ,
  `quiz_id` INT null DEFAULT null,
  `slug` VARCHAR(64) DEFAULT null,
  `parent` INT null
)ENGINE=InnoDb;

