SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `_revision_help_contents`;
DROP TABLE IF EXISTS `_revision_navigation`;
DROP TABLE IF EXISTS `_revision_node_properties`;
DROP TABLE IF EXISTS `_revision_quizzes`;
DROP TABLE IF EXISTS `_revision_nodes`;

DROP TABLE IF EXISTS `_revhistory_quizzes`;
DROP TABLE IF EXISTS `_revhistory_nodes`;
DROP TABLE IF EXISTS `_revhistory_navigation`;
DROP TABLE IF EXISTS `_revhistory_help_contents`;
DROP TABLE IF EXISTS `_revhistory_node_properties`;


CREATE TABLE IF NOT EXISTS `_revision_quizzes`(
  `id` int not null auto_increment,
  `revision` int null default null,
  `json_filename` VARCHAR(64) null default 0,
  `timestamp` timestamp not null default CURRENT_TIMESTAMP,
  `comment` varchar(300) null default null,
  `prod` tinyint default 0 ,
  `quiz_id` int not null ,
  `user_id` int not null ,
  PRIMARY KEY (`id`),
  INDEX (`revision`),
  CONSTRAINT qid_revs FOREIGN KEY (`quiz_id`) REFERENCES `quizzes` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT uid_revs FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  UNIQUE(revision,quiz_id)

)ENGINE=InnoDb;

SET FOREIGN_KEY_CHECKS=1;


/*
ALTER TABLE nodes DROP _revision;
ALTER TABLE quizzes DROP _revision;
ALTER TABLE node_properties DROP _revision;
ALTER TABLE help_contents DROP _revision;
ALTER TABLE navigation DROP _revision;
ALTER TABLE nodes
    ADD `_revision` bigint unsigned NULL;

ALTER TABLE quizzes
    ADD `_revision` BIGINT unsigned NULL,
    ADD UNIQUE INDEX (`_revision`,`id`);

ALTER TABLE node_properties
    ADD `_revision` BIGINT unsigned NULL;

ALTER TABLE navigation
    ADD `_revision` BIGINT unsigned NULL;

ALTER TABLE help_contents
    ADD `_revision` BIGINT unsigned NULL;


CREATE TABLE IF NOT EXISTS `_revision_quizzes` LIKE `quizzes`;
    ALTER TABLE _revision_quizzes
    CHANGE `id` `id` int(10) unsigned,
    DROP PRIMARY KEY,
    ADD `_revision_previous` bigint unsigned null,
    ADD `_revision_action` enum('INSERT','UPDATE') default null,
    ADD INDEX (`_revision`),
    ADD PRIMARY KEY (`_revision`,`id`),
    ADD INDEX (`_revision_previous`),
    ADD INDEX rev_quizzes_idx  (`id`);

CREATE TABLE IF NOT EXISTS `_revhistory_quizzes`(
   `id` int(10) unsigned,
  `_revision` BIGINT UNSIGNED NULL ,
  `_revhistory_user_id` int unsigned null,
  `_revhistory_timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `_revhistory_comment` VARCHAR(256),
  INDEX(id),
  INDEX(_revision),
  INDEX (_revhistory_user_id),
  INDEX (_revhistory_timestamp)

)ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS `_revision_nodes` LIKE `nodes`;
ALTER TABLE _revision_nodes
    CHANGE `id` `id` int(10) unsigned,
    DROP PRIMARY KEY,
    ADD `_revision_previous` bigint unsigned null,
    ADD `_revision_action` enum('INSERT','UPDATE', 'DELETE') default null,
    ADD INDEX (`_revision`),
    ADD PRIMARY KEY (`_revision`,`id`),
    ADD INDEX (`_revision_previous`),
    ADD INDEX rev_nodes_idx  (`id`);


CREATE TABLE IF NOT EXISTS `_revision_navigation` LIKE `navigation`;
ALTER TABLE _revision_navigation
    DROP PRIMARY KEY,
    ADD `_revision_previous` bigint unsigned null,
    ADD `_revision_action` enum('INSERT','UPDATE', 'DELETE') default null,
    ADD INDEX (`_revision`),
    ADD INDEX (`_revision_previous`),
    ADD PRIMARY KEY (_revision,ancestor, descendant );


CREATE TABLE IF NOT EXISTS `_revision_node_properties` LIKE `node_properties`;
ALTER TABLE _revision_node_properties
    CHANGE `id` `id` int(10) unsigned,
    DROP PRIMARY KEY,
    ADD `_revision_previous` bigint unsigned null,
    ADD `_revision_action` enum('INSERT','UPDATE', 'DELETE') default null,
    ADD INDEX (`_revision`),
    ADD PRIMARY KEY (`_revision`,`id`),
    ADD INDEX (`_revision_previous`),
    ADD INDEX rev_properties_idx  (`id`);


CREATE TABLE IF NOT EXISTS `_revision_help_contents` LIKE `help_contents`;
ALTER TABLE _revision_help_contents
    CHANGE `id` `id` int(10) unsigned,
    DROP PRIMARY KEY,
    ADD `_revision_previous` bigint unsigned null,
    ADD `_revision_action` enum('INSERT','UPDATE','DELETE') default null,
    ADD INDEX (`_revision`),
    ADD INDEX (`_revision_previous`),
    ADD PRIMARY KEY (`id`,`_revision`),
    ADD INDEX rev_help_contents_idx  (`id`);


