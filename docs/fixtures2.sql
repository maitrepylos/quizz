
USE quiz;
  SET FOREIGN_KEY_CHECKS=0;
  TRUNCATE TABLE nodes;
  TRUNCATE TABLE navigation;
  TRUNCATE TABLE node_properties;
  TRUNCATE TABLE quizzes;
  TRUNCATE TABLE users;

  INSERT INTO quizzes (id,name,slug, published)
  VALUES (1,"Financez votre projet", "financier", TRUE),
         (2,"Accompagnement", "accompagnement", TRUE);


INSERT INTO users (username, email, password, roles, activated)
  VALUES ("admin","admin@paperpixel.net","5FZ2Z8QIkA7UTZ4BYkoC+GsReLf569mSKDsfods6LYQ8t+a8EW9oaircfMpmaLbPBh4FOBiiFyLfuZmTSUwzZg==" ,"ROLE_ADMIN",1);

  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
    VALUES (1, "quiz","1","","Pour trouver l'aide financière qui convient à votre projet","","");
  INSERT INTO nodes (id, meta_id, `position`, status,quiz_id)
    VALUES (1, 1, 0, "visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT
      n.ancestor,
      1,
      n.depth + 1
    FROM navigation AS n
    WHERE n.descendant = 1
    UNION ALL
    SELECT
      1,
      1,
      0;


  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
      VALUES (2, "question","Vous cherchez une aide financière pour innover et vous êtes...", "vous êtes..","", "", "");
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
      VALUES (2, 2, 0, "visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 2, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 1
    UNION ALL
    SELECT 2, 2, 0;

  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
      VALUES (3, "popup", "Quelle est la différence entre une grande entreprise et une ENATR ?", "", "", "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis gravida leo diam, et viverra ligula convallis rhoncus. Fusce nec tortor faucibus, accumsan ante eu, aliquet enim. Curabitur condimentum faucibus felis, sit amet adipiscing erat venenatis in. Donec iaculis libero eget enim lacinia, a porta velit ullamcorper. Cras tellus sem, consectetur nec lacus et, posuere ornare mi. Suspendisse ut eros sagittis, iaculis sapien quis, tempus tellus.</p>","" );
  INSERT INTO nodes (id, meta_id, `position`, status,quiz_id)
      VALUES (3,3,1,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 3, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 1
    UNION ALL
    SELECT 3, 3, 0;



  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
    VALUES (4, "choice", "Une grande entreprise", "", "", "","" );
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
    VALUES (4,4,0,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
      /**
        4 = node_id qui vient d'être inséré
      */
    SELECT n.ancestor, 4,n.depth+1
    FROM navigation AS n
      /**
        n.descendant = id du parent
       */
    WHERE n.descendant = 1
        /**
            union all select pour insérer une l'autoréférence d'un noeud
            4,4 = last_insert_id()
            1 = quiz id
            0 = depth
         */
    UNION ALL
    SELECT 4, 4, 0;



  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
    VALUES (5, "choice", "1.1.2", "R&D", "", "R&D et faisabilité","" );
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
    VALUES (5,5,0,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 5, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 4
    UNION ALL
    SELECT 5, 5, 0;

  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
    VALUES (6, "question", "vous souhaitez", "", "", "","" );
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
    VALUES (6,6,0,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 6, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 5
    UNION ALL
    SELECT 6, 6, 0;

  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
    VALUES (7, "choice", "Etudier la faisabilité d'un projet d'innovation technologique", "Etudier la faisabilité d'un projet", "", "","" );
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
    VALUES (7,7,0,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 7, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 5
    UNION ALL
    SELECT 7, 7, 0;

  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
    VALUES (8, "question", "votre objectif", "", "", "","" );
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
    VALUES (8,8,0,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 8, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 7
    UNION ALL
    SELECT 8, 8, 0;

  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
    VALUES (9, "choice", "En savoir plus sur la faisabilité d'un projet de logiciel", "", "", "","" );
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
    VALUES (9,9,0,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 9, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 5
    UNION ALL
    SELECT 9, 9, 0;

  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
    VALUES (10, "question", "votre objectif", "", "", "","" );
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
    VALUES (10,10,0,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 10, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 9
    UNION ALL
    SELECT 10, 10, 0;
  /*
  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
    VALUES (17, "choice", "test choice et lorem etc", "", "", "","" );
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
    VALUES (17,17,0,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 17, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 15
    UNION ALL
    SELECT 17, 17, 0;
/*
  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
    VALUES (18, "choice", "test choice et lorem etc bis", "", "", "","" );
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
    VALUES (18,18,0,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 18, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 9
    UNION ALL
    SELECT 18, 18, 0;
*/

  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
      VALUES (11, "help", "Subvention \"Recherche industrielle\"", "", "", "On entend par \"recherche industrielle\" : la recherche planifiée ou des enquêtes critiques visant à acquérir de nouvelles connaissances et aptitudes en vue de mettre au point de nouveaux produits, procédés ou services, ou d'entraîner une amélioration notable de produits, procédés ou services existants.","" );
    INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
      VALUES (11,11,0,"visible",1);
    INSERT INTO navigation (ancestor, descendant, depth)
      SELECT n.ancestor, 11, n.depth+1
      FROM navigation AS n
      WHERE n.descendant = 9
      UNION ALL
      SELECT 11, 11, 0;

INSERT INTO node_properties (id, type, name, shortname, description, text, path)
      VALUES (12, "url", "En savoir plus", "", "", "","http://www.google.be" );
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
    VALUES (12,12,3,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 12, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 11
    UNION ALL
    SELECT 12, 12, 0;

INSERT INTO node_properties (id, type, name, shortname, description, text, path)
      VALUES (13, "file", "Fiche Synthétique", "", "http://www.google.be", "","" );
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
    VALUES (13,13,1,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 13, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 11
    UNION ALL
    SELECT 13, 13, 0;

INSERT INTO node_properties (id, type, name, shortname, description, text, path)
  VALUES (18, "file", "Fiche Synthétique 2", "", "http://www.google.be", "","" );
INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
  VALUES (18,18,2,"visible",1);
INSERT INTO navigation (ancestor, descendant, depth)
  SELECT n.ancestor, 18, n.depth+1
  FROM navigation AS n
  WHERE n.descendant = 11
  UNION ALL
  SELECT 18, 18, 0;
/*
 INSERT INTO node_properties (id, type, name, shortname, description, text, path)
      VALUES (14, "choice", "En savoir plus sur la faisabilité d'un projet de logiciel", "", "http://www.google.be", "","" );
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
    VALUES (14,14,0,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 14, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 9
    UNION ALL
    SELECT 14, 14, 0;
*/
  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
    VALUES (15, "choice", "Une PME", "", "", "","" );
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
    VALUES (15,15,0,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 15, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 1
    UNION ALL
    SELECT 15, 15, 0;

  INSERT INTO node_properties (id, type, name, shortname, description, text, path)
    VALUES (16, "choice", "Un porteur de projets", "", "", "","" );
  INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
    VALUES (16,16,0,"visible",1);
  INSERT INTO navigation (ancestor, descendant, depth)
    SELECT n.ancestor, 16, n.depth+1
    FROM navigation AS n
    WHERE n.descendant = 1
    UNION ALL
    SELECT 16, 16, 0;

INSERT INTO node_properties (id, type, name, shortname, description, text, path)
  VALUES (17, "choice", "test choice et lorem etc", "", "", "","" );
INSERT INTO nodes (id, meta_id, `position`, status, quiz_id)
  VALUES (17,17,0,"visible",1);
INSERT INTO navigation (ancestor, descendant, depth)
  SELECT n.ancestor, 17, n.depth+1
  FROM navigation AS n
  WHERE n.descendant = 15
  UNION ALL
  SELECT 17, 17, 0;