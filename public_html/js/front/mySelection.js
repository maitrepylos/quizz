/**
 * Created by PAPERPIXEL
 * www.paperpixel.net
 **/
var MySelection = function(myQuizId) {
    /*
     private properties
     */
    var helps = [],
        storage_key = 'helps_quiz_id_',
        quizId = parseInt(myQuizId);

    if(!quizId)
        Logger.error('You need to specify a quiz id to use MySelection');

    /*
     private constructor
     */
    var construct = function() {
        storage_key += quizId;

        var myHelps = $.jStorage.get(storage_key);
        if(myHelps && myHelps.length) {
            helps = myHelps;
            Logger.log('-- Helps stored : ' + helps.length);
        } else {
            Logger.log('No help stored yet');
        }
        Logger.log('storage size: ' + ($.jStorage.storageSize() / 1024) + ' kb');
        Logger.log('-- Storage key is : ' + storage_key);
    };


    /*
     Private methods
     */

    /*
     public methods
     */

    /**
     * Adds an help to selection in local storage
     * @param helpId
     * @param helpMetaId
     */
    this.addToSelection = function(helpId, helpMetaId) {
        if(!this.findHelpInSelection(helpMetaId)) {
            helps.push({id: parseInt(helpId), meta_id: parseInt(helpMetaId)});
            $.jStorage.set(storage_key, helps);
            Logger.log('Added help meta id ' + helpMetaId + ' to storage');
            return true;
        } else {
            Logger.log('Warning: help id already in selection !');
            return false;
        }
    };

    /**
     * Removes an help from selection in local storage
     * @param helpMetaId
     */
    this.removeFromSelection = function(helpMetaId) {
        var help = this.findHelpInSelection(parseInt(helpMetaId));
        if(help) {
            var helpIndex = jQuery.inArray(help, helps);
            helps.splice(helpIndex, 1);
            $.jStorage.set(storage_key, helps);
            Logger.log('Removed help meta id ' + helpMetaId + ' (index ' + helpIndex + ') from storage');
            return true;
        } else {
            Logger.error('Warning: help id you want to remove ('+helpMetaId+') isn\'t in selection !');
            return false;
        }
    };

    /**
     * Swaps helps in array
     * @param helpId1
     * @param helpId2
     */
    this.swapHelps = function(helpId1, helpId2) {

        Logger.log('Swapped help id ' + helpId1 + ' and help id ' + helpId2);
    };

    /**
     * removes all helps from storage
     */
    this.clearHelps = function() {
        helps = [];
        $.jStorage.set(storage_key, helps);
    };

    /**
     * checks if help is in selection,
     * returns true if it is, false if it isn't
     * @param helpId
     */
    this.isHelpInSelection = function(helpId) {
        return jQuery.inArray(parseInt(helpId), helps) > -1;
    };


    this.findHelpInSelection = function(helpMetaId) {
        for(var i = 0; i < helps.length; i++) {
            if(helps[i]['meta_id'] == helpMetaId) {
                return helps[i];
            }
        }
        return false;
    };


    /*
     Getters / setters
     */
    this.getHelps = function() {
        return helps;
    };


    this.getHelpIds = function() {
        var helpIds = [];
        for(var i = 0; i < helps.length; i++) {
            helpIds.push(helps[i]['id']);
        }
        return helpIds;
    };

    // initialisation
    construct();
};