/**
 * Created by PAPERPIXEL
 * www.paperpixel.net
 **/
(function($) {
    var $form = $('#iframe-colors-form');
    var $exportContainer = $('#export-box');
    var url = App.quizUrl;

    $form.find('input.color').on('change', function(event) {
        changeIframeUrl();
//        changeCSS();
    });


    var changeIframeUrl = function () {
        var dataObject = $form.serializeArray();
        var dataArray = [];
        for(var obj in dataObject) {
            if(dataObject.hasOwnProperty(obj)) {
                dataArray.push(dataObject[obj].value);
            }
        }
        var dataString = dataArray.join('|');
        var content = '&lt;iframe src="' + url + '?colors=' + dataString + '" frameborder="0" width="600" height="1500">&lt;/iframe>';
        $exportContainer.html(content);
    };

    var changeCSS = function() {
//        var dataObject = $form.serializeArray();
//        for(var i in dataObject) {
//            if(dataObject.hasOwnProperty(i)) {
//                var color = dataObject[i];
//                if(color.name == 'node') {
//                    $('.container ul#breadcrumb li.branch ul.choices li a').css('backgroundColor', '#' + color.value);
//                    $('.container div.branch .nodes .node > a').css('backgroundColor', '#' + color.value);
//                }
//                if(color.name == 'nodeHover') {
//                    $('.no-touch .container .node > a:hover').css('backgroundColor', '#' + color.value)
//                }
//                if(color.name == 'nodeSelected') {
//                    $('body ul#breadcrumb li.branch ul.choices li.active a').css('backgroundColor', '#' + color.value)
//                }
//                if(color.name == 'action') {
//                    $('.btn-success').css('backgroundColor', '#' + color.value + ' !important');
//                }
//                if(color.name == 'actionSelected') {
//                    $('.btn-danger').css('backgroundColor', '#' + color.value + ' !important');
//                }
//            }
//        }
    };

    changeIframeUrl();
}(jQuery));

