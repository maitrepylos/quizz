/**
 * Created by PAPERPIXEL
 * www.paperpixel.net
 **/

var Statistics = {
    json: null,

    /**
     *
     * @param [callback]
     */
    fetchJson: function(callback) {
        var ctx = this;
        if(App.getPiwikDataUrl) {
            $.Transfer.get({
                data: {
                    startDate: App.statsStartDate,
                    endDate: App.statsEndDate
                },
                url: App.getPiwikDataUrl,
                onTransferDone: function(data) {
                    ctx.json = data;
                    callback(data);
                }
            });
        } else {
            console.error('getPiwikDataUrl not defined');
        }
    },

    getStatsByNode: function(nodeId) {
        var node = this.getNodeById(nodeId);

        if(node) {
            return {
                clicks: node.nb_actions,
                level: this.processLevel(node),
                global: this.processGlobal(node)
            };
        }

        return {
            clicks: 0,
            level: 0,
            global: 0
        };
    },

    getNodeById: function(nodeId) {
        //nodeId = 187;
        if(this.json) {
            for(var i = 0; i < this.json.length; i++) {
                if(parseInt(this.json[i].label, 10) == nodeId) {
                    return this.json[i];
                }
            }
        }
        return false;
    },

    getLevelNodeIds: function(nodeId) {
        var qnode = quizParser.findNode(nodeId);
        var parent = quizParser.findNode(qnode.breadcrumbs[qnode.breadcrumbs.length -1]);
        var levelIds = [];
        for (var i = 0; i < parent.choices.length; i++)
        {
            levelIds.push(parent.choices[i].id);
        }
        return levelIds;
    },

    processLevel: function(node){
        var ids = this.getLevelNodeIds(parseInt(node.label, 10));
        //var ids = this.getLevelNodeIds(94); // DUMMY DATa !!
        var total = 0;
        for(var elm in ids)
        {
          if(ids.hasOwnProperty(elm)) {
            var nod = this.getNodeById(ids[elm]);
            if(nod && nod.nb_actions) {
              total += nod.nb_actions;
            }
          }
        }

        return Math.floor(parseInt(node.nb_actions,10) / total * 100);
    },

    processGlobal: function(node)
    {
        var total = 0;
        for(var elm in this.json)
        {
            if (this.json.hasOwnProperty(elm))
            {
                total += parseInt(this.json[elm].nb_actions, 10);
            }
        }
        return Math.floor(parseInt(node.nb_actions,10) / total * 100);
    }
};