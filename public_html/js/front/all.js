/**
 * Created by PAPERPIXEL
 * www.paperpixel.net
 **/

!function ($) {
    var $body = $('body');

    $body.removeClass('nojs');
    // adds a class if we're in a iframe
    Utils.isIframe() ? $body.addClass('iframe') : $body.addClass('no-iframe');
    // adds a class if it's a device controlled by mouse
    // (useful for handling hover events in css)
    Utils.isTouchDevice() ? $body.addClass('touch') : $body.addClass('no-touch');

    if($.validator) {
        $.validator.setDefaults({
            debug: App.debug,
            errorPlacement: function(error, element) {
                $(element).parent().append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).parents('.form-group').addClass('has-error').removeClass('has-success');
                $(element).siblings('label').find('em').remove();
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).parents('.form-group').removeClass('has-error');
                var $label = $(element).siblings('label');
                if(!$label.find('em').length)
                    $label.append('<em><i class="glyphicon glyphicon-ok"></i></em>');
            },
            onfocusout: function(element) {
                $(element).valid();
            },
            onsubmit: App.debug ? false : function(element) {
                $(element).valid();
            },
            onkeyup: false
        });

        /**
         * default messages
         */
        $.validator.messages = {
            required: 'Veuillez remplir ce champs.',
            email: 'Veuillez entrer une adresse e-mail valide.',
            maxlength: ''
        };
    }

    $.Transfer.setDefaults({
        debug: true,
        serverMessageParam: 'message',
        onInvalidData: function(data, extra, jqXHR) {
            console.log('Invalid data from server. \nResponse: ', data);
        }
    });
}(window.jQuery);
