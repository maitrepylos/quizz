/**
 * Created by PAPERPIXEL
 * www.paperpixel.net
 **/
!function ($) {
    /**
     * Common variables
     */
    var animationSpeed = 400,
        templates = {},
        mySelection = '',
        quiz,
        pageTitle = document.title;


    /**
     * Breadcrumb logic
     */
    var $breadcrumbContainer = $('#breadcrumb'),
        breadcrumb = [],
        isShown = false,
        showChoices = function($container) {
            var $li = $container.find('li');
            var cellHeight = $li.height();

            $container.find('li').each(function(index, element) {
                $(element).animate({
                    top: (cellHeight * index) + 'px'
                }, 300);
            });
            $container.addClass('expanded');
        },
        hideChoices = function($container, makeAnimation) {
            if($container == undefined) {
                $container = $breadcrumbContainer.find('.choices');
            }
            if(makeAnimation == undefined) {
                makeAnimation = true;
            }

            if(makeAnimation) {
                $container.find('li').animate({
                    top: 0
                }, 300);
            } else {
                $container.find('li').css({top: 0});
            }
            $container.removeClass('expanded');
        },
        changeSelection = function($choice) {
            $choice.siblings().removeClass('active');
            $choice.addClass('active');
            hideChoices(null, false);
        },
        attachBreadcrumbEvents = function() {
            if(!Utils.isTouchDevice()) {
                $breadcrumbContainer.hoverIntent(function() {
                    showChoices($(this));
                }, function() {
                    hideChoices($(this));
                }, 'li.branch ul.choices');
            } else {
                $breadcrumbContainer.on('click', 'li.branch ul.choices', function() {
                    if($(this).parents('ul.choices').hasClass('expanded')) {
                        hideChoices($(this));
                    } else{
                        hideChoices();
                        showChoices($(this));
                    }
                });
            }

            $breadcrumbContainer.on('click', '.choices a[data-node-id]', function (event) {
                if(!Utils.isTouchDevice() || $(this).parents('ul.choices').hasClass('expanded')) {
                    var nodeId = $(this).attr('data-node-id');
                    var node = quizParser.findNode(nodeId);

                    // if currentNode has choices
                    if(node.choices && node.choices.length) {
                        loadQuestion(node);
                        $helpsBlockContainer.hide();
                    } else {
                        // if currentNode has helps
                        if(node.helps && node.helps.length) {
                            loadHelps(node.helps);
                        }
                    }

                    History.pushState({currentNode: node, nodeId: nodeId}, pageTitle, getPageName()+'?n=' + nodeId);

                    addMultipleNodesToBreadcrumb(node);
                    changeSelection($(this).parent());
                }
                event.preventDefault();
            });

            /**
             * When user clicks on home icon in breadcrumb
             */
            $breadcrumbContainer.on('click', "a[data-action='go-home']", function (event) {
                History.pushState({goHome: true}, pageTitle, getPageName());
                event.preventDefault();
            });

            /**
             * Hide breadcrumb list if user clicks anywhere on the page
             * (touch devices only)
             */
            $('html').click(function (event) {
                if(!$(event.target).parents('ul.choices').length) {
                    hideChoices();
                }
            });
        },
        addMultipleNodesToBreadcrumb = function(node) {
            removeNodesFromBreadcrumb();
            if(node.breadcrumbs && node.breadcrumbs.length) {
                for(var i = 0; i < node.breadcrumbs.length; i++) {
                    addNodeToBreadcrumb(quizParser.findNode(node.breadcrumbs[i]), false);
                }
                addNodeToBreadcrumb(node, false);
            }
            populateBreadcrumbTemplate();
        },
        addNodeToBreadcrumb = function(node, populate) {
            if(populate == undefined) populate = true;

            var parentNode = quizParser.getParent(node);
            if(parentNode) {
                breadcrumb.push({
                    node: parentNode, // TODO shouldn't copy all children !
                    nodeActiveId: node.id
                });
            }
            if(populate) populateBreadcrumbTemplate();
        },
        removeNodesFromBreadcrumb = function() {
            breadcrumb = [];
        },
        populateBreadcrumbTemplate = function() {
            if(!$breadcrumbContainer.is(':hidden')) {
                $breadcrumbContainer.hide();
            }
            $breadcrumbContainer.html(templates.breadcrumb({nodes: breadcrumb, app: App}));
            hideChoices(null, false);
            $breadcrumbContainer.fadeIn(animationSpeed);
        };

    /**
     * Questions logic
     */
    var $questionsContainer = $("#questions"),
        $helpsListContainer = $("#help-list"),
        $helpsBlockContainer = $("#help-block"),
        $relatedContainer = $("#related-block"),
        loadQuestion = function(node) {
            if(!$questionsContainer.is(':hidden')) {
                $questionsContainer.hide();
            }
            $breadcrumbContainer.hide();
            $helpsBlockContainer.hide();

            $questionsContainer.html(templates.questions({node: node, app: App}));
            $questionsContainer.fadeIn(animationSpeed);
        },
        loadHelps = function(helps) {
            if(!$helpsBlockContainer.is(':hidden')){
                $helpsBlockContainer.hide();
            }
            $questionsContainer.hide();

            $helpsListContainer.html(templates.helps({helps: helps, app: App}));
            $helpsBlockContainer.fadeIn(animationSpeed);
        },
        loadRelated = function(related) {
            $relatedContainer.html(templates.related({related: related, app: App}));
        },
        changeState = function(data) {
            var nodeId = data.nodeId,
                currentNode = data.currentNode;

            if(currentNode) {
                // if currentNode has choices
                if(currentNode.choices && currentNode.choices.length) {
                    loadQuestion(currentNode);
                    addMultipleNodesToBreadcrumb(currentNode);
                } else {
                  // Piwik magics happens here
                  if(typeof(window['_paq']) != 'undefined') {
                    if(nodeId && currentNode && currentNode.name) {
                      console.log('track: ' + currentNode.name);
                      _paq.push(['setCustomVariable',1,"NodeId",nodeId,"page"]);
                      _paq.push(['setCustomVariable',2,"NodeName",currentNode.name,"page"]);
                    }
                    _paq.push(['trackPageView']);
                  }

                    // if currentNode has helps
                    if(currentNode.helps && currentNode.helps.length) {
                        addMultipleNodesToBreadcrumb(currentNode);
                        loadHelps(currentNode.helps);
                        // if currentNode has related content
                        if(currentNode.related && currentNode.related.length) {
                            loadRelated(currentNode.related);
                        }
                        // add class "active" to element
                        setActive($(this).parent('li'));

                        currentNode = false;
                    } else {
                        // currentNode has no choices and no helps. There's a problem.
                        Logger.log('JSON not complete');
                        // TODO remove alert
                        alert('[Version de développement] le quiz n\'est pas complet.');
                        $questionsContainer.hide();
                        $helpsBlockContainer.hide();
                        addMultipleNodesToBreadcrumb(currentNode);
                    }
                }

            } else {
                // the node couldn't be found
                Logger.log('couldn\'t find node with id ' + nodeId);
            }
        },
        attachQuestionsEvents = function() {
            // when click on a question
            $questionsContainer.on('click', "a[data-action='navigate']", function (event) {
                var nodeId = $(this).attr('data-node-id');
                if(nodeId) {
                    var currentNode = quizParser.findNode(nodeId);
                    History.pushState({currentNode: currentNode, nodeId: nodeId}, pageTitle, getPageName()+'?n=' + nodeId);
                }
                event.preventDefault();
            });
        },
        attachHelpsEvents = function() {
            $helpsBlockContainer.on('click', "a[data-action='add-to-selection']", function (e) {
                var helpMetaId = $(this).attr('data-node-meta-id');
                var helpId = $(this).attr('data-node-id');
                var result = mySelection.addToSelection(helpId, helpMetaId);
                if(result) {
                    var newTemplate = templates.selectionRemove({id: helpId, meta_id: helpMetaId});
                    $(this).replaceWith(newTemplate);
                    updateSelectionLink();
                }
                e.preventDefault();
            });
            $helpsBlockContainer.on('click', "a[data-action='remove-from-selection']", function (e) {
                var helpMetaId = $(this).attr('data-node-meta-id');
                var helpId = $(this).attr('data-node-id');
                var result = mySelection.removeFromSelection(helpMetaId);
                if(result) {
                    var newTemplate = templates.selectionAdd({id: helpId, meta_id: helpMetaId});
                    $(this).replaceWith(newTemplate);
                    updateSelectionLink();
                }
                e.preventDefault();
            });
        },
        registerHelpers = function() {
            Handlebars.registerHelper('ifHelpInSelection', function(helpMetaId, options) {
                // TODO check if help is in localStorage and display button accordingly
                if(mySelection.findHelpInSelection(helpMetaId)) {
                    return options.fn(this);
                } else {
                    return options.inverse(this);
                }
            });
        },
        updateSelectionLink = function() {
            if(mySelection && mySelection.getHelps()) {
                var tpl = templates.selectionLink({
                    selectionCount: mySelection.getHelps().length,
                    selectionUrl: App.selectionUrl
                });
                $("#see-selection-link").html(tpl);

                // Help on selections tooltip
                $('.no-touch #selection-popup-link').tooltip({
                    trigger: 'click',
                    placement: 'left'
                });
                $('.touch #selection-popup-link').tooltip({
                    trigger: 'click',
                    placement: 'bottom'
                });
            } else {
                Logger.log('Warning: no MySelection object found !');
            }
        },
        getPageName = function() {
            var pathArray = window.location.pathname.split( '/' );
            return pathArray[pathArray.length-1];
        };


    var setActive = function($container) {
            $container.siblings().each(function () {
                unsetActive($(this));
            });
            !$container.hasClass('active') ? $container.addClass('active') : false;
        },
        unsetActive = function($container) {
            $container.hasClass('active') ? $container.removeClass('active') : false;
        };

    /**
     * start point of the logic after the quiz has been loaded
     */
    var init = function(myQuiz) {
        templates.breadcrumb = Handlebars.compile($("#breadcrumb-item-template").html());
        templates.questions = Handlebars.compile($("#questions-template").html());
        templates.helps = Handlebars.compile($("#helps-template").html());
        templates.related = Handlebars.compile($("#related-template").html());
        templates.selectionAdd = Handlebars.compile($("#add-to-selection-template").html());
        templates.selectionRemove = Handlebars.compile($("#remove-selection-template").html());
        templates.selectionLink = Handlebars.compile($("#selection-link-template").html());

        Handlebars.templates = templates;
        mySelection = new MySelection(App.quizId);

        registerHelpers();
        attachBreadcrumbEvents();
        attachQuestionsEvents();
        attachHelpsEvents();
        updateSelectionLink();

        // start navigation
        var state = History.getState();

        if(state.data.nodeId) {
            changeState({nodeId: state.data.nodeId, currentNode: quizParser.findNode(state.data.nodeId)});
        } else {
            // TODO remove this and show good question with php
            if('queryStringToJSON' in String.prototype) { // test if method is available in utils.js
                var node = state.url.queryStringToJSON();
                if(node['n']) { // if there's a "n" parameter in url, go to question
                    changeState({nodeId: node['n'], currentNode: quizParser.findNode(node['n'])});
                    return;
                }
            }
            loadQuestion(myQuiz);
        }
    };
    // setup the version to retrieve the json
    quizParser.quiz_ts = 'prod';

    if(App.previs){
        quizParser.quiz_ts = 'last';
    }
    /**
     * Fires when the quiz has been loaded
     * @param quiz
     */
    quizParser.onQuizReady = function(myQuiz) {
        if(!App.showStats) {
            beforeInit(myQuiz);
        } else {
            console.log('onquizready');
            Statistics.fetchJson(function(data) {
                beforeInit(myQuiz);
            });
        }
    };

    var beforeInit = function(myQuiz) {
        quiz = myQuiz;

        $('#loading-message').fadeOut(animationSpeed, function () {
            $(this).remove();
            init(myQuiz);
        });
    };

    $helpsBlockContainer.hide();
    $breadcrumbContainer.hide();
    $questionsContainer.hide();

    History.Adapter.bind(window,'statechange',function(){
        var state = History.getState();
        if(state.data.nodeId && state.data.currentNode) {
            changeState(state.data);
        }
        if(state.data.goHome) {
            loadQuestion(quiz);
        }
    });


}(window.jQuery);
 