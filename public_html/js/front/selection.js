
/**
 * Created by PAPERPIXEL
 * www.paperpixel.net
 **/
!function ($) {
    var mySelection = new MySelection(App.quizId),
        helps = [],
        templates = {},
        $container = $("#selection-container"),
        retrieveHelps = function() {
            helps = [];
            for(var i = 0; i < mySelection.getHelps().length; i++) {
                var helpMetaId = parseInt(mySelection.getHelps()[i]['meta_id']);
                var node = quizParser.findHelp(helpMetaId);
                if(node) {
                    helps.push(node);
                } else {
                    Logger.log('Warning: help not found. Removing from selection');
                    mySelection.removeFromSelection(helpMetaId);
                }
            }
        },
        populateTemplate = function() {
            $("#selection-container").html(templates.helps({helps: helps}));
            $('#send_mail_form').validate();
            attachSortable();

            if(mySelection.getHelps().length) {
                $('#right_menu').find('button').removeAttr('disabled');
            } else {
                $('#right_menu').find('button').attr('disabled', 'disabled');
            }
        },
        attachEvents = function() {
            /**
             * On help remove
             */
            $container.on('click', "a[data-action='help-remove']", function (e) {
                var conf = window.confirm('Etes-vous sûr(e) ?');
                if(conf) {
                    var helpMetaId = $(this).attr('data-help-meta-id');
                    if(helpMetaId) {
                        removeHelp(helpMetaId, $(this));
                    }
                }
                e.preventDefault();
            });

            /**
             * On selection clear (removes all helps)
             */
            $container.on('click', "a[data-action='selection-clear']", function (e) {
                var conf = window.confirm('Etes-vous sûr(e) ?');
                if(conf) {
                    $("#help-block").fadeOut(400, function () {
                        mySelection.clearHelps();
                        helps = [];
                        populateTemplate();
                    });
                }
            });


            /**
             * On open send mail modal
             */
            $("#send-mail-modal").on('shown.bs.modal', function(e) {
                $('#email').focus();
            });

            /**
             * On send mail
             */
            $(document).off('submit', '#send_mail_from').on('submit', '#send_mail_form', function(e) {
                e.preventDefault();
                var $button = $('button[data-action="send-mail"]');
                $(this).Transfer('post', {
                    data: {
                        email: $('#email').val(),
                        helps: mySelection.getHelpIds()
                    },
                    url: App.selectionSendMailUrl,
                    onTransferDone: function(data) {
                        $('#email').val('');
                        removeLoadingState($button, 'E-mail envoyé');
                        setTimeout(function() {
                            removeLoadingState($button);
                        }, 5000);
                    },
                    onTransferFailed: function(data) {
                        removeLoadingState($button, 'L\'e-mail n\'a pas été envoyé');
                    },
                    onInvalidData: function() {
                        removeLoadingState($button, 'E-mail envoyé');
                    }
                });
                $("#send-mail-modal").modal('hide');
                addLoadingState($button, 'Envoi en cours...');
            });

            /**
             * On pdf export
             */
            $("button[data-action='export-pdf']").on('click', function(e) {
                $('#help-inputs-container').html(templates.helpInputs({helps: helps}));
            });
        },
        removeHelp = function(helpMetaId, $elmt) {
            var res = mySelection.removeFromSelection(helpMetaId);
            if(res) {
                $elmt.parents('li.help').fadeOut(400, function () {
                    for(var i = 0; i < helps.length; i++) {
                        if(helps[i].meta_id == helpMetaId) {
                            helps.splice(i, 1);
                            $(this).remove();
                            populateTemplate();
                        } else {
                            $(this).show();
                        }
                    }
                });
            }
        },
        attachSortable = function() {
            $('#help-list').sortable({
                axis: 'y',
                forcePlaceholderSize: true,
                handle: '.handle-icon'
            }).bind('sortupdate', function(event, elmt) {
                    mySelection.clearHelps();
                    $("#help-list").find('.help').each(function () {
                        var helpMetaId = $(this).attr('data-help-meta-id');
                        var helpId = $(this).attr('data-help-id');
                        if(helpId && helpMetaId) {
                            mySelection.addToSelection(helpId, helpMetaId);
                        } else {
                            throw new Error('Could not find help id ('+helpId+') and meta id ('+helpMetaId+') on element.');
                        }
                    });
                });
        };

    var init = function() {
        templates.helps = Handlebars.compile($("#help-template").html());
        templates.helpInputs = Handlebars.compile($("#help-input-template").html());

        Handlebars.templates = templates;

        retrieveHelps();
        populateTemplate();
        attachEvents();
    };


    /**
     * Fires when the quiz has been loaded
     * @param quiz
     */
    quizParser.onQuizReady = function(quiz) {
        $('#right_menu').fadeIn(300);
        $('#loading-message').fadeOut(300, function () {
            $(this).remove();
            init();
        });
    };
    $('#right_menu').hide().removeClass('hidden');
}(window.jQuery);