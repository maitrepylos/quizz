// /* Backend client logic */

var Backend = {
    debug: true,
    published: '',

    init: function(){
        this.templateContext.levels.push(quizParser.quiz);
    },

    active_nodes: {id:[], level:[]},

    bindOnce: function() {
        $(document).on('submit', 'form', function(event){ // same as "button.async" | send form
            event.preventDefault(); // we don't want to follow the link
            var attr = $(this).attr("id");
            var attrS = attr.split('-');

            if(attrS[1] == 'edit' || attrS[1] == 'create'){
                Backend.createEditNode(attr, attrS);
            }
            if(attrS[1] == 'duplicate' && attrS[0] == 'help')
            {
                Backend.duplicateHelp(attr, attrS);
            }
        });
    },

    bind: function(){ // trigged by Template.draw

        // Actions
        $("button#previs").click(function(){
            App.previs = true;
            var iframeContent = Template.previewTemplate({url: App.quizUrlPreview});
            $('#preview-modal').html(iframeContent);
        });

        $("button#publish").click(function(){
            Backend.publishQuiz();
        });
        $("button#unpublish").click(function(){
            Backend.unpublishQuiz();
        });
        $("button#save").click(function(){
            Backend.publishQuiz(true);
        });


        $("a.async").click(function(event){
            var attr = $(this).attr("id");
            attr = attr.split("-"); // 1st : target type - 2nd : action - 3rd : id
            var type = attr[0];
            var action = attr[1];
            var id = attr[2];
            if(type == "node")
            {
                switch(action)
                {
                    case "open":
                        var level = $(this).attr("data-level");
                        Backend.openNode(id, level);
                        break;
                    case "delete":
                        Backend.deleteNode(id);
                        event.preventDefault();
                        break;
                    case "create":
                        //TODO : create node // There's no link to create node, only buttons
                        break;
                }
            }
        });

        $('[id^=urllocal-]').on("show.bs.modal", function(e){// when a help duplication modal is opened
            //retrieve list of all helps from any levels
            var placeh = $(this).attr("id").split('-')[1] == "create" ? "#urllocal_help_placeholder_create" : "#urllocal_help_placeholder_edit_" + $(this).attr("id").split('-').pop();
            $(placeh).Transfer("get", {
                "url": App.basePath + '/api/quiz/' + App.quizId + '/getHelpList',
                "spinner": false,
                "onTransferDone": function(data, ctx, extra){
                    if(data.length) {
                        var $sel = $('<input type="hidden" name="urlHelpId"/>').addClass('form-control');
                        $(ctx).html($sel);
                        $sel.select2({
                            data: {results: data, text: 'name'},
                            formatResult: function(help) {
                                var breadcrumbs = quizParser.getShortnameBreadcrumbs(help.id, 15);
                                if( breadcrumbs ) breadcrumbs = breadcrumbs.join(' > ');
                                return '<span class="help-list-breadcrumb">'+breadcrumbs+'</span>' + '<span class="help-list-name">'+help.name+'</em>';
                            },
                            formatSelection: function(help) {
                                return help.name;
                            },
                            formatNoMatches: function() { return 'Pas de résultat.'; }
                        })
                            .select2('focus');
                    } else {
                        $(ctx).html('Veuillez d\'abord ajouter une aide.');
                    }
                }
            });
        });

        $('[id^=relatedlocal-]').on("show.bs.modal", function(e){// when a help duplication modal is opened
            //retrieve list of all helps from any levels
            var placeh = $(this).attr("id").split('-')[1] == "create" ? "#relatedlocal_placeholder_create" : "#relatedlocal_placeholder_edit_" + $(this).attr("id").split('-').pop();
            console.log(placeh);
            $(placeh).Transfer("get", {
                "url": App.basePath + '/api/quiz/' + App.quizId + '/getHelpList',
                "spinner": false,
                "onTransferDone": function(data, ctx, extra){
                    console.log(ctx);
                    if(data.length) {
                        var $sel = $('<input type="hidden" name="relatedHelpId"/>').addClass('form-control');
                        $(ctx).html($sel);
                        $sel.select2({
                            data: {results: data, text: 'name'},
                            formatResult: function(help) {
                                var breadcrumbs = quizParser.getShortnameBreadcrumbs(help.id, 15);
                                if( breadcrumbs ) breadcrumbs = breadcrumbs.join(' > ');
                                return '<span class="help-list-breadcrumb">'+breadcrumbs+'</span>' + '<span class="help-list-name">'+help.name+'</em>';
                            },
                            formatSelection: function(help) {
                                return help.name;
                            },
                            formatNoMatches: function() { return 'Pas de résultat.'; }
                        })
                            .select2('focus');
                    } else {
                        $(ctx).html('Veuillez d\'abord ajouter une aide.');
                    }
                }
            });
        });

        $('[id^=help-duplicate-modal-]').on("show.bs.modal", function(e){// when a help duplication modal is opened
            //retrieve list of all helps from any levels
            $("#help_duplicate_placeholder").Transfer("get", {
                "url": App.basePath + '/api/quiz/' + App.quizId + '/getHelpList',
                "spinner": false,
                "onTransferDone": function(data, ctx, extra){
                    if(data.length) {
                        var $sel = $('<input type="hidden" name="duplicateId"/>').addClass('form-control');
                        $(ctx).html($sel);
                        $sel.select2({
                            data: {results: data, text: 'name'},
                            formatResult: function(help) {
                                var breadcrumbs = quizParser.getShortnameBreadcrumbs(help.id, 15);
                                if( breadcrumbs ) breadcrumbs = breadcrumbs.join(' > ');
                                return '<span class="help-list-breadcrumb">'+breadcrumbs+'</span>' + '<span class="help-list-name">'+help.name+'</em>';
                            },
                            formatSelection: function(help) {
                                return help.name;
                            },
                            formatNoMatches: function() { return 'Pas de résultat.'; }
                        })
                            .select2('focus');
                    } else {
                        $(ctx).html('Veuillez d\'abord ajouter une aide.');
                    }
                }
            });
        });
    },

    validate: function() {
        $('form').each(function() {
            $(this).validate({
                // by default, validate ignores hidden elements. We don't want that for CKeditor textareas
                ignore: '',
                // set focus on CKeditor textarea when invalid
                invalidHandler: function(event, validator) {
                    var $elmt = $(validator.errorList[0].element);
                    if($elmt.is('textarea')) {
                        CKEDITOR.instances[$elmt.attr('id')].focus();
                    }
                }
            });
        });
    },
    /**
     * Publish a quiz
     * @param [save] if quiz is already published, we display a save message
     */
    publishQuiz: function(save){
        var msg = 'Le quiz a bien été publié.';
        if(save) {
            msg = 'Le quiz a bien été enregistré.';
        }
        var data = {'quiz_id':App.quizId};
        $.Transfer.post({'action':'publishQuiz', 'data':data, 'onTransferDone':function(data){
            $.Transfer.alert(msg, $.Transfer.STATUS.SUCCESS);
            Backend.setPublished(true);
            Template.draw();
        }});
    },
    unpublishQuiz: function() {
        var data = {'quiz_id':App.quizId};
        $.Transfer.post({'action':'unpublishQuiz', 'data':data, 'onTransferDone':function(data){
            $.Transfer.alert("Le quiz a bien été passé en brouillon.", $.Transfer.STATUS.SUCCESS);
            Backend.setPublished(false);
            Template.draw();
        }});
    },
    duplicateHelp: function(attr, attrS)
    {
        data = { // to be sent to server
            "type": "help",
            "quiz_id": App.quizId,
            "parent_id": attrS[2]
        };
        data.help_id = $('[name=duplicateId]').val(); // selected helps

        var parent = quizParser.findNode(data["parent_id"] + ''); // get parent node
        var original = quizParser.findNode(data.help_id + ''); // get original node

        var extra = "new_" + data.type + data.parent_id; // don't have an id yet

        $('[id^=help-duplicate-modal-]').on('hidden.bs.modal', function(){
            //Template.redraw(); // redraw

            $.Transfer.post({'action': 'duplicateHelp', 'data': data, 'extra' : extra, 'onTransferDone' : function(data){
                Template.newIdUpdater(data.id, extra, 'duplicateHelp');
            }}); // post to server
        });

        $('[id^=help-duplicate-modal-]').modal("hide");
    },

    createEditNode: function(attr, attrS)
    {
        var call = false; // flag to authorise call to be made
        var urlLocal = false; // unless specified later we're not dealing with an local url
        var relatedLocal = false; // unless specified later we're not dealing with an local url

        data = {
            "type": attrS[0],
            "quiz_id": App.quizId
        };

        data[attrS[1] == 'edit' ? "node_id" : "parent_id"] = attrS[2]; // if "edit" node_id is current node; if "create" parent_id is current node

        if (attrS[1] == 'edit') data.id = attrS[2]; // if editing id = current id

        $('[data-id="'+ attr +'"]').each(function(){ // get datas out of form
            var tagName = $(this)[0].tagName;
            if (tagName == "INPUT")
            {
                data[$(this).attr("name").split("_")[1]] = $(this).val();
            }else if( tagName == "TEXTAREA" )
            {
                data[$(this).attr("name").split("_")[1]] = CKEDITOR.instances[$(this).attr('data-id')].getData();
            }
        });

        var nod;
        if(data.type == 'urllocal')
        {
            urlLocal = true;
            data.type = "url";
            nod = quizParser.findNode(parseInt($("[name=urlHelpId").val(),10));
            data.text = "%basePath%" + nod.breadcrumbs[nod.breadcrumbs.length-1];
        }

        if(data.type == 'relatedlocal')
        {
            relatedLocal = true;
            data.type = "related";
            nod = quizParser.findNode(parseInt($("[name=relatedHelpId").val(),10));
            data.text = "%basePath%" + nod.breadcrumbs[nod.breadcrumbs.length-1];
        }

        var found = quizParser.findNode(data[attrS[1] == "edit" ? "node_id" : "parent_id"]); // check existance of the node we want to work on ( actual node or parent node )

        //EDIT
        if(attrS[1] == 'edit' && found && found.type == data.type)  // EDIT
        {
            for(var elm in data){                                               // edit values
                if(elm != "quiz_id" && elm != "node_id" && elm != "type" && elm != "related")
                {
                    found[elm] = data[elm];                                 // object magic
                }
            }

            var c_mode = false;

            if (data.node_id.split("_")[0] == 'c'){ // we're working on a not_a_node node ( url & file are fake nodes)
                c_mode = true;
                related = quizParser.findNode(found.breadcrumbs[found.breadcrumbs.length - 1]).related;
            }else
            {
                related = found.related;
            }

            if(data.type == "file") // special case
            {
                var extra = data.id;
                if ($(".fileupload[data-id=" + attr + "]").val !== '' && $(".fileupload[data-id=" + attr + "]").val !== undefined)
                {
                    $(".fileupload[data-id=" + attr + "]").ajaxfileupload({ // we send the file here
                        'action': App.basePath + '/api/updateFileNode',
                        'params': {
                            'name': function(element){
                                $("#file-title[data-id=" + element[0].attributes["data-id"].nodeValue + "]").each(function(){ window.tempVal = $(this).val(); });
                                return window.tempVal;
                            },
                            'id': function(element){ return element[0].attributes["data-id"].nodeValue.split("-")[2]; },
                            'type': 'file',
                            'quiz_id': App.quizId
                        },
                        'onCancel': function() { console.log('cancelling: '); console.log(this); },
                        'onComplete': function(response){ Template.fileCleaner(response, extra);},
                        'onError': function(elm) {$.Transfer.alert(elm["message"]); window.location.reload();}
                    }).upload();
                }
            }

            var modal = false;
            if(urlLocal)
            {
                modal = $('#urllocal-modal-' + attrS[2]);
            }else if(relatedLocal){
                modal = $('#relatedlocal-modal-' + attrS[2]);
            }else{
                modal = $('#' + (data.type == 'choice' ? 'node' : data.type) + '-modal-' + attrS[2]);
            }

            modal.on('hidden.bs.modal', function(){
                if(data.type != 'file') Template.redraw(); // redraw
            });

            modal.modal("hide");

            call = true;

            //CREATE
        }else if(attrS[1] == 'create' && found) // CREATE
        {
            data["id"] = "new_" + data["name"] + "_" + Math.floor(Math.random() * 1000);    // we don't know yet what it will be ( server have to decide )

            data.choices = [];
            data.breadcrumbs = [];
            for(var b = 0; b < found.breadcrumbs.length; b++)
            {
                data.breadcrumbs.push(found.breadcrumbs[b]);
            }
            data.breadcrumbs.push(data.parent_id + '');

            if(data.type == "help" || data.type == "url" || data.type == "file" || data.type == "choice" || data.type == "related") // arrays type
            {
                var key = ( data.type == 'related' ? data.type : data.type + 's');
                if(!found[key])
                    found[key] = [data];    // create array if not existing
                else
                    found[key].push(data);  // or push data in if it does

                data.position = quizParser.findNode(data.parent_id)[data.type == 'related' ? data.type : data.type + 's'].length; // should be the last of the order
            }
            else
            {
                found[data.type] = data;    // so need to be put in place
            }

            if(data.type == "choice") // we could need to clean the active node list
            {
                //close all sublevel
                if(data.parent_id != this.active_nodes.id[this.active_nodes.id.length - 1])
                {
                    //not last
                    var index = this.active_nodes.id.indexOf(data.parent_id);
                    this.active_nodes.id.splice((index + 1), this.active_nodes.id.length); // nuke everything after it
                    this.active_nodes.level.splice((index + 1), this.active_nodes.level.length);
                    this.templateContext.levels.splice(index ? (index + 2) : 2, this.templateContext.levels.length);
                }
            }

            if(data.type == "file") // special case
            {
                var extra = data.id;
                $(".fileupload[data-id=" + attr + "]").ajaxfileupload({ // we send the file here
                    'action': App.basePath + '/api/createNode',
                    'params': {
                        'name': function(element){
                            $("#file-title[data-id=" + element[0].attributes["data-id"].nodeValue + "]").each(function(){ window.tempVal = $(this).val(); });
                            return window.tempVal;
                        },
                        'parent_id': function(element){ return element[0].attributes["data-id"].nodeValue.split("-")[2];},
                        'type': 'file',
                        'quiz_id': App.quizId
                    },
                    'extra': data.id,
                    'onCancel': function() { console.log('cancelling: '); console.log(this); },
                    'onComplete': function(response){ Template.fileCleaner(response, extra);},
                    'onError': function(elm) {$.Transfer.alert(elm["message"]); window.location.reload();}
                }).upload();
            }

            var modal;
            if(urlLocal)
            {
                modal = $('#urllocal-create-modal-' + attrS[2]);
            }else if(relatedLocal){
                modal = $('#relatedlocal-create-modal-' + attrS[2]);
            }else{
                modal = $('#' + (data.type == 'choice' ? 'node' : data.type) + '-create-modal-' + attrS[2]);
            }

            modal.on('hidden.bs.modal', function(){
                if(data.type != 'file') Template.redraw(); // redraw
            });

            modal.modal("hide");

            call = true;
        }else
        {
            $.Transfer.alert("Une erreur de communication est survenue, votre modification n'a pas été enregistrée ! ( node not found )");
        }

        if (call && data.type != 'file')
            $.Transfer.post({
                'action': (attrS[1] == 'edit' ? 'updateNode' : 'createNode'),
                'data': data,
                'extra': data.id,
                'onTransferDone': function(data, element, extra){
                    if(attrS[1] == 'create') Template.newIdUpdater(data.id + '', extra);
                    else if(typeof found.related !== undefined) {
                        if(found.related.length > 0) Template.updateJson(found.id + '');
                    }
                },
                'onTransferFailed': function(message, extra){ if(attrS[1] == 'edit') window.location.reload(); else Template.revertUpdater(extra); }
            }); // update db
        // in case of new node, id will be addad when call's reply arrives.
    },

    openNode: function(id, level){
        if(this.active_nodes.id.indexOf(id) < 0) // node is not allready open
        {
            var levelup = (parseInt(level, 10) + 1);
            var removeIndex = -1;

            if(levelup > quizParser.findNode(id)['breadcrumbs'].length)
            {
                var i = this.active_nodes.id.length; // we go down in level so nothing to close
            }
            else
            {
                for (var z = 0; z < this.active_nodes.level.length; z++) //a less strict version of indexOf
                {
                    if(this.active_nodes.level[z] == level)
                    {
                        removeIndex = z;
                    }
                }
            }

            if ( removeIndex >= 0 ) // close levels if any
            {
                this.active_nodes.id.splice(removeIndex, this.active_nodes.id.length);
                this.active_nodes.level.splice(removeIndex, this.active_nodes.level.length);
            }

            this.active_nodes.id.push(id); // a new level is open
            this.active_nodes.level.push(level);
            this.openLvl(id); // preprare ui
            Template.draw(); // impact ui
        }
    },

    openLvl: function(id){
        this.templateContext.levels = [quizParser.quiz]; // sarting from the initial node
        for( i = 0; i < this.active_nodes.id.length; i++)
        {
            this.templateContext.levels.push(quizParser.findNode(this.active_nodes.id[i]));
        }
    },

    deleteNode: function(id){
        var confirmation = confirm("Cliquez sur OK pour valider la suppression de cet élément.");
        if(confirmation)
        {

            if(quizParser.findNode(id)) //node exist
            {


                var idelete = id;
                $.Transfer.post({
                    'action': 'deleteNode',
                    'data': {'node_id': id +'', 'cascade': true},
                    'id' : id,
                    onTransferDone : function(data, element, id){
                        // delete in the template subjson
                        while(quizParser.findNode(idelete, quizParser.quiz, true)){} // delete it as much as we need
                        var p = Backend.templateContext.levels.length;
                        while(p)
                        {
                            p--;
                            if(Backend.templateContext.levels[p]["id"] == idelete+'')
                            {
                                Backend.templateContext.levels.splice(p, Backend.templateContext.levels.length);
                            }
                        }

                        p = Backend.active_nodes.id.length;
                        while(p)
                        {
                            p--;
                            if(Backend.active_nodes.id[p] == idelete+'')
                            {
                                Backend.active_nodes.level.splice(p, Backend.active_nodes.level.length);
                                Backend.active_nodes.id.splice(p, Backend.active_nodes.id.length);
                            }
                        }

                        Template.redraw(); // clean
                        //Template.updateJsonParent(ideleteParent); // clean
                    }
                });
            }else
            {
                //Error.message('error', );
                $.Transfer.alert("L'objet que vous essayez de supprimer est introuvable.");
            }
        }
    },

    isPublished: function() {
        if(this.published === '') {
            if($("#actions").data('quiz-published') == 1) {
                this.setPublished(true);
                return true;
            } else {
                this.setPublished(false);
                return false;
            }
        } else {
            return this.published;
        }
    },
    setPublished: function(published) {
        this.published = published;
    },

    templateContext: {"levels": []} // list of QuizObject, each beeing a reference of a node from quizParser.quiz
};

var Template = {
    init: function()
    {
        var context = this;
        $("#loading-message").fadeOut(300, function() {
            context.placeHolder = $("#client-template-placeholder");
            context.actions = $("#actions");

            context.template = Handlebars.compile($("#client-template").html());
            context.previewTemplate = Handlebars.compile($("#preview-template").html());
            context.actionsTemplate = Handlebars.compile($("#actions-template").html());

            Handlebars.templates = [context.template, context.previewTemplate, context.actionsTemplate]; // used in partial helper

            context.draw();
            Backend.bindOnce();

            $("#quiz-form-container").fadeIn(400);
        });
    },

    //proxy to prepare redraw (not used right now)
    redraw: function(){
        $('.modal-backdrop').hide();
        this.draw();
    },

    draw: function()
    {
        $('.modal').hide();
        this.placeHolder.html(this.template(Backend.templateContext));
        this.drawActions();
        Backend.validate();
        Backend.bind();
        Sortable.init();
        this.activeClass();
        this.wysiwyg();
        $(".popup-answer-text").truncate({
            moreText: 'Lire plus',
            lessText: 'Lire moins'
        });
        $('.modal').find('form').focusLoop();

        document.documentElement.style.overflow = 'scroll';
        document.body.scroll = 'auto';
    },

    drawActions: function() {
        this.actions.html(this.actionsTemplate({published: Backend.isPublished()}));
    },

    wysiwyg: function()
    {
        // attach CKeditor to every textarea
        $('textarea').each(function() {
            var $textarea = $(this);
            $textarea.ckeditor();

            // Check if data is valid on focus out
            CKEDITOR.instances[$textarea.attr('id')].on('blur', function() {
                $textarea.valid();
            });
        });

    },

    activeClass: function(){
        var a = Backend.active_nodes.id.length;
        $('a.active.async').removeClass('active'); // just to be sure
        while (a)
        {
            a--;
            $("li#" + Backend.active_nodes.id[a]).addClass('active');
        }
    },

    revertUpdater: function(extra){
        var n = quizParser.findNode(extra); // find the new node
        var parent_id = n.breadcrumbs[n.breadcrumbs.length - 1];

        var parent = quizParser.findNode(parent_id);

        var array_type = n.type == 'related' ? n.type : n.type + 's';

        var position = -1;
        for(var i = 0; i < parent[array_type].length; i++)
        {
            if(parent[array_type][i].id == n.id) { position = i; }
        }

        if (position > -1) parent[array_type].splice(position, 1);
        if($('.modal-backdrop').length) {
            setTimeout(function() {
                Template.redraw();
            }, 1000);
        } else {
            Template.redraw();
        }
    },

    newIdUpdater: function(new_id, extra, action){
        Logger.log("newIdUpdater");
        var n = quizParser.findNode(extra); // find the new node
        n.id = new_id; // update it's id

        if(extra == "undefined" || extra === undefined) {extra = "new_undefined";} // workaround probably deprecated

        if(action === "duplicateHelp"){
            this.updateJson(new_id); //find the newly created node and update json
        }else{

            for( var i = 0; i< Backend.active_nodes.id.length; i++) // clean actives
            {
                if (Backend.active_nodes.id[i] == extra)
                {
                    Backend.active_nodes.id[i] = n.id;
                }
            }

            $("[id$='"+ extra +"']").each(function(){ // update every link related to "new" node
                var id = $(this).attr("id");
                id = id.substring(0, id.length - extra.length);
                id += new_id;
                $(this).attr("id", id);
            });

            $("[data-target$='"+ extra +"']").each(function(){
                var id = $(this).attr("data-target");
                id = id.substring(0, id.length - extra.length);
                id += new_id;
                $(this).attr("data-target", id);
            });

            $("[data-id$='"+ extra +"']").each(function(){
                var id = $(this).attr("data-id");
                id = id.substring(0, id.length - extra.length);
                id += new_id;
                $(this).attr("data-id", id);
            });

            for (i in CKEDITOR.instances)
                if (i.split('-')[2] == extra)
                    CKEDITOR.instances[i.substring(0, i.length - extra.length) + new_id] = CKEDITOR.instances[i];

            this.updateRelatedHelps(n, new_id); // applies only for fakenode in related helps (files/urls);
        }
    },

    updateJson: function(node){ // node is the node we works with, it will be used to update the right level in ui // TODO: use $.Transfer instead of custom solution for messaging
        if (typeof node !== undefined){
            $.Transfer.alert("Mise à jour de l'interface", 1, true);
            quizParser.fetchQuiz("last", function(quiz){
                node = quizParser.findNode(node);
                //close all levels
                Backend.active_nodes.id = [];
                Backend.active_nodes.level = [];
                Backend.templateContext = [];
                //reopen untill right level
                Backend.openNode(node.breadcrumbs[node.breadcrumbs.length - 1], (node.depth - 2) + ''); //reopen it with proper data (id, level)
                Template.redraw();
                $('.modal-backdrop').remove();
            });
        }
        else
            $.bsod("Une erreur critique est survenue durant le transfert. La dernière action que vous avez entreprise s'est déroulée normalement mais la mise à jour de l'affichage à échoué.");
    },

    updateRelatedHelps: function(n, new_id)
    {
        if(new_id.split("_")[0] == 'c')
        {
            var parent = quizParser.findNode(n.breadcrumbs[n.breadcrumbs.length-1]);
            if(parent.related !== undefined)
                for(i=0; i<parent.related.length; i++)
                {
                    var rel = quizParser.findNode(parent.related[i]);
                    if(rel[n.type + 's'] === undefined) rel[n.type + 's'] = [n]; else rel[n.type + 's'].push(n);
                }
        }
    },

    fileCleaner: function(response, extra){ // A VERIFIER
        var n = quizParser.findNode(extra);
        n.id = response.node_id + '';
        n.path = response.path;

        $("[id$='" + extra + "'']").each(function(){
            var id = $(this).attr("id");
            id = id.substring(0, id.length - extra.length);
            id += response.node_id;
            $(this).attr("id", id);
        });
        $("[data-target$='" + extra + "']").each(function(){
            var id = $(this).attr("data-target");
            id = id.substring(0, id.length - extra.length);
            id += response.node_id;
            $(this).attr("data-target", id);
        });
        $("[data-id$='" + extra + "']").each(function(){
            var id = $(this).attr("data-id");
            id = id.substring(0, id.length - extra.length);
            id += response.node_id;
            $(this).attr("data-id", id);
        });

        this.updateRelatedHelps(n, response.node_id);

        if($('.modal-backdrop').length) {
            setTimeout(function() {
                Template.redraw();
            }, 1000);
        }else{
            Template.redraw();
        }
    }
};

var Sortable = {
    init : function() {
        $('.sortable').each(function() {
            var params = {                                          // init jquery sortable
                items: ':not(.undragable)',                         // not on add
                forcePlaceholderSize: true,
                distance: 10
            };
            if($(this).find('.handle-icon').length) {
                params.handle = '.handle-icon';
            }
            $(this).sortable(params).on('sortupdate', function() {  // and bind to an ajax call
                // call /api/updateOrder as :
                // { parent : parent_id, order : [child_one_id, child_two_id, child_n_id] }
                var data = {
                    "parent" : $(this).attr("id"),
                    "type" : $(this).attr("data-type"),
                    "order" : []
                };

                var parent = quizParser.findNode(data.parent);      // prepare local json

                var old_index = [];

                for(var e = 0; e < parent[data.type == "related" ? data.type : data.type + "s"].length; e++)
                {
                    old_index.push(parent[data.type == "related" ? data.type : data.type + "s"][e].id);
                }

                $(this).children('li.drag').each(function(){
                    data.order.push($(this).attr('id') + ''); // prepare data to send
                });

                $.Transfer.post({action: 'updateOrder', 'data': data});
                magnitude = 0;

                var moved;
                for (m = 0; m < data.order.length; m++) //spot what moved
                {
                    if(data.order[m] != old_index[m]){ //if moved
                        var mag = Math.abs(old_index.indexOf(data.order[m]) - data.order.indexOf(data.order[m]));
                        if(mag > magnitude){ // biggest movement
                            moved = data.order[m];
                            magnitude = mag;
                        }
                    }
                }

                parent[data.type == "related" ? data.type : data.type + "s"].move(old_index.indexOf(moved + ''), data.order.indexOf(moved + '')); // update json
            });
        });
    }
};

var Init = {
    init : function(withQuizz){
        Backend.init(); // create backend client controler
        if(withQuizz)
        {
            Template.init();
            Sortable.init();
        }else
        {
            Backend.bind();
        }
    },

    attachCallbacks: function(){
        quizParser.onQuizReady = function(quiz){
            Init.init(true);
        };
    }
};

!function ($)
{
    $('body').removeClass('nojs');
    $("#loading-message").hide().removeClass('hidden').fadeIn(300);
    $("#quiz-form-container").hide().removeClass('hidden');

    Init.attachCallbacks();
}(window.jQuery);

$(document).ready(function(){
    $.localScroll({
        lazy: true
    });
});