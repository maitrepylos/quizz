!(function($) {

    $('.rev-restore-action').on('click', function(e) {
        e.preventDefault();
        var revId = $(this).attr('data-rev-id');
        var msg = $(this).attr('data-confirm');

        if(msg != undefined) {
            if(!confirm(msg)) {
            }else{
                $.Transfer.post({
                    action:'restoreRevision',
                    data: {rev_id:revId},
                    onTransferDone: function() {
                        $.Transfer.alert("Le quiz a bien été mis à jour.", $.Transfer.STATUS.SUCCESS);
                        setTimeout(function(){location.reload(false)},300);
                    }
                });
            }
        }
    });

    $('.rev-preview-action').on('click', function(e) {
        $('#tree-view-container').empty();
        TreeView.show(d3, $(this).attr('data-rev-id'), $(this).attr('data-quiz-slug'), "#tree-view-container");
    });

    $('.rev-delete-action').on('click',function(e){
        e.preventDefault();
        var revId = $(this).attr('data-rev-id');
        var msg = $(this).attr('data-confirm');
        if(msg != undefined){
            if(confirm(msg)){
                $.Transfer.post({
                    action:'deleteRevision',
                    data: {rev_id:revId},
                    onTransferDone: function(){
                        location.reload(false);
                    }
                });
            }
        }
    });

})(jQuery);