/**
 * Created by PAPERPIXEL
 * www.paperpixel.net
 **/
!(function($) {
    /**
     * removes nojs class after loading
     * /!\ important ! Since .nojs is hidden in css,
     * if not removed the content won't be shown
     */
    $('body').removeClass('nojs');

    $.Transfer.setDefaults({
        debug: App.debug,
        serverMessageParam: 'message',
        basePath: App.apiPath,
        alertContainer: '#transfer_alert', // defined in back\_back.html.twig (just after header)
        alertElement: '<div class="alert alert-[[class]] alert-dismissable <!--fade-->">[[closeButton]]<span class="alert-message">[[message]]</span></div>',
        alertCloseButton: '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
        alert: true,
        errorMessage: 'Erreur',
        invalidMessage: 'Les données renvoyées par le serveur sont invalides.',
        warningMessage: 'Attention',
        successMessage: 'OK',
        getLoadingMessage: 'Chargement...',
        postLoadingMessage: 'Enregistrement...',
        errorClass: 'danger'
    });

    $.validator.setDefaults({
        debug: App.debug,
        errorPlacement: function(error, element) {
            $(element).parent().append(error);
        },
        highlight: function(element, errorClass, validClass) {
            $(element).parents('.form-group').addClass('has-error').removeClass('has-success');
            $(element).siblings('label').find('em').remove();
        },
        unhighlight: function(element, errorClass, validClass) {
            $(element).parents('.form-group').removeClass('has-error');
            var $label = $(element).siblings('label');
            if(!$label.find('em').length)
                $label.append('<em><i class="glyphicon glyphicon-ok"></i></em>');
        },
        onfocusout: function(element) {
            $(element).valid();
        },
        onsubmit: App.debug ? false : function(element) {
            $(element).valid();
        },
        onkeyup: false,
        rules: {
            "file_text": {
                extension: 'pdf'
            }
        },
        messages: {
            name: {
                required: 'Veuillez entrer un nom.'
            },
            "file_text": {
                extension  : 'Veuillez sélectionner un fichier PDF.'
            },
            question_shortname: {
                maxlength: 'La question raccourcie est limitée à 15 caractères.'
            }
        }
    });

    /**
     * default messages
     */
    $.validator.messages = {
        required: 'Veuillez remplir ce champs.',
        email: 'Veuillez entrer une adresse e-mail valide.',
        maxlength: ''
    };

    moment.lang('fr', {
      months : "janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre".split("_"),
      monthsShort : "janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.".split("_"),
      weekdays : "dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),
      weekdaysShort : "dim._lun._mar._mer._jeu._ven._sam.".split("_"),
      weekdaysMin : "Di_Lu_Ma_Me_Je_Ve_Sa".split("_"),
      longDateFormat : {
        LT : "HH:mm",
        L : "DD/MM/YYYY",
        LL : "D MMMM YYYY",
        LLL : "D MMMM YYYY LT",
        LLLL : "dddd D MMMM YYYY LT"
      },
      calendar : {
        sameDay: "[Aujourd'hui à] LT",
        nextDay: '[Demain à] LT',
        nextWeek: 'dddd [à] LT',
        lastDay: '[Hier à] LT',
        lastWeek: 'dddd [dernier à] LT',
        sameElse: 'L'
      },
      relativeTime : {
        future : "dans %s",
        past : "il y a %s",
        s : "quelques secondes",
        m : "une minute",
        mm : "%d minutes",
        h : "une heure",
        hh : "%d heures",
        d : "un jour",
        dd : "%d jours",
        M : "un mois",
        MM : "%d mois",
        y : "une année",
        yy : "%d années"
      },
      ordinal : function (number) {
        return number + (number === 1 ? 'er' : 'ème');
      },
      week : {
        dow : 1, // Monday is the first day of the week.
        doy : 4  // The week that contains Jan 4th is the first week of the year.
      }
    });

    /**
     * Sets focus on all first form element in modal when shown
     */
    $(document).on('shown.bs.modal', '.modal', function () {
        $(this).find(':input:enabled:visible:not(button):first').focus();
      $(this).attr('draggable') ? $(this).attr('draggable', 'false') : false;
//        $(this).find('input,textarea').css('-ms-user-select', 'initial');
    });


  /*
   Lousy workaround for a nasty bug
   In IE and Firefox, when an input is descendant of a draggable element, you can't select its text.
   Solution is to set draggable to false everytime we want to interact with a form element.
     */
  $('#quiz-form-container').on('focus', '.drag .modal input, .drag .modal textarea', function(e) {
    $(this).closest('.drag').attr('draggable', 'false');
  }).on('blur', function(e) {
      $(this).closest('.drag').attr('draggable', 'true');
    });
})(jQuery);

/**
 Adds and removes a loading state to a button
 */
addLoadingState = function($elmt, text) {
    text = !text ? 'Enregistrement en cours...' : text;

    $elmt.attr('data-loading-text', text);
    $elmt.button('loading');
};
removeLoadingState = function($elmt, text) {
    $elmt.button(!text ? 'reset' : text);
};