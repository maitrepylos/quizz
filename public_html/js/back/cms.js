/**
 * Created by PAPERPIXEL
 * http://www.paperpixel.net
 */

var CMS = {
  templates: {},
  $contentContainer: $('#content'),
  $toolLinkModalContainer: $("#tool-link-modal"),
  $footerModalContainer: $("#footer-modal"),

  /**
   * Everything starts here.
   */
  init: function() {
    this.templates.cms_tool_link_modal = Handlebars.compile($('#tool_link_modal_template').html());
    this.templates.cms_row = Handlebars.compile($('#tool-link-row-template').html());
    this.templates.cms_footer_modal = Handlebars.compile($('#cms_footer_modal_template').html());

    // do this after assigning and compiling templates !
    Handlebars.templates = this.templates;

    CKEDITOR.config.extraPlugins = 'justify';
    CKEDITOR.config.filebrowserUploadUrl = App.urls.cms_image_upload + "?CKEditor=cms_footer_input&CKEditorFuncNum=1&langCode=fr";

    this.bindEvents();
  },

  /**
   * Every events are declared here
   */
  bindEvents: function() {
    var cms = this;

    /**
     * - Change default quiz <-- not anymore (jbe)
     * - Display tool links
     * - Display footer everywhere
     */
    this.$contentContainer.on('change', '.single-value-form', function(e) {
      e.preventDefault();
      var $input = $(this);
      var value;
      if($(this).attr('type') == 'checkbox') {
        value = $(this).is(':checked');
      } else {
        window.testinput = $(this);
        value = $(this).val();
      }

      $(this).attr('disabled', 'disabled');
      $(this).Transfer('post', {
        url: $(this).attr('data-action-url'),
        data: {
          value: value
        },
        onTransferDone: function(data) {
          $input.removeAttr('disabled');
          if(data.message) {
            $.Transfer.alert(data.message, $.Transfer.STATUS.SUCCESS);
          }
        }
      });
    });

    /**
     * Change default quiz
     */
    this.$contentContainer.on('submit', '#default_quiz_form', function(e) {
      var $select = $('#default_quiz');
      console.log($select.val());

      $select.attr('disabled', 'disabled');

      $select.Transfer('post', {
        url: $select.attr('data-action-url'),
        data: {
          value: $select.val()
        },
        onTransferDone: function(data) {
          $select.removeAttr('disabled');
          if(data.message) {
            $.Transfer.alert(data.message, $.Transfer.STATUS.SUCCESS);
          }
        }
      });

      e.preventDefault();
    });

    /**
     * Add a link
     */
    this.$contentContainer.on('click', 'a[data-action="tool-link-update"]', function(e) {
      var link_hash = $(this).attr('data-link-hash');
      if(link_hash) {
        var link = $.parseJSON(link_hash);
        cms.$toolLinkModalContainer.html(cms.templates.cms_tool_link_modal({link: link, action_url: App.urls.link_update}));
      }
      e.preventDefault();
    });

    /**
     * Update a link
     */
    this.$contentContainer.on('click', 'a[data-action="tool-link-add"]', function(e) {
      cms.$toolLinkModalContainer.html(cms.templates.cms_tool_link_modal({action_url: App.urls.link_add}));

      e.preventDefault();
    });

    /**
     * Delete a link
     */
    this.$contentContainer.on('click', 'a[data-action="tool-link-delete"]', function(e) {
      if(confirm("êtes-vous sûr(e) de vouloir supprimer ce lien ? ")) {
        var link_id = $(this).attr('data-link-id');
        $(this).Transfer('post', {url: App.urls.link_delete, data: {link_id: link_id}})
          .on($.Transfer.EVENT.DONE, function(event, data) {
            console.log(data);
            cms.populateLinkTable(data);
          });
      }
    });


    /**
     * Submit tool links modal form
     */
    this.$toolLinkModalContainer.on('submit', 'form#tool_link_form', function(e) {
      $(this).Transfer('post', {url: $(this).attr('action'), data: $(this).serialize()})
        .on($.Transfer.EVENT.DONE, function(event, data) {
          cms.populateLinkTable(data);
          cms.$toolLinkModalContainer.modal('hide');
        });
      e.preventDefault();
    });

    /**
     * Open footer modal
     */
    this.$contentContainer.on('click', 'a[data-action="footer-update"]', function(e) {
      cms.$footerModalContainer.html(cms.templates.cms_footer_modal());
      var $textarea = cms.$footerModalContainer.find('#cms_footer_input');
      $textarea.html($('#cms_footer_content').html());
      $textarea.ckeditor();

      e.preventDefault();
    });

    /**
     * Submit footer form
     */
    this.$footerModalContainer.on('submit', 'form#footer_form', function(e) {
      $(this).Transfer('post', {url: App.urls.cms_footer_update, data: $(this).serialize()})
        .on($.Transfer.EVENT.DONE, function(event, data) {
          $("#cms_footer_content").html($('#cms_footer_input').val());
          cms.$footerModalContainer.modal('hide');
        });

      e.preventDefault();
    });
  },

  /**
   * Empties and refills the tool links table
   */
  populateLinkTable: function(data) {
    var $table = $("#links_table").find('tbody');
    $table.empty();
    for(var i = 0; i < data.length; i++) {
      var new_row = this.templates.cms_row({link: data[i]});
      $table.append(new_row);
    }
  }
};

!(function($) {
  CMS.init();
}(jQuery));