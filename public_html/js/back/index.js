/**
 * Created by PAPERPIXEL
 * www.paperpixel.net
 **/
!(function($) {
    var templates = {};
    var quizzes;
    var $quizContainer = $("ul#quiz-choice");

    var createEditQuiz = function(attr, attrS)
    {
        var id = attrS[2];

        if(attrS[1] == 'edit')
        {
            var $quizEditForm = $('form#quiz-edit-'+id);
            $('#quiz-edit-modal-' + id).modal("hide");

            $quizEditForm
                .Transfer('post', {
                    url: App.apiPath + 'editQuiz',
                    data: $quizEditForm.serialize()
                })
                .on('transfer.done', function(event, data) {
                    onAjaxDone(data);
                })
                .on('transfer.fail', function() {
                    populateTemplates();
                });
        } else {
            var $quizCreateForm = $('form#quiz-create');
            $quizCreateForm
                .Transfer('post', {
                    url: App.apiPath + 'createQuiz',
                    data: $quizCreateForm.serialize()
                })
                .on('transfer.done', function(event, data) {
                    onAjaxDone(data);
                });

            $('#quiz-create-modal').modal("hide");
        }
    };

    // after ajax call, receives quiz
    var onAjaxDone = function(data) {
        for(var i = 0; i < quizzes.length; i++) {
            if(quizzes[i].id == data.id) {
                quizzes[i] = data;
                populateTemplates();
                $('#quiz-edit-'+data.id).validate();
                return;
            }
        }
        quizzes.push(data);
        populateTemplates();
    };

    var deleteQuiz = function($elmt, id){
        var confirmation = confirm("Cliquez sur OK pour supprimmer ce quiz. ATTENTION : cette action est irréversible !");
        if(confirmation) {
            $elmt.Transfer('post',  {
                url: App.apiPath + 'deleteQuiz',
                data: {
                    id: id
                }
            });
            $elmt.on('transfer.done', function(event, data) {
                if(data.message == 'ok') {
                    for(var i = 0; i < quizzes.length; i++) {
                        if(quizzes[i].id == id) {
                            quizzes.splice(i, 1);
                            populateTemplates();
                        }
                    }
                }
            });
        }
    };

    var duplicateQuiz = function($elmt, id) {
        var conf = confirm("Cliquez sur OK pour dupliquer ce quiz.");

        if(conf) {
            $elmt.Transfer('post', {
                url: App.apiPath + 'duplicateQuiz',
                data: {
                    quiz_id: id
                }
            })
                .on('transfer.done', function(event, data) {
                    quizzes.push(data);
                    populateTemplates();
                })
                .on('transfer.fail', function() {
                    Logger.error('Can\'t duplicate quiz: server error');
                });
//            $.post(App.basePath + '/api/duplicateQuiz', {quiz_id: id}, function(data, res) {
//                if(res == 'success') {
//                    quizzes.push(data);
//                    populateTemplates();
//                } else {
//                    throw new Error('Can\'t duplicate quiz: server error');
//                }
//            });
        }
    };

    var attachEvents = function() {
        var $container = $(".container");

        $container.on('submit', 'form', function(event){ // same as "button.async"
            event.preventDefault(); // we don't want to follow the link
            var attr = $(this).attr("id");
            var attrS = attr.split('-');

            if(attrS[1] == 'edit' || attrS[1] == 'create')
            {
                createEditQuiz(attr, attrS);
            }
        });


        $container.on('click', 'a.async', function(event){
            var attr = $(this).attr("id");
            attr = attr.split("-"); // 1 target type - 2 action - 3 id
            var type = attr[0];
            var action = attr[1];
            var id = attr[2];
            if(action == "delete")
            {
                deleteQuiz($(this), id);
            } else if(action == "duplicate") {
                duplicateQuiz($(this), id);
            }
            event.preventDefault();
        });
    };

    var populateTemplates = function() {
        var content = templates.quizTemplate({quizzes: quizzes});
        $quizContainer.find('li:not(:last)').remove();
        $quizContainer.find('div').remove();
        $quizContainer.prepend(content);

        $('form').each(function() {
            $(this).validate();
        });
    };

    var init = function(myQuizzes) {
        var context = this;
        $("#loading-message").fadeOut(300, function() {
            quizzes = myQuizzes;
            templates.quizTemplate = Handlebars.compile($('#quiz-template').html());
            Handlebars.templates = templates;

            populateTemplates();

            attachEvents();

//            $(this).remove();

            $("#quiz-choice").fadeIn(400);
        });
    };

    $("#loading-message").hide().removeClass('hidden').fadeIn(300);
    $("#quiz-choice").hide().removeClass('hidden');

    // get all quizzes
    $('body')
        .Transfer('get', {
            url: App.apiPath,
            spinner: false
        })
        .on('transfer.done', function(event, data) {
            init(data);
        })
        .on('transfer.fail', function(event, data) {
            Logger.error('couldn\'t get quizzes');
        });
})(jQuery);

