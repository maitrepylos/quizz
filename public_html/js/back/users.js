/**
 * Created by PAPERPIXEL
 * www.paperpixel.net
 **/

var UsersManagement = {
    templates: {},
    $contentContainer: $('#content'),
    $modalContainer: $("#user-modal"),
    $noUserRow: $('.no-user'),
    init: function() {
        this.templates.user = Handlebars.compile($('#user-template').html());
        this.templates.user_modal = Handlebars.compile($('#user_modal_template').html());

        Handlebars.templates = this.templates;

        !this.isTableEmpty() ? this.$noUserRow.hide() : '';

        this.bindEvents();
    },
    bindEvents: function() {
        var usersManagement = this;

        /**
         * On user add
         */
        $('a[data-action="user-add"]').on('click', function (event) {
            usersManagement.populateModal(App.urls.user_add);

            event.preventDefault();
        });

        /**
         * On user update or on profile update
         */
        this.$contentContainer.on('click', 'a[data-action="user-update"]', function(event) {
            var user_hash = $(this).attr('data-user-hash');

            if(user_hash) {
                var user = $.parseJSON(user_hash);
                usersManagement.populateModal(App.urls.user_update, user);
            }

            event.preventDefault();
        });

        /**
         * On user delete
         */
        this.$contentContainer.on('click', 'a[data-action="user-delete"]', function(event) {
            var conf = confirm('Voulez-vous vraiment supprimer cet utilisateur ?');

            if(conf) {
                usersManagement.deleteUser($(this).attr('data-user-id'));
            }

            event.preventDefault();
        });

        /**
         * On user send mail
         */
        this.$contentContainer.on('click', 'a[data-action="user-send-email"]', function (event) {
            var conf = confirm('Voulez-vous vraiment envoyer un nouvel e-mail d\'activation à cet utilisateur ? Ce compte sera désactivé.');

            if(conf) {
                usersManagement.sendEmail($(this).attr('data-user-id'));
            }

            event.preventDefault();
        });

        /**
         * On user form submit
         */
//        $(document).on('submit', 'form#user_form', function(event) {
//            usersManagement.saveUser($(this));
//            event.preventDefault();
//        });
    },
    populateModal: function(action_url, user) {
        var params = {
            action_url: action_url,
            user: user,
            isItMe: user ? this.isItMe(user.id) : false
        };
        this.$modalContainer.html(this.templates.user_modal(params));

        var usersManagement = this;

        $('#user_form').validate();

        $(document).off('submit', 'form#user_form').on('submit', 'form#user_form', function(event) {
            event.preventDefault();
            usersManagement.$modalContainer.modal('hide');
            $(this).Transfer('post', {url: $(this).attr('action'), data: $(this).serialize()})
                .on($.Transfer.EVENT.DONE, function(event, data) {
                    usersManagement.populateTable(data);
                });
        });
    },
    deleteUser: function(user_id) {
        var context = this;

        $.Transfer.post({
            data: {
                id: user_id
            },
            url: App.urls.user_delete,
            onTransferDone: function() {
                var $tr = context.findUserInTable(user_id);
                if($tr) {
                    $tr.remove();
                    if(context.isTableEmpty()) {
                        $('.no-user').show();
                    }
                } else {
                    $.Transfer.alert('Pas de colonne ' + user_id + ' trouvée');
                }
            }
        });
    },
    sendEmail: function(user_id) {
        var context = this;

        $.Transfer.post({
            data: {
                user_id: user_id
            },
            url: App.urls.user_send_email,
            onTransferDone: function(data) {
                // TODO set 'activated' to false
                var $tr = context.findUserInTable(user_id);
                if($tr) {
                    $tr.replaceWith(context.templates.user({user: data}));
                    $.Transfer.alert('Mail envoyé.', $.Transfer.STATUS.SUCCESS);
                } else {
                    $.Transfer.alert('Pas de colonne ' + user_id + ' trouvée');
                }
            }
        });
    },
    populateTable: function(user) {
        var $tr = this.findUserInTable(user.id);
        var new_row = this.templates.user({user: user});
        if(!$tr.length) { // new user or me
            if(!this.isItMe(user.id)) { // new user
                $("#user_table").find("tbody").append(new_row);
            } else {
                $("#user_my_profile_edit").attr('data-user-hash', JSON.stringify(user));
            }
            !this.$noUserRow.is(':hidden') ? this.$noUserRow.hide() : false;
        } else { // existing user
            $tr.replaceWith(new_row);
        }
    },
    findUserInTable: function(id) {
        return $("#user_table").find('tr[data-user-id="'+id+'"]');
    },
    isItMe: function(id) {
        return $("#user_my_profile_edit").attr('data-user-id') == id;
    },
    isTableEmpty: function() {
        var $elmt = $("#user_table tbody").find('tr:not(.no-user)');
        return !$elmt.length;
    }
};

!(function($) {
    UsersManagement.init();
})(jQuery);