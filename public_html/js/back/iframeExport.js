/**
 * Created by PAPERPIXEL
 * www.paperpixel.net
 **/
(function($) {
    var $form = $('#iframe-colors-form');
    var $exportContainer = $('#export-box');
    var $previs = $('#previs');
    var $iframeContainer = $('.iframe-container');
    var url = App.quizUrl;
    var urlNoStats = App.quizUrlPreview;
    var iframe_url = '';

    $form.find('input.color').on('change', function(event) {
        changeExportContent();
    });

    $previs.on('click', function(event) {
        event.preventDefault();
        App.previs = true;
        content = '<iframe src="' + getIframeUrl(urlNoStats) + '" frameborder="0" width="600" height="1500"></iframe>';
        $iframeContainer.html(content);
    });

    function changeExportContent() {
        var content = '&lt;iframe src="' + getIframeUrl(url) + '" frameborder="0" width="600" height="1500">&lt;/iframe>';
        $exportContainer.html(content);
    }

    var getIframeUrl = function (baseUrl) {
        var dataObject = $form.serializeArray();
        var dataArray = [];
        for(var obj in dataObject) {
            if(dataObject.hasOwnProperty(obj)) {
                dataArray.push(dataObject[obj].value);
            }
        }
        var dataString = dataArray.join('|');
        var urlChar = '?';
        if(baseUrl.indexOf('?') > 0) {
            urlChar = '&';
        }
        return baseUrl + urlChar + 'colors=' + dataString+"&is_iframe=true";
    };

    changeExportContent();
}(jQuery));

