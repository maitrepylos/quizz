/**
 * Created by PAPERPIXEL
 * www.paperpixel.net
 **/

/** translate the id's breadcrumbs to an array containing shortnames, or if not shortname exist a substring of the name followed by " ... " 
* @param id {integer} The id of the node we work on
* @param [sub] {integer} is optional. The number of char to return from the name of a element that have no shortname, if not provided the complete name will be served
* @returns {Array} An array containing the translation of the breadcrumbs into shortnames or substring of the name
*/
quizParser.getShortnameBreadcrumbs = function(id, sub){
	if (typeof sub === "undefined") sub = -1;
	id = id + '';
	
	var node = this.findNode(id);
	var bCount = 0;
	var shortnameBreadcrumbs = [];
	if(node.breadcrumbs === undefined){
		Logger.log("ORPHANED NODE : " + id);
	}else{
		while(bCount < node.breadcrumbs.length)
		{
			var bcElm = this.findNode(node.breadcrumbs[bCount]);
                shortnameBreadcrumbs.push((bcElm.shortname && bcElm.shortname.length) ? bcElm.shortname : (bcElm.name.length > sub && sub > 0 ? bcElm.name.substr(0, sub) + "..." : bcElm.name));
			bCount++;
		}
		return shortnameBreadcrumbs;
	}
	return false;
};