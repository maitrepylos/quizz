/**
 * Created by PAPERPIXEL
 * www.paperpixel.net
 **/

/**
 * Date range picker by Dan Grossman
 * https://github.com/dangrossman/bootstrap-daterangepicker
 */

!(function($) {

    var defaultStartDate = moment().subtract('months', 1);
    var defaultEndDate = moment().subtract('days', 1);
    var piwikDateFormat = 'YYYY-MM-DD';
    var displayFormat = 'DD MMMM YYYY';
    var $datePicker = $('input#datepicker');
    var $loader = $('#stats_loader');
    var $content = $('#stats_container');
    var $title = $content.find('h2');
    var $iframe_container = $('#iframe_container');

    moment.lang('fr');

  if($('#datepicker-container').length) {
    // show dates in input
    $datePicker.attr('value', defaultStartDate.format(displayFormat) + ' - ' + defaultEndDate.format(displayFormat));

    showIframe(defaultStartDate, defaultEndDate);
  }

    $datePicker.daterangepicker({
        opens: 'right',
        maxDate: moment(),
        format: displayFormat,
        startDate: defaultStartDate,
        endDate: defaultEndDate,
        /*ranges: { // nice idea but calendars are hidden, so it's not very interesting
         'Depuis une semaine': [moment().subtract('weeks', 1), defaultEndDate],
         'Depuis un mois': [moment().subtract('months', 1), defaultEndDate],
         'Depuis un an': [moment().subtract('years', 1), defaultEndDate]
         },*/
        locale: {
            applyLabel: 'Appliquer',
            cancelLabel: 'Annuler',
            fromLabel: 'De',
            toLabel: 'à',
            weekLabel: 'S',
            customRangeLabel: 'Custom Range',
            daysOfWeek: moment()._lang._weekdaysMin.slice(),
            monthNames: moment()._lang._monthsShort.slice(),
            firstDay: 1
        }
    }, function(start, end) {
        showIframe(start, end);
    });

    function showIframe(start, end) {
        updateTitle(start, end);
        showContent();

        var url = App.quizUrlNoStats;
        url += "&showstats=true&is_iframe=true";
        url += "&startdate=" + start.format(piwikDateFormat);
        url += "&enddate=" + end.format(piwikDateFormat);
        content = '<iframe src="' + url + '" frameborder="0" width="600" height="1500"></iframe>';
       $iframe_container.html(content);
    }

    /**
     * Call piwik api
     * @param startDate start date
     * @param endDate end date
     */
        function callPiwik(startDate, endDate) {
            showLoader();
            updateTitle(startDate, endDate);

            $.Transfer.get({
                data: {
                    startDate: startDate.format('YYYY-MM-DD'),
                    endDate: endDate.format('YYYY-MM-DD')
                },
                url: App.urls.get_stats_from_piwik,
                onTransferDone: parsePiwikData
            });
        }

    function parsePiwikData(data) {
        // TODO call piwik api and do stuff

        console.log(data);
        showContent();
    }

    function updateTitle(startDate, endDate) {
        var $startDateSpan = $title.find('span.start-date');
        var $endDateSpan = $title.find('span.end-date');

        $startDateSpan.html(startDate.format('DD MMMM YYYY'));
        $endDateSpan.html(endDate.format('DD MMMM YYYY'));
    }

    function showLoader() {
        if(!$content.hasClass('hidden')) {
            $content.fadeOut(400, function() {
                $content.addClass('hidden');
                showLoader();
            });
            return;
        }
        if($loader.hasClass('hidden')) {
            $loader.removeClass('hidden').hide();
            $loader.fadeIn(400);
        }
    }

    function showContent() {
        if(!$loader.hasClass('hidden')) {
            $loader.fadeOut(400, function() {
                $loader.addClass('hidden');
                showContent();
            });
            return;
        }
        if($content.hasClass('hidden')) {
            $content.removeClass('hidden').hide();
            $content.fadeIn(400);
        }
    }
})(jQuery);