/*
 *  Transfer
 *
 *  jQuery communication abstraction layer
 *  Talk with your server and display feedback nicely.
 *
 *  Made by Jonathan Berger and Edwin Joassart
 *  http://www.paperpixel.net
 *  Under MIT License
 */
;(function ( $, window, document, undefined ) {
    var pluginName = "Transfer",
        defaults = {
            /* transfer */
            url: '',
            cache: false,
            debug: false,
            serverMessageParam: 'msg', // if server return an error, it should return a json with a unique "msg"
            validate: [],
            prefix: '',
            onTransferDone: '',
            onTransferFailed: '',
            onInvalidData: '',
            basePath: '',
            action: '',
            /* alert */
            alert: false,
            spinner: true,
            hideErrorAlerts: false,
            hideDelay: 3000,
            fadeTransition: 200,
            errorMessage: 'Error !',
            invalidMessage: 'Data from server is invalid.',
            warningMessage: 'Warning !',
            successMessage: 'Success !',
            getLoadingMessage: 'Loading...',
            postLoadingMessage: 'Saving...',
            errorClass: 'error',
            warningClass: 'warning',
            successClass: 'success',
            loadingClass: 'loading',
            alertContainer: '',
            alertElement: '<div class="[[class]]">[[message]] [[closeButton]]</div>',
            alertCloseButton: '<a href="#" class="transfer-alert-close">Close</a>'
        },
        METHOD = {
            GET: "get",
            POST: "post"
        },
        STATUS = {
            ERROR: 0,
            WARNING: 1,
            SUCCESS: 2
        },
        EVENT = {
            // STARTED is fired just before every call to server
            STARTED: 'transfer.started',
            // DONE is fired when a transfer is done successfully
            DONE: 'transfer.done',
            // @deprecated
            FAIL: 'transfer.fail',
            // FAILED is fired when the server responds with an error
            FAILED: 'transfer.failed',
            // INVALID_DATA is fired when the transfer is successful but the data sent by server is not JSON valid
            INVALID_DATA: 'transfer.invalid_data',
            // FINISHED is fired when a transfer is finished, no matter if it failed or succeeded
            FINISHED: 'transfer.finished'
        };

    function Plugin ( element, options ) {
        this.element = element;
        typeof options != 'string' ? this.settings = $.extend( {}, defaults, options ) : this.settings = $.extend( {}, defaults );
        this._defaults = defaults;
        this._name = pluginName;
        this._init();
    }

    Plugin.prototype = {
        /**
         * Initial method
         * @private
         */
        _init: function () {
            $.ajaxSetup({
                cache: this.settings.cache
            });

            if(this.settings.alert && this.settings.alertContainer == '') {
                this._error('You have to define a container for alerts.');
            }
        },

        /**
         * Submits form and sends to server.
         * @param params
         */
        submit: function(params) {
            // error handling
            if(!this.element) {
                this._error('You can\'t use submit method without an element (eg. $(element).Transfer(\'submit\', {}).');
                return;
            }
            if(!$(this.element).is('form')) {
                this._error('To use the submit method, your element must be a form');
                return;
            }
            if(params == undefined) params = {};
            var plugin = this;
            $(this.element).on('submit', function(e) {
                e.preventDefault();
                plugin._log('submitting form');

                var dataString = $(this).serialize();

                // if there isn't a prefix set, check in defaults
                if(params.prefix == undefined)
                    params.prefix = plugin.settings.prefix;
                // if there's a prefix set, remove it from all parameters
                if(params.prefix && params.prefix.length) {
                    // but first we need to check if prefix is ok
                    params.prefix = params.prefix.split('&').join('').split('=').join('');
                    // and then we remove it // TODO : remove only if between an & (or first char) and an equal sign ( to prevent altering data that contains the prefix )
                    dataString = dataString.split(params.prefix).join('');
                }

                if(!params.url || params.url == '') {
                    var action = $(this).attr('action');

                    if(action != '#') {
                        params.url = action;
                    }
                }
                params.data = dataString;
                plugin.post(params);
            });
        },

        /**
         * Send data to server with GET method
         * @param params
         */
        get: function(params) {
            if(params == undefined) params = {};
            this._log('sending GET data');
            params['method'] = METHOD.GET;
            this._send(params);
        },

        /**
         * Send data to server with POST method
         * @param params
         */
        post: function(params) {
            if(params == undefined) params = {};
            this._log('sending POST data');
            params['method'] = METHOD.POST;
            this._send(params);
        },

        /**
         * Sends data to server.
         * @param params
         * @param params.url if not defined here, the one defined in constructor is used.
         * @throws Error if no url is defined
         * @throws Error if no data is defined
         * @private
         */
        _send: function(params) {
            if(this.element)
                $(this.element).triggerHandler(EVENT.STARTED);

            // merge local settings with global ones
            params = $.extend({}, this.settings, params);

            if(params.alert && params.spinner) {
                var container = this._parseAlertElement(
                    this.settings.alertElement,
                    this.settings.loadingClass,
                    params.method == METHOD.POST ? params.postLoadingMessage : params.getLoadingMessage,
                    ''
                );
                var $container = $(container);
                $(this.settings.alertContainer).hide().append($container).fadeIn(this.settings.fadeTransition);
                params['_alertContainer'] = $container;
            }

            if(params.action) {
                if(params.basePath) {
                    params.url = params.basePath + params.action;
                } else {
                    this._error('You should define a basePath if you plan to use action.');
                    return;
                }
            }

            // sanity checks
            if(params.method != undefined && !(params.method == METHOD.GET || params.method == METHOD.POST)) {
                this._error('Method should be either ' + METHOD.GET + ' or ' + METHOD.POST + '.');
            }
            params.method == undefined ? params.method = METHOD.GET : null;

            if(params.url == undefined || params.url == '') {
                if(params.basePath) {
                    this._log('No url or action defined, trying with basePath.');
                    params.url = params.basePath;
                } else {
                    this._error('You should define a url, either globally, in constructor or in send method.');
                    return;
                }
            }

            // send data to server
            $.ajax(params.url, {
                data: params.data,
                type: params.method,
                success : this._onDone(params),
                error : this._onFail(params)
            });
        },

        /**
         * Handles response from server and triggers ajaxDone event
         * @param params
         * @returns {Function}
         * @private
         */
        _onDone: function(params) {
            var plugin = this;
            return function(data, status, jqXHR) {
                if(data) {
                    // If data is not JSON
                    if(!$.isPlainObject(data) && !$.isArray(data)) {
                        try {
                            // try to convert to JSON
                            data = $.parseJSON(data);
                        } catch(e) {
                            plugin._onInvalidData(data, params, jqXHR);
                            return;
                        }
                    }
                    // If data is incorrect
                    if(params.validate && $.isArray(params.validate)) {
                        if(!plugin._isValid(data, params.validate)) {
                            plugin._onInvalidData(data, params, jqXHR);
                            return;
                        }
                    }

                    if(params._alertContainer) {
                        params._alertContainer.fadeOut(plugin.settings.fadeTransition, function() {
                            $(this).remove();
                        });
                    }

                    plugin._log('ajax transfer done');
                    if(plugin.element) {
                        $(plugin.element).triggerHandler(EVENT.DONE, [data, params.extra, jqXHR]);
                        $(plugin.element).triggerHandler(EVENT.FINISHED);
                    }
                    if($.isFunction(params.onTransferDone)) {
                        params.onTransferDone(data, plugin.element, params.extra);
                    }
                } else {
                    plugin._error('No data from server');
                }
            }
        },

        /**
         * Handles response from server and triggers ajaxFailed event
         * @param params
         * @returns {Function}
         * @private
         */
        _onFail: function(params) {
            var plugin = this;
            return function(jqXHR, status, errorThrown) {
                var message = '';
                if(jqXHR.responseText != '') {
                    try {
                        var data;
                        // If data is not JSON
                        if(!$.isPlainObject(jqXHR.responseText) && !$.isArray(jqXHR.responseText)) {
                            try {
                                // try to convert to JSON
                                data = $.parseJSON(jqXHR.responseText);
                            } catch(e) {
                                plugin._onInvalidData(jqXHR.responseText, params, jqXHR);
                                return;
                            }
                        }
                        message = data[params.serverMessageParam];
                    } catch(e) {
                        plugin._error('Couldn\'t parse JSON sent by server. \nResponse: ' + jqXHR.responseText + '\nError: ' + e.message);
                        message = e.message;
                    }
                } else {
                    plugin._error('No data from server');
                }
                if(params.alert) {
                    plugin.alert({message: message, status: STATUS.ERROR, _alertContainer: params._alertContainer});
                }
                if(plugin.element) {
                    $(plugin.element).triggerHandler(EVENT.FAIL, [message, params.extra, jqXHR, errorThrown]);
                    $(plugin.element).triggerHandler(EVENT.FAILED, [message, params.extra, jqXHR, errorThrown]);
                    $(plugin.element).triggerHandler(EVENT.FINISHED);
                }
                if($.isFunction(params.onTransferFailed)) {
                    params.onTransferFailed(message, params.extra, jqXHR, errorThrown);
                }
                return false;
            }
        },

        /**
         * Fired if data coming from the server is not JSON
         * @param data
         * @param params
         * @param jqXHR
         * @private
         */
        _onInvalidData: function(data, params, jqXHR) {
            this._error('Server returned invalid data.');
            if(this.element) {
                $(this.element).triggerHandler(EVENT.INVALID_DATA, [data, params.extra, jqXHR]);
                $(this.element).triggerHandler(EVENT.FINISHED);
            }
            if($.isFunction(params.onInvalidData)) {
                params.onInvalidData(data, params.extra, jqXHR);
            }
            this.alert({message: params.invalidMessage, status: STATUS.ERROR, _alertContainer: params._alertContainer});
        },

        /**
         * Check if json coming from server has required attributes.
         * @param data json from server
         * @param required. If empty, returns true
         * @returns {boolean}
         * @private
         */
        _isValid: function(data, required) {
            var isIt = true;
            for(var i = 0; i < required.length; i++) {
                isIt = false;
                for(var key in data) {
                    if(data.hasOwnProperty(key)) {
                        if(key == required[i]) {
                            isIt = true;
                            break;
                        }
                    }
                }
            }
            return isIt;
        },

        /**
         * Shows an alert with message and different statuses
         * @deprecated
         * @param params
         */
        showAlert: function(params) {
            this.alert(params);
        },

        /**
         * Shows an alert with message and different statuses
         * @params : params
         */
        alert: function(params) {
            var message = params.message;
            params.status == undefined ? params.status = 0 : false;
            var plugin = this;

            if(this.settings.alert) {
                if(typeof params.message != 'string' || params.message == "" || params.message == undefined) params.message = this.settings.errorMessage;

                var cssClass = '';
                switch(params.status) {
                    case STATUS.WARNING :
                        cssClass = this.settings.warningClass;
                        break;
                    case STATUS.SUCCESS :
                        cssClass = this.settings.successClass;
                        break;
                    default:
                    case STATUS.ERROR :
                        cssClass = this.settings.errorClass;
                        break;
                }

                var container = this._parseAlertElement(
                    this.settings.alertElement,
                    cssClass,
                    message,
                    this.settings.alertCloseButton
                );
                var $container = $(container);

                if(params._alertContainer) {
                    params._alertContainer.fadeOut(200, function() {
                        $(this).replaceWith($container);
                        $container.hide().fadeIn(200);
                    });
                } else {
                    $(this.settings.alertContainer).hide().append($container).fadeIn(this.settings.fadeTransition);
                }

                if((params.status == STATUS.ERROR || params.status == STATUS.WARNING) && (this.settings.hideErrorAlerts || params.hide)) {
                    setTimeout(function() {
                        $container.fadeOut(plugin.settings.fadeTransition, function() {
                            $(this).remove();
                        });
                    }, params.hideDelay || this.settings.hideDelay);
                }
            }
        },

        /**
         * Get container with data in params
         * @param container the container to parse on
         * @param alertClass
         * @param message
         * @param closeButton
         * @returns {string}
         * @private
         */
        _parseAlertElement: function(container, alertClass, message, closeButton) {
            return container
                .split("[[class]]").join(alertClass)
                .split("[[message]]").join(message)
                .split("[[closeButton]]").join(closeButton);
        },

        /**
         * log method. No log if not in debug mode
         * @param object
         * @private
         */
        _log: function(object) {
            this.settings.debug ? console.log('Transfer: ', object) : false;
        },

        /**
         * error log method. No error thrown if not in debug mode
         * @param object
         * @private
         */
        _error: function(object) {
            this.settings.debug ? console.error('Transfer error\n', object) : false;
        }
    };

    $.fn.Transfer = function ( optionsOrString, params ) {
        return this.each(function() {
            if ( !$.data( this, "plugin_" + pluginName ) ) {
                $.data( this, "plugin_" + pluginName, new Plugin(this, optionsOrString) );
            }

            if ((typeof optionsOrString == "string" || optionsOrString instanceof String)) {
                if($.isFunction(Plugin.prototype[optionsOrString]) && optionsOrString[0] != '_') {
                    $.data(this, 'plugin_' + pluginName)[optionsOrString](params);
                } else {
                    $.error(optionsOrString + ' method not found.');
                }
            }
        });
    };

    $.Transfer = {
        /**
         * define default options globally
         * @param options
         */
        setDefaults: function(options) {
            $.extend(defaults, options);
        },

        /**
         * GET default options
         */
        getDefaults: function() {
            return defaults;
        },

        /**
         * Proxy method to show alert.
         * @caution an Alert container must be defined globally with $.Transfer.setDefaults
         * @param message {string}
         * @param [status] {int}
         * @param [hide] {boolean} hide after delay, default : false
         * @param [hideDelay] {int} delay before hiding
         */
        alert: function(message, status, hide, hideDelay) {
            new Plugin(null, {}).alert({message: message, status: status, hide: hide, hideDelay: hideDelay});
        },

        /**
         * Proxy method for server GET request
         * @caution Events are not available since no DOM element is associated
         * @param params
         */
        get: function(params) {
            new Plugin(null, params).get({});
        },

        /**
         * Proxy method for server POST request
         * @caution Events are not available since no DOM element is associated
         * @param params
         */
        post: function(params) {
            new Plugin(null, {}).post(params);
        },

        /**
         * Proxy constants
         */
        EVENT: EVENT,
        STATUS: STATUS,
        METHOD: METHOD
    };

})( jQuery, window, document );