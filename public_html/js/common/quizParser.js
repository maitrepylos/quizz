var QuizParser = function() {
    this.quiz = "";
    this.quiz_ts = "last";
    /**
     * Fetches the quiz json object from the server
     * @param quiz_ts (optional) : timestamp of the quiz we want to get OR 'prod' OR 'last'
     * @return : nothing
     * @callbacks : call onQuizReady and pass the retrieved quiz
     */
    this.fetchQuiz = function(quiz_ts, fwd, fail) {
        if(!quiz_ts) quiz_ts = this.quiz_ts;

        var success = $.proxy(function(data) {
            this.quiz = data;
            //Logger.log(this.quiz);
            if(!fwd)
            {
                this.onQuizReady(this.quiz);
            }else
            {
                fwd(this.quiz);
            }
            
        }, this);

        var error = function(){
            //  alert("Ce quizz n'existe pas !");
            //Logger.log("quizz not found");
            if (!fail)
                this.onQuizNotReady();
            else
                fail();
        };

        //$.getJSON(App.basePath + "/json/" + App.quizName + "/" + quiz_ts, success); // App.quizSlug is defined in back/quiz.html.twig in the prescript block
        if(App.quizName){
            $.ajax(App.basePath + "/json/" + App.quizSlug + "/" + quiz_ts, {
                dataType : 'json',
                success: success,
                error: error
            });
        }else
            this.onQuizNotReady();
    };

    this.nodes_index = ['choices', 'helps', 'question', 'urls', 'files', 'popup', 'related'];

    /**
     * Find a specified id in the quiz and return
     * @params id : searched id; node (optional): a branch to search in, default to first level of the loaded quiz
     * @return : a branch or a node
     */
    this.findNode = function(id, node, toDelete){
        this.found = false;                 // used to stop the recursion
        this.indexNode = -1;
        if(!node) {
            node = this.quiz;         // by default start at the entrance of the quiz
            this.lvl = 0;            // debug tool
        }

        this.lvl++;                  // debug tool
        this.findNodeRec(id, node, toDelete);

        return this.found;
    };

    // Recursive function to find a specified id in a specified branch
    this.findNodeRec = function(id, node, toDelete)
    {
        //Logger.log("-->" + (toDelete ? "Deleting : " : "Searching : ") + id);
        if(id){
            if(node["id"] == id) {
                this.found = node;
                this.indexNode = q;
                return this.found;
            }

            var n = this.nodes_index.length;
            while(n)
            {
                n--;
                
                if (node[this.nodes_index[n]])
                {
                    //Logger.log("Check : " + this.nodes_index[n]);
                    if( Object.prototype.toString.call(node[this.nodes_index[n]]) == '[object Object]')
                    {
                        //Logger.log("Object Mode");
                        if (node[this.nodes_index[n]]["id"] == id)
                        {
                            this.found = node[this.nodes_index[n]];
                            //Logger.log("Spotted : " + node[this.nodes_index[n]]["id"]);
                            if(toDelete)
                            {
                                delete node[this.nodes_index[n]];
                                this.found = "deleted";
                            }
                            break;
                        }
                    }else{
                        //Logger.log("Array Mode : " + node[this.nodes_index[n]].length + " elements");
                        var q = node[this.nodes_index[n]].length;
                        while(q)
                        {
                            q--;
                            //Logger.log("compare with : " + node[this.nodes_index[n]][q]["id"]);
                            if (node[this.nodes_index[n]][q]["id"] == id )
                            {
                                this.found = node[this.nodes_index[n]][q];
                                //Logger.log("Spotted : " + node[this.nodes_index[n]][q]["id"]);
                                if(toDelete)
                                {
                                    //Logger.log("will delete : ");
                                    //Logger.log(node[this.nodes_index[n]][q]);
                                    node[this.nodes_index[n]].splice(q, 1);
                                    this.found = "deleted";
                                }
                                break; 
                            }
                            else
                            {
                                if (!this.found) this.findNode(id, node[this.nodes_index[n]][q], toDelete); // recurse
                            }
                        }
                    }
                }
                if(this.found) break;
            }
        }
        
        return this.found;
    };

    this.findHelp = function(helpMetaId, node, rec) {
        if(helpMetaId) {
            // first iteration
            if(!rec) {
                this.found = false;
                if(!node) node = this.quiz;
                this.findHelp(helpMetaId, node, true);
            }

            // if help is found, stop the function and return help
            if(this.found) {
                return this.found;
            }

            // loops recursively to find help node
            var are_children_helps = node.helps && node.helps.length ? true : false;
            var q;
            if(!are_children_helps) {
                q = node.choices.length;
                while(q) {
                    q--;

                    this.findHelp(helpMetaId, node.choices[q], true);
                }
            } else {
                q = node.helps.length;
                while(q) {
                    q--;
                    if(parseInt(node.helps[q]['meta_id'], 10) == helpMetaId) {
                        this.found = node.helps[q];
                    }
                }
            }
        }
    };

    /**
     * returns the type of the children (is there another branch or are these helps ?)
     * @param node_id
     * @return Object( "nodes" : [], "is_choice" : boolean )
     * @deprecated
     */
    this.getChildren = function(node_id) {
        var nodes = this.findNode(node_id, this.quiz);
        try {
            var is_choice = nodes.choices[0]['type'] == 'choice';
        } catch(err) {
            //Logger.log("caught error: " + err);
        }

        return {
            "nodes": nodes,
            "is_choice": is_choice
        };
    };

    /**
     * Returns direct parent
     * @param node
     * @return node
     */
    this.getParent = function(node) {
        var parent = null;

        if(node.breadcrumbs && node.breadcrumbs.length) {
            parent = this.findNode(node.breadcrumbs[node.breadcrumbs.length-1]);
        }

        return parent;
    };

    /**
     * constructor
     * @param none
     * @return nothing
     */
    this.init = function()
    {
        this.fetchQuiz();
    };
    //this.init(); -> quizParser.init() -> this is the Initiator of all javascript ( triggered in common/layout.html.twig )

    /*Callbacks*/
    this.onQuizReady = function(quiz) {
        Logger.log("onQuizReady");
    };

    this.onQuizNotReady = function() {
        Logger.log("onQuizNotReady");
    };
};

var quizParser = new QuizParser(); //

/**
 * Node types
 * @type {{HELP: string, CHOICE: string, QUESTION: string, URL: string, FILE: string, POPUP: string, RELATED: string}}
 */
// var NODE_TYPE = {
    // HELP: "help",
    // CHOICE: "choice",
    // QUESTION: "question", // ?
    // URL: "url",
    // FILE: "file",
    // POPUP: "popup",
    // RELATED: "related"
// };