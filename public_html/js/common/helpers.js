/**
 * Created by PAPERPIXEL
 * www.paperpixel.net
 **/



/**
 * Returns length of array + 1; usefull when a "add" colums is needed
 * @params : array
 * @return : length of array + 1
 */
Handlebars.registerHelper('length', function(options) {
    return options.length + 1;
});

/**
 * Takes two args and returns true if they're equal
 * @args : expression
 * @return : boolean
 */
Handlebars.registerHelper('equal', function(lvalue, rvalue, options) {
    if (arguments.length < 3)
        throw new Error("Handlebars Helper equal needs 2 parameters");
    if( lvalue!=rvalue ) {
        return options.inverse(this);
    } else {
        return options.fn(this);
    }
});


/**
 * Logs any object passed as argument
 */
Handlebars.registerHelper('dump', function(object) {
    console.log(object);
});

/**
 *
 */
Handlebars.registerHelper('trim', function(options) {
    /* TODO strip HTML and finetune (trims at next word end) */
    var name = options.hash ? options.hash.name : false;
    var shortname = options.hash ? options.hash.shortname : false;
    var maxChar = options.hash ? options.hash.maxChar : false;

    if(shortname && shortname.length > 0) {
        return shortname;
    } else {
        if(!maxChar) {
            maxChar = 40;
        }
        if(name) {
// /           name = jQuery(''+name).text(); // strip html tags
            if(maxChar < name.length) {
                return name.substring(0, maxChar) + "...";
            } else {
                return name;
            }
        }
        return name;
    }
});

Handlebars.registerHelper('getColumns', function (options) {
    var nodeQty = options.hash['nodeQty'];
    var treshold = options.hash['treshold'] ? options.hash['treshold'] : 3;
    var is_plus_one = options.hash['is_plus_one'] ? options.hash['is_plus_one'] : false;
    if(nodeQty) {
        nodeQty = is_plus_one ? nodeQty + 1 : nodeQty;
        if(nodeQty > treshold)
            return treshold;
        else
            return nodeQty;
    } else {
        return false;
    }
});

/**
 * Returns true if node is active // works only in backend
 * @params : id : node id; context
 * @return : "active" or ""
 */
Handlebars.registerHelper('is_active', function(id) {
    var i = Backend.active_nodes.id.length;
    var active = false;
    while(i)
    {
        i--;
        if (Backend.active_nodes.id[i] === id)
        {
            active = true;
            break;
        }
    }
    return active ? "active" : "";
});

/**
 * Compute level index of a node
 * @params : breadcrumbs
 * @return : level index
 */
Handlebars.registerHelper('lvl', function(breadcrumbs){
    return breadcrumbs.length;
});

Handlebars.registerHelper('lvldown', function(breadcrumbs){
    return breadcrumbs.length + 1;
});

/**
 * renders a template in another template
 * (not using built-in partial syntax since it's not precompiled
 * and that could lead to some issues)
 * /!\ All templates must be registered in Handlebars.templates !
 */
Handlebars.registerHelper('partial', function(templateName, context){
    return new Handlebars.SafeString(Handlebars.templates[templateName](this));
});

/**
 * Returns the JSON object stringified to use in html markup
 */
Handlebars.registerHelper('stringify', function (object) {
    if(JSON) {
        return JSON.stringify(object);
    } else {
        throw new Error('JSON object not found');
    }
});

Handlebars.registerHelper('questionCheck', function(choices, breadcrumbs, options)
{
    if(choices.length || !breadcrumbs.length)
    {
        return options.fn(this);
    }else
    {
        return options.inverse(this);
    }
});

Handlebars.registerHelper('urllocal', function(text, options)
{
    if(text.search(/%basePath%/) !== -1)
    {
        return App.basePath + '/' + App.quizSlug + '?n=' + text.substr(10);
    }else
    {
        return text;
    }
});

Handlebars.registerHelper('urllocalName', function(text, options)
{
    if(text.search(/%basePath%/) !== -1)
    {
        return "interne -> " + quizParser.findNode(parseInt(text.substr(10),10)).name;
    }else
    {
        return text;
    }
});

Handlebars.registerHelper('isUrllocal', function(text, options)
{
    var base = text.search(/%basePath%/);
    // console.log(base);
    if(base != -1)
    {
        return options.fn(this);
    }else
    {
        return options.inverse(this);
    }
});

Handlebars.registerHelper('isNotUrllocal', function(text, options)
{
    var base = text.search(/%basePath%/);
    // console.log(base);
    if(base == -1)
    {
        return options.fn(this);
    }else
    {
        return options.inverse(this);
    }
});

Handlebars.registerHelper('strip_tags', function(text) {
    return text.stripTags();
});

Handlebars.registerHelper('quizUrl', function(quizSlug) {
    if(App && App['quizUrlBase']) {
        return App['quizUrlBase'] + quizSlug;
    } else {
        throw new Error('App or App.quizUrlBase not defined');
    }
});

Handlebars.registerHelper('quizAdminUrl', function(quizSlug) {
    if(App && App['adminQuizUrlBase']) {
        return App['adminQuizUrlBase'] + 'quiz/' + quizSlug;
    } else {
        throw new Error('App or App.adminQuizUrlBase not defined');
    }
});

Handlebars.registerHelper('is_admin', function(options) {
    if(App && (App['is_admin'] !== undefined)) {
        if(App['is_admin']) {
            return options.fn(this);
        } else {
            return options.inverse(this);
        }
    } else {
        throw new Error('App or App.is_admin is not defined');
    }
});


Handlebars.registerHelper('formatDate', function(options) {
    var dateTime = options.hash.date;
    var timeAgo = options.hash.timeAgo;

    if(timeAgo) {
        return moment(dateTime).fromNow();
    } else {
        return moment(dateTime).format('D MMM à H:m');
//        var date = new Date(dateTime);
//        return 'le ' + date.toLocaleDateString() + ' à ' + date.getHours() + ':' + date.getMinutes();
    }
});


Handlebars.registerHelper('getDomain', function() {
    return window.location.host;
});

Handlebars.registerHelper('breadcrumbPath', function(id) {
    var breadcrumb = quizParser.getShortnameBreadcrumbs(id, 20);
    if(breadcrumb) {
        var helpName = breadcrumb.pop();
        var first = breadcrumb.length ? '<span class="help-breadcrumb">'+breadcrumb.join(' > ')+' > </span>' : '';
        var second = '<span class="help-name">'+helpName+'</span>';
        return first + second;
        //return quizParser.getShortnameBreadcrumbs(id).join(' > ');
    }
});

Handlebars.registerHelper('getStatsFromNode', function(nodeId, options) {
    if(Statistics.json) {
        var stats = Statistics.getStatsByNode(nodeId);

        if(stats) {
            return options.fn(stats);
        }
        Logger.log('Couldn\'t get statistics data for node id ' + nodeId);
        return null;
    }
    Logger.log('No json from piwik');
    return null;
});