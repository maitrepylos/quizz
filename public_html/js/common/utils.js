/**
 * Created by PAPERPIXEL
 * www.paperpixel.net
 **/


var Utils = {
    isIframe: function() {
        return !(window.self === window.top);
    },
    isTouchDevice: function() {
        return 'ontouchstart' in document.documentElement;
    }
};


/**
 * Logs only if in debug env
 * @type {{log: Function}}
 */
var Logger = {
    log: function(param) {
        if(App && App.debug) {
            console.log(param);
        }
    },
    error: function(param) {
        if(App && App.debug) {
            throw new Error(param);
        }
    }
};




/**
 * Adds and removes a loading state to a button
 *
 */
addLoadingState = function($elmt, text) {
//    if(App && !App.debug) {
        text = !text ? 'Enregistrement en cours...' : text;

        $elmt.button('loading');
        $elmt.html(text);
//    }
};

removeLoadingState = function($elmt, text) {
//    if(App && !App.debug) {
        if(text) {
            $elmt.removeAttr('disabled').removeClass('disabled').html(text);
        } else {
            $elmt.button('reset');
        }
//    }
};

(function($) {
    $.fn.focusLoop = function() {
        return this.each(function() {
            // TODO handle shift tab
            var $form = $(this);
            var $firstElmt = $form.find(':input').filter(':first');
            var $lastElmt = $form.find('button,input[type=submit]').filter(':last');

            $lastElmt.off('blur').on('blur', function(event) {
                    $firstElmt.focus();
            });
        });
    };
}(jQuery));


/**
 * Displays an error screen with a reload button
 * @param message the string passed
 * @param [showButton]
 */

(function($) {
    $.bsod = function(message) {
        // variable declarations
        var bsodClass = 'bsod-container',
            backdropClass = 'bsod-backdrop',
            messageClass = 'bsod-message',
            messageParagraphClass = 'bsod-message-text',
            buttonParagraphClass = 'bsod-message-button',
            buttonClass = 'bsod-button';

        // error handling
        if(!message) throw Error('$.bsod: you should define a message');

        // hide scroll
        document.documentElement.style.overflow = 'hidden';  // firefox, chrome
        document.body.scroll = "no"; // ie only

        // DOM creation
        var $bsodContainer = $('.'+bsodClass);
        if(!$bsodContainer.length) {
            // bsod element
            $bsodContainer = $('<div/>').addClass(bsodClass).hide();

            // backdrop
            var $backdrop = $('<div/>').addClass(backdropClass).css({
                'position': 'fixed',
                'top': 0, 'right': 0, 'bottom': 0, 'left': 0,
                'backgroundColor': '#000000',
                opacity: 0.8,
                'zIndex': 93000
            });

            var $messageContainer = $('<div/>').addClass(messageClass).css({
                position: 'fixed',
                padding: '20px 40px',
                backgroundColor: '#fff',
                top: '50%',
                left: '50%',
                textAlign: 'center',
                zIndex: 93001,
                width: '400px',
                height: '100px',
                marginLeft: '-200px',
                marginTop: '-50px'
            });

            var $message = $('<p/>').addClass(messageParagraphClass).html(message);

            var $buttonParagraph = $('<p/>').addClass(buttonParagraphClass);
            var $button = $('<a/>').addClass('btn btn-danger').html('Recharger la page');

            // add to DOM
            $buttonParagraph.append($button);
            $messageContainer.append($message).append($button);
            $bsodContainer.append($backdrop).append($messageContainer);
            $('body').append($bsodContainer);

            // event on button
            $button.off('click').on('click', function(event) {
                window.location.reload();
            });
        }

        //backdrop display
        $bsodContainer.show();
    };
})(jQuery);

/**
 * Displays an error screen with a reload button
 * @param message the string passed
 * @param [showButton]
 */

(function($) {
    $.bsod = function(message) {
        // variable declarations
        var bsodClass = 'bsod-container',
            backdropClass = 'bsod-backdrop',
            messageClass = 'bsod-message',
            messageParagraphClass = 'bsod-message-text',
            buttonParagraphClass = 'bsod-message-button',
            buttonClass = 'bsod-button';

        // error handling
        if(!message) throw Error('$.bsod: you should define a message');

        // hide scroll
        document.documentElement.style.overflow = 'hidden';  // firefox, chrome
        document.body.scroll = "no"; // ie only

        // DOM creation
        var $bsodContainer = $('.'+bsodClass);
        if(!$bsodContainer.length) {
            // bsod element
            $bsodContainer = $('<div/>').addClass(bsodClass).hide();

            // backdrop
            var $backdrop = $('<div/>').addClass(backdropClass).css({
                'position': 'fixed',
                'top': 0, 'right': 0, 'bottom': 0, 'left': 0,
                'backgroundColor': '#000000',
                opacity: 0.8,
                'zIndex': 93000
            });

            var $messageContainer = $('<div/>').addClass(messageClass).css({
                position: 'fixed',
                padding: '20px 40px',
                backgroundColor: '#fff',
                top: '50%',
                left: '50%',
                textAlign: 'center',
                zIndex: 93001,
                width: '400px',
                height: '100px',
                marginLeft: '-200px',
                marginTop: '-50px'
            });

            var $message = $('<p/>').addClass(messageParagraphClass).html(message);

            var $buttonParagraph = $('<p/>').addClass(buttonParagraphClass);
            var $button = $('<a/>').addClass('btn btn-danger').html('Recharger la page');

            // add to DOM
            $buttonParagraph.append($button);
            $messageContainer.append($message).append($button);
            $bsodContainer.append($backdrop).append($messageContainer);
            $('body').append($bsodContainer);

            // event on button
            $button.off('click').on('click', function(event) {
                window.location.reload();
            });
        }

        //backdrop display
        $bsodContainer.show();
    };
})(jQuery);


/**
 * jQuery truncate plugin
 * Truncates and adds a read more link. When clicked, the whole text is shown.
 * Text must be html-markup free !
 */
(function($) {
    $.fn.truncate = function(options) {
        var defaults = {
            textLength: 150,
            moreText: 'More',
            lessText: 'Less',
            moreClass: 'j-trunctate-more-link',
            lessClass: 'j-trunctate-less-link',
            truncateString: '[...]',
            truncateStringClass: 'j-truncate-truncate-string',
            truncatedContainerClass: 'j-truncate-container',
            transitionSpeed: 300
        };
        options = $.fn.extend(defaults, options);

        return this.each(function () {
            var $elmt = $(this),
                fullText = $elmt.html();

            if(fullText.length > options.textLength) {
                var isShown = false,
                    truncatedText = fullText.substr(0, options.textLength),
                    restText = fullText.substr(options.textLength, fullText.length-1),
                    $truncatedContainer = $('<span>').addClass(options.truncatedContainerClass),
                    $truncateString = $('<span>').addClass(options.truncateStringClass).html(options.truncateString).css({marginRight: '5px'}),
                    $moreLink = $('<a>').attr('href', '#').addClass(options.moreClass).html(options.moreText),
                    $lessLink = $('<a>').attr('href', '#').addClass(options.lessClass).html(options.lessText);

                $lessLink.hide();
//                $truncatedContainer.hide();
                $elmt
                    .html(truncatedText)
                    .append($truncateString)
                    .append($truncatedContainer)
                    .append($moreLink)
                    .append($lessLink);

                $elmt.on('click', 'a.'+options.moreClass, function(event) {
                    $truncateString.hide();
                    $truncatedContainer.html(restText)/*.show(options.transitionSpeed)*/;
                    $lessLink.show();
                    $moreLink.hide();

                    event.preventDefault();
                });
                $elmt.on('click', 'a.'+options.lessClass, function(event) {
//                    $truncatedContainer.hide(options.transitionSpeed, function() {
//                        $(this).html('');
//                    });
                    $truncateString.show();
                    $truncatedContainer.html('');
                    $moreLink.show();
                    $lessLink.hide();

                    event.preventDefault();
                });
            }
        });
    };
})(jQuery);


/* Tooling */
Array.prototype.move = function (old_index, new_index) {
    if (new_index >= this.length) {
        var k = new_index - this.length;
        while ((k--) + 1) {
            this.push(undefined);
        }
    }
    this.splice(new_index, 0, this.splice(old_index, 1)[0]);
    return this; // for testing purposes
};

/**
 * Return a new JSON object of the old string.
 * Turns:
 *                 file.js?a=1&amp;b.c=3.0&b.d=four&a_false_value=false&a_null_value=null
 * Into:
 *                 {"a":1,"b":{"c":3,"d":"four"},"a_false_value":false,"a_null_value":null}
 * @version 1.1.0
 * @date July 16, 2010
 * @since 1.0.0, June 30, 2010
 * @package jquery-sparkle {@link http://balupton.com/projects/jquery-sparkle}
 * @author Benjamin "balupton" Lupton {@link http://balupton.com}
 * @copyright (c) 2009-2010 Benjamin Arthur Lupton {@link http://balupton.com}
 * @license MIT License {@link http://creativecommons.org/licenses/MIT/}
 */
String.prototype.queryStringToJSON = String.prototype.queryStringToJSON || function ( )
{        // Turns a params string or url into an array of params
    // Prepare
    var params = String(this);
    // Remove url if need be
    params = params.substring(params.indexOf('?')+1);
    // params = params.substring(params.indexOf('#')+1);
    // Change + to %20, the %20 is fixed up later with the decode
    params = params.replace(/\+/g, '%20');
    // Do we have JSON string
    if ( params.substring(0,1) === '{' && params.substring(params.length-1) === '}' )
    {        // We have a JSON string
        return eval(decodeURIComponent(params));
    }
    // We have a params string
    params = params.split(/\&(amp\;)?/);
    var json = {};
    // We have params
    for ( var i = 0, n = params.length; i < n; ++i )
    {
        // Adjust
        var param = params[i] || null;
        if ( param === null ) { continue; }
        param = param.split('=');
        if ( param === null ) { continue; }
        // ^ We now have "var=blah" into ["var","blah"]

        // Get
        var key = param[0] || null;
        if ( key === null ) { continue; }
        if ( typeof param[1] === 'undefined' ) { continue; }
        var value = param[1];
        // ^ We now have the parts

        // Fix
        key = decodeURIComponent(key);
        value = decodeURIComponent(value);
        try {
            // value can be converted
            value = eval(value);
        } catch ( e ) {
            // value is a normal string
        }

        // Set
        // window.console.log({'key':key,'value':value}, split);
        var keys = key.split('.');
        if ( keys.length === 1 )
        {        // Simple
            json[key] = value;
        }
        else
        {        // Advanced (Recreating an object)
            var path = '',
                cmd = '';
            // Ensure Path Exists
            $.each(keys,function(ii,key){
                path += '["'+key.replace(/"/g,'\\"')+'"]';
                jsonCLOSUREGLOBAL = json; // we have made this a global as closure compiler struggles with evals
                cmd = 'if ( typeof jsonCLOSUREGLOBAL'+path+' === "undefined" ) jsonCLOSUREGLOBAL'+path+' = {}';
                eval(cmd);
                json = jsonCLOSUREGLOBAL;
                delete jsonCLOSUREGLOBAL;
            });
            // Apply Value
            jsonCLOSUREGLOBAL = json; // we have made this a global as closure compiler struggles with evals
            valueCLOSUREGLOBAL = value; // we have made this a global as closure compiler struggles with evals
            cmd = 'jsonCLOSUREGLOBAL'+path+' = valueCLOSUREGLOBAL';
            eval(cmd);
            json = jsonCLOSUREGLOBAL;
            delete jsonCLOSUREGLOBAL;
            delete valueCLOSUREGLOBAL;
        }
        // ^ We now have the parts added to your JSON object
    }
    return json;
}; 

/**
 * Strips html tags from string given
 * @link: http://phpjs.org/functions
 */
String.prototype.stripTags = String.prototype.stripTags || function(allowed) {
    var input = String(this);
    allowed = (((allowed || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join('');
    var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
        commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return input.replace(commentsAndPhpTags, '').replace(tags, function ($0, $1) {
        return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
    });
};