<?php

/* @Widgetize/iframe_empty.twig */
class __TwigTemplate_120a4c964c40a858828e1bd511534884c2ed9416f05465399af6d125806bcf44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["content"]) ? $context["content"] : $this->getContext($context, "content"));
    }

    public function getTemplateName()
    {
        return "@Widgetize/iframe_empty.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
