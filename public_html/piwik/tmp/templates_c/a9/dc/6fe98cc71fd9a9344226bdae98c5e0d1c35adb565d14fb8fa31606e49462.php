<?php

/* @CoreHome/_indexContent.twig */
class __TwigTemplate_a9dc6fe98cc71fd9a9344226bdae98c5e0d1c35adb565d14fb8fa31606e49462 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $context["ajax"] = $this->env->loadTemplate("ajaxMacros.twig");
        // line 2
        echo "<div class=\"pageWrap\">
    ";
        // line 3
        $this->env->loadTemplate("@CoreHome/_notifications.twig")->display($context);
        // line 4
        echo "    <div class=\"top_controls\">
        ";
        // line 5
        $this->env->loadTemplate("@CoreHome/_periodSelect.twig")->display($context);
        // line 6
        echo "        ";
        echo call_user_func_array($this->env->getFunction('postEvent')->getCallable(), array("Template.nextToCalendar"));
        echo "
        ";
        // line 7
        $this->env->loadTemplate("@Dashboard/_dashboardSettings.twig")->display($context);
        // line 8
        echo "        ";
        $this->env->loadTemplate("@CoreHome/_headerMessage.twig")->display($context);
        // line 9
        echo "        ";
        echo $context["ajax"]->getrequestErrorDiv();
        echo "
    </div>

    ";
        // line 12
        echo $context["ajax"]->getloadingDiv();
        echo "

    <div id=\"content\" class=\"home\">
        ";
        // line 15
        if ((isset($context["content"]) ? $context["content"] : $this->getContext($context, "content"))) {
            echo twig_escape_filter($this->env, (isset($context["content"]) ? $context["content"] : $this->getContext($context, "content")), "html", null, true);
        }
        // line 16
        echo "    </div>
    <div class=\"clear\"></div>
</div>

<br/><br/>";
    }

    public function getTemplateName()
    {
        return "@CoreHome/_indexContent.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 16,  77 => 21,  23 => 3,  165 => 64,  153 => 62,  151 => 61,  136 => 52,  130 => 50,  128 => 49,  113 => 44,  106 => 43,  102 => 42,  98 => 40,  92 => 38,  79 => 33,  71 => 32,  62 => 29,  44 => 24,  28 => 4,  35 => 6,  26 => 4,  90 => 37,  82 => 22,  69 => 18,  39 => 8,  63 => 17,  49 => 27,  33 => 6,  31 => 6,  59 => 8,  52 => 12,  48 => 12,  27 => 4,  24 => 3,  21 => 2,  19 => 1,  134 => 43,  131 => 42,  127 => 35,  124 => 47,  121 => 33,  114 => 20,  112 => 19,  107 => 16,  105 => 15,  100 => 13,  87 => 9,  84 => 8,  81 => 7,  74 => 20,  72 => 19,  68 => 44,  66 => 13,  60 => 12,  55 => 36,  53 => 33,  50 => 9,  41 => 9,  38 => 8,  36 => 7,  32 => 5,  30 => 19,  22 => 2,  56 => 13,  54 => 15,  51 => 11,  47 => 10,  45 => 9,  42 => 9,  40 => 5,  37 => 10,  34 => 3,  29 => 5,);
    }
}
