<?php

/* @Live/ajaxTotalVisitors.twig */
class __TwigTemplate_2adfbc32e8957a56cbde0a078368a600685e60fffc9e2f702f94aa8359df0a6b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->loadTemplate("@Live/_totalVisitors.twig")->display($context);
    }

    public function getTemplateName()
    {
        return "@Live/ajaxTotalVisitors.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
