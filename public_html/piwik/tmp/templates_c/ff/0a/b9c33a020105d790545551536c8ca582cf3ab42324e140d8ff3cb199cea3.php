<?php

/* @VisitsSummary/getSparklines.twig */
class __TwigTemplate_ff0ab9c33a020105d790545551536c8ca582cf3ab42324e140d8ff3cb199cea3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->loadTemplate("@VisitsSummary/_sparklines.twig")->display($context);
    }

    public function getTemplateName()
    {
        return "@VisitsSummary/getSparklines.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
