<?php

/* @Widgetize/iframe_empty.twig */
class __TwigTemplate_4899fbcfc594333eefd0e22e69c9b865112e9d47e5c5714dcbeb0c04d40cd271 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo (isset($context["content"]) ? $context["content"] : $this->getContext($context, "content"));
    }

    public function getTemplateName()
    {
        return "@Widgetize/iframe_empty.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
