<?php

/* @Annotations/getAnnotationManager.twig */
class __TwigTemplate_00ad640809199b8f10b563f29d1c6e38c9bedafd9c23a8e12d8e017ff0fee2e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"annotation-manager\"
     ";
        // line 2
        if (((isset($context["startDate"]) ? $context["startDate"] : $this->getContext($context, "startDate")) != (isset($context["endDate"]) ? $context["endDate"] : $this->getContext($context, "endDate")))) {
            echo "data-date=\"";
            echo twig_escape_filter($this->env, (isset($context["startDate"]) ? $context["startDate"] : $this->getContext($context, "startDate")), "html", null, true);
            echo ",";
            echo twig_escape_filter($this->env, (isset($context["endDate"]) ? $context["endDate"] : $this->getContext($context, "endDate")), "html", null, true);
            echo "\" data-period=\"range\"
     ";
        } else {
            // line 3
            echo "data-date=\"";
            echo twig_escape_filter($this->env, (isset($context["startDate"]) ? $context["startDate"] : $this->getContext($context, "startDate")), "html", null, true);
            echo "\" data-period=\"";
            echo twig_escape_filter($this->env, (isset($context["period"]) ? $context["period"] : $this->getContext($context, "period")), "html", null, true);
            echo "\"
     ";
        }
        // line 4
        echo ">

    <div class=\"annotations-header\">
        <span>";
        // line 7
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Annotations_Annotations")), "html", null, true);
        echo "</span>
    </div>

    <div class=\"annotation-list-range\">";
        // line 10
        echo twig_escape_filter($this->env, (isset($context["startDatePretty"]) ? $context["startDatePretty"] : $this->getContext($context, "startDatePretty")), "html", null, true);
        if (((isset($context["startDate"]) ? $context["startDate"] : $this->getContext($context, "startDate")) != (isset($context["endDate"]) ? $context["endDate"] : $this->getContext($context, "endDate")))) {
            echo " &mdash; ";
            echo twig_escape_filter($this->env, (isset($context["endDatePretty"]) ? $context["endDatePretty"] : $this->getContext($context, "endDatePretty")), "html", null, true);
        }
        echo "</div>

    <div class=\"annotation-list\">
        ";
        // line 13
        $this->env->loadTemplate("@Annotations/_annotationList.twig")->display($context);
        // line 14
        echo "
        <span class=\"loadingPiwik\" style=\"display:none;\"><img src=\"plugins/Zeitgeist/images/loading-blue.gif\"/>";
        // line 15
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_Loading")), "html", null, true);
        echo "</span>

    </div>

    <div class=\"annotation-controls\">
        ";
        // line 20
        if ((isset($context["canUserAddNotes"]) ? $context["canUserAddNotes"] : $this->getContext($context, "canUserAddNotes"))) {
            // line 21
            echo "            <a href=\"#\" class=\"add-annotation\" title=\"";
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Annotations_CreateNewAnnotation")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Annotations_CreateNewAnnotation")), "html", null, true);
            echo "</a>
        ";
        } elseif (((isset($context["userLogin"]) ? $context["userLogin"] : $this->getContext($context, "userLogin")) == "anonymous")) {
            // line 23
            echo "            <a href=\"index.php?module=Login\">";
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Annotations_LoginToAnnotate")), "html", null, true);
            echo "</a>
        ";
        }
        // line 25
        echo "    </div>

</div>
";
    }

    public function getTemplateName()
    {
        return "@Annotations/getAnnotationManager.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 25,  83 => 23,  75 => 21,  73 => 20,  65 => 15,  62 => 14,  60 => 13,  50 => 10,  44 => 7,  39 => 4,  31 => 3,  22 => 2,  19 => 1,);
    }
}
