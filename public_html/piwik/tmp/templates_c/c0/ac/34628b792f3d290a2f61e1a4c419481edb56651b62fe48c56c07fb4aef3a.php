<?php

/* _piwikTag.twig */
class __TwigTemplate_c0ac34628b792f3d290a2f61e1a4c419481edb56651b62fe48c56c07fb4aef3a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "
";
        // line 3
        if ((((isset($context["piwikUrl"]) ? $context["piwikUrl"] : $this->getContext($context, "piwikUrl")) == "http://demo.piwik.org/") || (isset($context["debugTrackVisitsInsidePiwikUI"]) ? $context["debugTrackVisitsInsidePiwikUI"] : $this->getContext($context, "debugTrackVisitsInsidePiwikUI")))) {
            // line 4
            echo "    <div class=\"clear\"></div>
    <!-- Piwik -->
    <script type=\"text/javascript\">
    var _paq = _paq || [];
    _paq.push(['setTrackerUrl', 'piwik.php']);
    _paq.push(['setSiteId', 1]);
    _paq.push(['setCookieDomain', '*.piwik.org']);
    // set the domain the visitor landed on, in the Custom Variable
    _paq.push([function () {
    if (!this.getCustomVariable(1)) {
        this.setCustomVariable(1, \"Domain landed\", document.domain);
    }
    }]);
    // Set the selected Piwik language in a custom var
    _paq.push(['setCustomVariable', 2, \"Demo language\", piwik.languageName]);
    _paq.push(['setDocumentTitle', document.domain + \"/\" + document.title]);
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);

    (function() {
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
    g.defer=true; g.async=true; g.src='js/piwik.js'; s.parentNode.insertBefore(g,s);
    })();
    </script>
    <!-- End Piwik Code -->
";
        }
    }

    public function getTemplateName()
    {
        return "_piwikTag.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 52,  120 => 49,  111 => 44,  104 => 33,  101 => 32,  95 => 30,  78 => 24,  64 => 18,  58 => 16,  77 => 21,  23 => 4,  165 => 64,  153 => 62,  151 => 61,  136 => 52,  130 => 50,  128 => 49,  113 => 44,  106 => 43,  102 => 42,  98 => 40,  92 => 38,  79 => 33,  71 => 32,  62 => 29,  44 => 9,  28 => 4,  35 => 6,  26 => 4,  90 => 28,  82 => 22,  69 => 18,  39 => 8,  63 => 17,  49 => 27,  33 => 6,  31 => 6,  59 => 15,  52 => 12,  48 => 12,  27 => 5,  24 => 4,  21 => 3,  19 => 2,  134 => 43,  131 => 42,  127 => 35,  124 => 47,  121 => 33,  114 => 45,  112 => 19,  107 => 42,  105 => 15,  100 => 13,  87 => 9,  84 => 26,  81 => 25,  74 => 20,  72 => 21,  68 => 44,  66 => 19,  60 => 12,  55 => 36,  53 => 13,  50 => 9,  41 => 9,  38 => 8,  36 => 7,  32 => 5,  30 => 6,  22 => 3,  56 => 13,  54 => 15,  51 => 11,  47 => 11,  45 => 9,  42 => 9,  40 => 5,  37 => 10,  34 => 3,  29 => 4,);
    }
}
