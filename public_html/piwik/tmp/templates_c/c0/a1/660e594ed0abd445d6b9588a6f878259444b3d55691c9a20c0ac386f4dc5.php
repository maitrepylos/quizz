<?php

/* _jsCssIncludes.twig */
class __TwigTemplate_c0a1660e594ed0abd445d6b9588a6f878259444b3d55691c9a20c0ac386f4dc5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        echo "    ";
        echo call_user_func_array($this->env->getFunction('includeAssets')->getCallable(), array(array("type" => "css")));
        echo "
    ";
        // line 3
        echo call_user_func_array($this->env->getFunction('includeAssets')->getCallable(), array(array("type" => "js")));
        echo "
";
        // line 5
        if ((call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_LayoutDirection")) == "rtl")) {
            // line 6
            echo "    <link rel=\"stylesheet\" type=\"text/css\" href=\"plugins/Zeitgeist/stylesheets/rtl.css\"/>
";
        }
    }

    public function getTemplateName()
    {
        return "_jsCssIncludes.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 6,  28 => 5,  132 => 36,  127 => 34,  124 => 33,  121 => 32,  113 => 30,  108 => 28,  104 => 27,  100 => 26,  96 => 25,  92 => 24,  88 => 23,  84 => 22,  79 => 21,  77 => 20,  72 => 19,  70 => 15,  62 => 13,  54 => 11,  46 => 9,  38 => 7,  31 => 5,  27 => 4,  23 => 3,  39 => 14,  25 => 6,  66 => 18,  64 => 17,  60 => 16,  58 => 15,  53 => 12,  42 => 15,  37 => 13,  33 => 7,  29 => 5,  26 => 4,  24 => 3,  21 => 2,  19 => 2,);
    }
}
