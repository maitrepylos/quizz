<?php

/* @VisitFrequency/getSparklines.twig */
class __TwigTemplate_b45949de6639fdf00175679936b51210b555678c680d12eb216469f6e44b4e5b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->loadTemplate("@VisitFrequency/_sparklines.twig")->display($context);
    }

    public function getTemplateName()
    {
        return "@VisitFrequency/getSparklines.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
