<?php

/* @Live/ajaxTotalVisitors.twig */
class __TwigTemplate_046676097efaa26d13a8b05a94eed25f65fc93340f793abd9961404bf6746dc1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->loadTemplate("@Live/_totalVisitors.twig")->display($context);
    }

    public function getTemplateName()
    {
        return "@Live/ajaxTotalVisitors.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
