<?php

/* _sparklineFooter.twig */
class __TwigTemplate_f876baea6be6df51d1e90a04dbea2ed0af0eac7ba139c40abc5772aab18c8fb7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\">
    \$(function () {
        initializeSparklines();
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "_sparklineFooter.twig";
    }

    public function getDebugInfo()
    {
        return array (  185 => 65,  183 => 64,  175 => 59,  171 => 58,  168 => 57,  162 => 54,  158 => 53,  154 => 52,  148 => 49,  144 => 48,  140 => 47,  137 => 46,  131 => 43,  127 => 42,  123 => 41,  120 => 40,  118 => 39,  113 => 37,  109 => 36,  105 => 35,  102 => 34,  96 => 31,  92 => 30,  89 => 29,  87 => 28,  78 => 23,  76 => 22,  72 => 21,  66 => 18,  62 => 17,  56 => 14,  52 => 13,  45 => 10,  43 => 9,  39 => 8,  35 => 6,  32 => 5,  27 => 4,  23 => 3,  19 => 1,);
    }
}
