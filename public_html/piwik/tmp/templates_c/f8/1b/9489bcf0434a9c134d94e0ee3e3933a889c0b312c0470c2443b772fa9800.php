<?php

/* @UsersManager/index.twig */
class __TwigTemplate_f81b9489bcf0434a9c134d94e0ee3e3933a889c0b312c0470c2443b772fa9800 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("admin.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<h2>";
        // line 5
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_ManageAccess")), "html", null, true);
        echo "</h2>
<div id=\"sites\" class=\"usersManager\">
    <section class=\"sites_selector_container\">
        <p>";
        // line 8
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_MainDescription")), "html", null, true);
        echo "</p>

        <div class=\"sites_selector_title\">";
        // line 10
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("SitesManager_Sites")), "html", null, true);
        echo ":</div>

        ";
        // line 12
        ob_start();
        // line 13
        echo "            <strong>";
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_ApplyToAllWebsites")), "html", null, true);
        echo "</strong>
        ";
        $context["applyAllSitesText"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 15
        echo "        ";
        $this->env->loadTemplate("@CoreHome/_siteSelect.twig")->display(array_merge($context, array("siteName" => (isset($context["defaultReportSiteName"]) ? $context["defaultReportSiteName"] : $this->getContext($context, "defaultReportSiteName")), "idSite" => (isset($context["idSiteSelected"]) ? $context["idSiteSelected"] : $this->getContext($context, "idSiteSelected")), "allSitesItemText" => (isset($context["applyAllSitesText"]) ? $context["applyAllSitesText"] : $this->getContext($context, "applyAllSitesText")), "allWebsitesLinkLocation" => "top", "siteSelectorId" => "usersManagerSiteSelect", "switchSiteOnSelect" => false)));
        // line 23
        echo "    </section>
</div>

";
        // line 26
        $context["ajax"] = $this->env->loadTemplate("ajaxMacros.twig");
        // line 27
        echo $context["ajax"]->geterrorDiv();
        echo "
";
        // line 28
        echo $context["ajax"]->getloadingDiv();
        echo "

<div class=\"entityContainer\" style=\"width:600px;\">
    ";
        // line 31
        if ((isset($context["anonymousHasViewAccess"]) ? $context["anonymousHasViewAccess"] : $this->getContext($context, "anonymousHasViewAccess"))) {
            // line 32
            echo "        <div style=\"display:inline-block;margin-top:10px;\" id=\"usersManagerAnonymousUserHasViewAccess\">
            ";
            // line 33
            echo call_user_func_array($this->env->getFilter('notification')->getCallable(), array(twig_join_filter(array(0 => call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_AnonymousUserHasViewAccess", "'anonymous'", "'view'")), 1 => call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_AnonymousUserHasViewAccess2"))), " "), array("placeAt" => "#usersManagerAnonymousUserHasViewAccess", "noclear" => true)));
            echo "
        </div>
    ";
        }
        // line 36
        echo "    <table class=\"entityTable dataTable\" id=\"access\" style=\"display:inline-table;width:500px;\">
        <thead>
        <tr>
            <th class='first'>";
        // line 39
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_User")), "html", null, true);
        echo "</th>
            <th>";
        // line 40
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_Alias")), "html", null, true);
        echo "</th>
            <th>";
        // line 41
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_PrivNone")), "html", null, true);
        echo "</th>
            <th>";
        // line 42
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_PrivView")), "html", null, true);
        echo "</th>
            <th>";
        // line 43
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_PrivAdmin")), "html", null, true);
        echo "</th>
        </tr>
        </thead>

        <tbody>
        ";
        // line 48
        $context["accesValid"] = ('' === $tmp = "<img src='plugins/UsersManager/images/ok.png' class='accessGranted' />") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 49
        echo "        ";
        $context["accesInvalid"] = ('' === $tmp = "<img src='plugins/UsersManager/images/no-access.png' class='updateAccess' />") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 50
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["usersAccessByWebsite"]) ? $context["usersAccessByWebsite"] : $this->getContext($context, "usersAccessByWebsite")));
        foreach ($context['_seq'] as $context["login"] => $context["access"]) {
            // line 51
            echo "            <tr>
                <td id='login'>";
            // line 52
            echo twig_escape_filter($this->env, (isset($context["login"]) ? $context["login"] : $this->getContext($context, "login")), "html", null, true);
            echo "</td>
                <td>";
            // line 53
            echo $this->getAttribute((isset($context["usersAliasByLogin"]) ? $context["usersAliasByLogin"] : $this->getContext($context, "usersAliasByLogin")), (isset($context["login"]) ? $context["login"] : $this->getContext($context, "login")), array(), "array");
            echo "</td>
                <td id='noaccess'>";
            // line 54
            if ((((isset($context["access"]) ? $context["access"] : $this->getContext($context, "access")) == "noaccess") && ((isset($context["idSiteSelected"]) ? $context["idSiteSelected"] : $this->getContext($context, "idSiteSelected")) != "all"))) {
                echo twig_escape_filter($this->env, (isset($context["accesValid"]) ? $context["accesValid"] : $this->getContext($context, "accesValid")), "html", null, true);
            } else {
                echo twig_escape_filter($this->env, (isset($context["accesInvalid"]) ? $context["accesInvalid"] : $this->getContext($context, "accesInvalid")), "html", null, true);
            }
            echo "&nbsp;</td>
                <td id='view'>";
            // line 55
            if ((((isset($context["access"]) ? $context["access"] : $this->getContext($context, "access")) == "view") && ((isset($context["idSiteSelected"]) ? $context["idSiteSelected"] : $this->getContext($context, "idSiteSelected")) != "all"))) {
                echo twig_escape_filter($this->env, (isset($context["accesValid"]) ? $context["accesValid"] : $this->getContext($context, "accesValid")), "html", null, true);
            } else {
                echo twig_escape_filter($this->env, (isset($context["accesInvalid"]) ? $context["accesInvalid"] : $this->getContext($context, "accesInvalid")), "html", null, true);
            }
            echo "&nbsp;</td>
                <td id='admin'>
                    ";
            // line 57
            if (((isset($context["login"]) ? $context["login"] : $this->getContext($context, "login")) == "anonymous")) {
                // line 58
                echo "                        N/A
                    ";
            } else {
                // line 60
                echo "                        ";
                if ((((isset($context["access"]) ? $context["access"] : $this->getContext($context, "access")) == "admin") && ((isset($context["idSiteSelected"]) ? $context["idSiteSelected"] : $this->getContext($context, "idSiteSelected")) != "all"))) {
                    echo twig_escape_filter($this->env, (isset($context["accesValid"]) ? $context["accesValid"] : $this->getContext($context, "accesValid")), "html", null, true);
                } else {
                    echo twig_escape_filter($this->env, (isset($context["accesInvalid"]) ? $context["accesInvalid"] : $this->getContext($context, "accesInvalid")), "html", null, true);
                }
                echo "&nbsp;
                    ";
            }
            // line 62
            echo "                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['login'], $context['access'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 65
        echo "        </tbody>
    </table>
    <div id=\"accessUpdated\" style=\"vertical-align:top;\"></div>
</div>

<div class=\"ui-confirm\" id=\"confirm\">
    <h2>";
        // line 71
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_ChangeAllConfirm", "<span id='login'></span>"));
        echo "</h2>
    <input role=\"yes\" type=\"button\" value=\"";
        // line 72
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_Yes")), "html", null, true);
        echo "\"/>
    <input role=\"no\" type=\"button\" value=\"";
        // line 73
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_No")), "html", null, true);
        echo "\"/>
</div>

";
        // line 76
        if ((isset($context["userIsSuperUser"]) ? $context["userIsSuperUser"] : $this->getContext($context, "userIsSuperUser"))) {
            // line 77
            echo "    <div class=\"ui-confirm\" id=\"confirmUserRemove\">
        <h2></h2>
        <input role=\"yes\" type=\"button\" value=\"";
            // line 79
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_Yes")), "html", null, true);
            echo "\"/>
        <input role=\"no\" type=\"button\" value=\"";
            // line 80
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_No")), "html", null, true);
            echo "\"/>
    </div>
    <div class=\"ui-confirm\" id=\"confirmPasswordChange\">
        <h2>";
            // line 83
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_ChangePasswordConfirm")), "html", null, true);
            echo "</h2>
        <input role=\"yes\" type=\"button\" value=\"";
            // line 84
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_Yes")), "html", null, true);
            echo "\"/>
        <input role=\"no\" type=\"button\" value=\"";
            // line 85
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_No")), "html", null, true);
            echo "\"/>
    </div>
    <br/>
    <h2>";
            // line 88
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_UsersManagement")), "html", null, true);
            echo "</h2>
    <p>";
            // line 89
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_UsersManagementMainDescription")), "html", null, true);
            echo "
        ";
            // line 90
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_ThereAreCurrentlyNRegisteredUsers", (("<b>" . (isset($context["usersCount"]) ? $context["usersCount"] : $this->getContext($context, "usersCount"))) . "</b>")));
            echo "</p>
    ";
            // line 91
            $context["ajax"] = $this->env->loadTemplate("ajaxMacros.twig");
            // line 92
            echo "    ";
            echo $context["ajax"]->geterrorDiv("ajaxErrorUsersManagement");
            echo "
    ";
            // line 93
            echo $context["ajax"]->getloadingDiv("ajaxLoadingUsersManagement");
            echo "
    <div class=\"entityContainer\" style=\"margin-bottom:50px;\">
        <table class=\"entityTable dataTable\" id=\"users\">
            <thead>
            <tr>
                <th>";
            // line 98
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_Username")), "html", null, true);
            echo "</th>
                <th>";
            // line 99
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_Password")), "html", null, true);
            echo "</th>
                <th>";
            // line 100
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_Email")), "html", null, true);
            echo "</th>
                <th>";
            // line 101
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_Alias")), "html", null, true);
            echo "</th>
                <th>token_auth</th>
                <th>";
            // line 103
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_Edit")), "html", null, true);
            echo "</th>
                <th>";
            // line 104
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_Delete")), "html", null, true);
            echo "</th>
            </tr>
            </thead>

            <tbody>
            ";
            // line 109
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["users"]) ? $context["users"] : $this->getContext($context, "users")));
            foreach ($context['_seq'] as $context["i"] => $context["user"]) {
                // line 110
                echo "                ";
                if (($this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "login") != "anonymous")) {
                    // line 111
                    echo "                    <tr class=\"editable\" id=\"row";
                    echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")), "html", null, true);
                    echo "\">
                        <td id=\"userLogin\" class=\"editable\">";
                    // line 112
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "login"), "html", null, true);
                    echo "</td>
                        <td id=\"password\" class=\"editable\">-</td>
                        <td id=\"email\" class=\"editable\">";
                    // line 114
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email"), "html", null, true);
                    echo "</td>
                        <td id=\"alias\" class=\"editable\">";
                    // line 115
                    echo $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "alias");
                    echo "</td>
                        <td id=\"token_auth\">";
                    // line 116
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "token_auth"), "html", null, true);
                    echo "</td>
                        <td>
                            <span class=\"edituser link_but\" id=\"row";
                    // line 118
                    echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")), "html", null, true);
                    echo "\">
                                <img title=\"";
                    // line 119
                    echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_Edit")), "html", null, true);
                    echo "\" src='plugins/Zeitgeist/images/ico_edit.png'/>
                                <span>";
                    // line 120
                    echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_Edit")), "html", null, true);
                    echo "</span>
                            </span>
                        </td>
                        <td>
                            <span class=\"deleteuser link_but\" id=\"row";
                    // line 124
                    echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : $this->getContext($context, "i")), "html", null, true);
                    echo "\">
                                <img title=\"";
                    // line 125
                    echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_Delete")), "html", null, true);
                    echo "\" src='plugins/Zeitgeist/images/ico_delete.png'/>
                                <span>";
                    // line 126
                    echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_Delete")), "html", null, true);
                    echo "</span>
                            </span>
                        </td>
                    </tr>
                ";
                }
                // line 131
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['i'], $context['user'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 132
            echo "            </tbody>
        </table>
        <div class=\"addrow\"><img src='plugins/UsersManager/images/add.png'/> ";
            // line 134
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("UsersManager_AddUser")), "html", null, true);
            echo "</div>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@UsersManager/index.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  359 => 134,  355 => 132,  349 => 131,  341 => 126,  337 => 125,  333 => 124,  326 => 120,  322 => 119,  318 => 118,  313 => 116,  309 => 115,  305 => 114,  300 => 112,  295 => 111,  292 => 110,  288 => 109,  280 => 104,  276 => 103,  271 => 101,  267 => 100,  263 => 99,  259 => 98,  251 => 93,  246 => 92,  244 => 91,  240 => 90,  236 => 89,  232 => 88,  226 => 85,  222 => 84,  218 => 83,  212 => 80,  208 => 79,  204 => 77,  202 => 76,  196 => 73,  192 => 72,  188 => 71,  180 => 65,  172 => 62,  162 => 60,  158 => 58,  156 => 57,  147 => 55,  139 => 54,  135 => 53,  131 => 52,  128 => 51,  123 => 50,  120 => 49,  118 => 48,  110 => 43,  106 => 42,  102 => 41,  98 => 40,  94 => 39,  89 => 36,  83 => 33,  80 => 32,  78 => 31,  72 => 28,  68 => 27,  66 => 26,  61 => 23,  58 => 15,  52 => 13,  50 => 12,  45 => 10,  40 => 8,  34 => 5,  31 => 4,  28 => 3,);
    }
}
