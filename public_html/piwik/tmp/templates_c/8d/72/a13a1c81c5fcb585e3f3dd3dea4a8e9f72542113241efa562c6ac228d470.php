<?php

/* @Dashboard/_dashboardSettings.twig */
class __TwigTemplate_8d72a13a1c81c5fcb585e3f3dd3dea4a8e9f72542113241efa562c6ac228d470 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"dashboardSettings\" class=\"js-autoLeftPanel\">
    <span>";
        // line 2
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Dashboard_WidgetsAndDashboard")), "html", null, true);
        echo "</span>
    <ul class=\"submenu\">
        <li>
            <div id=\"addWidget\">";
        // line 5
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Dashboard_AddAWidget")), "html", null, true);
        echo " &darr;</div>
            <ul class=\"widgetpreview-categorylist\"></ul>
        </li>
        <li>
            <div id=\"manageDashboard\">";
        // line 9
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Dashboard_ManageDashboard")), "html", null, true);
        echo " &darr;</div>
            <ul>
                <li onclick=\"resetDashboard();\">";
        // line 11
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Dashboard_ResetDashboard")), "html", null, true);
        echo "</li>
                <li onclick=\"showChangeDashboardLayoutDialog();\">";
        // line 12
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Dashboard_ChangeDashboardLayout")), "html", null, true);
        echo "</li>
                ";
        // line 13
        if (((isset($context["userLogin"]) ? $context["userLogin"] : $this->getContext($context, "userLogin")) && ((isset($context["userLogin"]) ? $context["userLogin"] : $this->getContext($context, "userLogin")) != "anonymous"))) {
            // line 14
            echo "                    <li onclick=\"renameDashboard();\">";
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Dashboard_RenameDashboard")), "html", null, true);
            echo "</li>
                    <li onclick=\"removeDashboard();\" id=\"removeDashboardLink\">";
            // line 15
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Dashboard_RemoveDashboard")), "html", null, true);
            echo "</li>
                    ";
            // line 16
            if ((isset($context["isSuperUser"]) ? $context["isSuperUser"] : $this->getContext($context, "isSuperUser"))) {
                // line 17
                echo "                        <li onclick=\"setAsDefaultWidgets();\">";
                echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Dashboard_SetAsDefaultWidgets")), "html", null, true);
                echo "</li>
                        <li onclick=\"copyDashboardToUser();\">";
                // line 18
                echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Dashboard_CopyDashboardToUser")), "html", null, true);
                echo "</li>
                    ";
            }
            // line 20
            echo "                ";
        }
        // line 21
        echo "            </ul>
        </li>
        ";
        // line 23
        if (((isset($context["userLogin"]) ? $context["userLogin"] : $this->getContext($context, "userLogin")) && ("anonymous" != (isset($context["userLogin"]) ? $context["userLogin"] : $this->getContext($context, "userLogin"))))) {
            // line 24
            echo "            <li onclick=\"createDashboard();\">";
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Dashboard_CreateNewDashboard")), "html", null, true);
            echo "</li>
        ";
        }
        // line 26
        echo "    </ul>
    <ul class=\"widgetpreview-widgetlist\"></ul>
    <div class=\"widgetpreview-preview\"></div>
</div>";
    }

    public function getTemplateName()
    {
        return "@Dashboard/_dashboardSettings.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 26,  80 => 24,  78 => 23,  74 => 21,  71 => 20,  61 => 17,  59 => 16,  55 => 15,  50 => 14,  44 => 12,  35 => 9,  101 => 17,  69 => 8,  63 => 6,  52 => 5,  97 => 33,  95 => 32,  91 => 31,  57 => 23,  48 => 13,  40 => 11,  22 => 2,  30 => 6,  28 => 5,  132 => 36,  127 => 34,  124 => 33,  121 => 32,  113 => 30,  108 => 28,  104 => 36,  100 => 26,  96 => 25,  92 => 16,  88 => 30,  84 => 22,  79 => 21,  77 => 11,  72 => 19,  70 => 15,  62 => 25,  54 => 11,  46 => 9,  38 => 2,  31 => 6,  27 => 1,  23 => 3,  39 => 14,  25 => 6,  66 => 18,  64 => 17,  60 => 16,  58 => 15,  53 => 12,  42 => 15,  37 => 13,  33 => 7,  29 => 5,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
