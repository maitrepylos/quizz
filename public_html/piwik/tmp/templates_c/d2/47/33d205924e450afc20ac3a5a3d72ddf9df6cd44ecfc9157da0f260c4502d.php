<?php

/* @CoreHome/_headerMessage.twig */
class __TwigTemplate_d24733d205924e450afc20ac3a5a3d72ddf9df6cd44ecfc9157da0f260c4502d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 2
        $context["test_latest_version_available"] = "3.0";
        // line 3
        $context["test_piwikUrl"] = "http://demo.piwik.org/";
        // line 4
        ob_start();
        echo twig_escape_filter($this->env, (((isset($context["piwikUrl"]) ? $context["piwikUrl"] : $this->getContext($context, "piwikUrl")) == "http://demo.piwik.org/") || ((isset($context["piwikUrl"]) ? $context["piwikUrl"] : $this->getContext($context, "piwikUrl")) == "https://demo.piwik.org/")), "html", null, true);
        $context["isPiwikDemo"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 5
        echo "
<span id=\"header_message\" class=\"";
        // line 6
        if (((isset($context["isPiwikDemo"]) ? $context["isPiwikDemo"] : $this->getContext($context, "isPiwikDemo")) || (!(isset($context["latest_version_available"]) ? $context["latest_version_available"] : $this->getContext($context, "latest_version_available"))))) {
            echo "header_info";
        } else {
            echo "header_alert";
        }
        echo "\">
    <span class=\"header_short\">
        ";
        // line 8
        if ((isset($context["isPiwikDemo"]) ? $context["isPiwikDemo"] : $this->getContext($context, "isPiwikDemo"))) {
            // line 9
            echo "            ";
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_YouAreViewingDemoShortMessage")), "html", null, true);
            echo "
        ";
        } elseif ((isset($context["latest_version_available"]) ? $context["latest_version_available"] : $this->getContext($context, "latest_version_available"))) {
            // line 11
            echo "            ";
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_NewUpdatePiwikX", (isset($context["latest_version_available"]) ? $context["latest_version_available"] : $this->getContext($context, "latest_version_available")))), "html", null, true);
            echo "
        ";
        } else {
            // line 13
            echo "            ";
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_AboutPiwikX", (isset($context["piwik_version"]) ? $context["piwik_version"] : $this->getContext($context, "piwik_version")))), "html", null, true);
            echo "
        ";
        }
        // line 15
        echo "    </span>

    <span class=\"header_full\">
        ";
        // line 18
        if ((isset($context["isPiwikDemo"]) ? $context["isPiwikDemo"] : $this->getContext($context, "isPiwikDemo"))) {
            // line 19
            echo "            ";
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_YouAreViewingDemoShortMessage")), "html", null, true);
            echo "
            <br/>
            ";
            // line 21
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_DownloadFullVersion", "<a href='http://piwik.org/'>", "</a>", "<a href='http://piwik.org'>piwik.org</a>"));
            echo "
            <br/>
        ";
        }
        // line 24
        echo "        ";
        if ((isset($context["latest_version_available"]) ? $context["latest_version_available"] : $this->getContext($context, "latest_version_available"))) {
            // line 25
            echo "            ";
            if ((isset($context["isSuperUser"]) ? $context["isSuperUser"] : $this->getContext($context, "isSuperUser"))) {
                // line 26
                echo "                ";
                echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_PiwikXIsAvailablePleaseUpdateNow", (isset($context["latest_version_available"]) ? $context["latest_version_available"] : $this->getContext($context, "latest_version_available")), "<br /><a href='index.php?module=CoreUpdater&amp;action=newVersionAvailable'>", "</a>", "<a href='?module=Proxy&amp;action=redirect&amp;url=http://piwik.org/changelog/' target='_blank'>", "</a>"));
                echo "
                <br/>
                ";
                // line 28
                echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_YouAreCurrentlyUsing", (isset($context["piwik_version"]) ? $context["piwik_version"] : $this->getContext($context, "piwik_version")))), "html", null, true);
                echo "
            ";
            } elseif ((!(isset($context["isPiwikDemo"]) ? $context["isPiwikDemo"] : $this->getContext($context, "isPiwikDemo")))) {
                // line 30
                echo "                ";
                echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_PiwikXIsAvailablePleaseNotifyPiwikAdmin", "<a href='?module=Proxy&action=redirect&url=http://piwik.org/' target='_blank'>Piwik</a> <a href='?module=Proxy&action=redirect&url=http://piwik.org/changelog/' target='_blank'>{{ latest_version_available }}</a>"));
                echo "
            ";
            }
            // line 32
            echo "        ";
        } elseif ((!(isset($context["isPiwikDemo"]) ? $context["isPiwikDemo"] : $this->getContext($context, "isPiwikDemo")))) {
            // line 33
            echo "            ";
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_PiwikIsACollaborativeProjectYouCanContributeAndDonate", "<a href='?module=Proxy&action=redirect&url=http://piwik.org' target='_blank'>", ((isset($context["piwik_version"]) ? $context["piwik_version"] : $this->getContext($context, "piwik_version")) . "</a>"), "<br />", "<a target='_blank' href='?module=Proxy&action=redirect&url=http://piwik.org/contribute/'>", "</a>", "<br/>", "<a href='http://piwik.org/donate/' target='_blank'><strong><em>", "</em></strong></a>"));
            // line 42
            echo "
        ";
        }
        // line 44
        echo "        ";
        if ((isset($context["hasSomeAdminAccess"]) ? $context["hasSomeAdminAccess"] : $this->getContext($context, "hasSomeAdminAccess"))) {
            // line 45
            echo "            <br/>
            <div id=\"updateCheckLinkContainer\">
                <span class='loadingPiwik' style=\"display:none;\"><img src='plugins/Zeitgeist/images/loading-blue.gif'/></span>
                <img src=\"plugins/Zeitgeist/images/reload.png\"/>
                <a href=\"#\" id=\"checkForUpdates\"><em>";
            // line 49
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("CoreHome_CheckForUpdates")), "html", null, true);
            echo "</em></a>
            </div>
        ";
        }
        // line 52
        echo "    </span>
</span>
";
    }

    public function getTemplateName()
    {
        return "@CoreHome/_headerMessage.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 52,  120 => 49,  111 => 44,  104 => 33,  101 => 32,  95 => 30,  78 => 24,  64 => 18,  58 => 16,  77 => 21,  23 => 4,  165 => 64,  153 => 62,  151 => 61,  136 => 52,  130 => 50,  128 => 49,  113 => 44,  106 => 43,  102 => 42,  98 => 40,  92 => 38,  79 => 33,  71 => 32,  62 => 29,  44 => 9,  28 => 4,  35 => 6,  26 => 4,  90 => 28,  82 => 22,  69 => 18,  39 => 8,  63 => 17,  49 => 27,  33 => 6,  31 => 6,  59 => 15,  52 => 12,  48 => 12,  27 => 5,  24 => 3,  21 => 3,  19 => 2,  134 => 43,  131 => 42,  127 => 35,  124 => 47,  121 => 33,  114 => 45,  112 => 19,  107 => 42,  105 => 15,  100 => 13,  87 => 9,  84 => 26,  81 => 25,  74 => 20,  72 => 21,  68 => 44,  66 => 19,  60 => 12,  55 => 36,  53 => 13,  50 => 9,  41 => 9,  38 => 8,  36 => 7,  32 => 5,  30 => 6,  22 => 2,  56 => 13,  54 => 15,  51 => 11,  47 => 11,  45 => 9,  42 => 9,  40 => 5,  37 => 10,  34 => 3,  29 => 4,);
    }
}
