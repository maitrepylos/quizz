<?php

/* _jsGlobalVariables.twig */
class __TwigTemplate_1b4643984d8ba9b5e39392f7a456d62a570b9ac7dc11384e77b1fb9e49ccd123 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\">
    var piwik = {};
    piwik.token_auth = \"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["token_auth"]) ? $context["token_auth"] : $this->getContext($context, "token_auth")), "html", null, true);
        echo "\";
    piwik.piwik_url = \"";
        // line 4
        echo twig_escape_filter($this->env, (isset($context["piwikUrl"]) ? $context["piwikUrl"] : $this->getContext($context, "piwikUrl")), "html", null, true);
        echo "\";
    ";
        // line 5
        if ((isset($context["userLogin"]) ? $context["userLogin"] : $this->getContext($context, "userLogin"))) {
            echo "piwik.userLogin = \"";
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, (isset($context["userLogin"]) ? $context["userLogin"] : $this->getContext($context, "userLogin")), "js"), "html", null, true);
            echo "\";
    ";
        }
        // line 7
        echo "    ";
        if (array_key_exists("idSite", $context)) {
            echo "piwik.idSite = \"";
            echo twig_escape_filter($this->env, (isset($context["idSite"]) ? $context["idSite"] : $this->getContext($context, "idSite")), "html", null, true);
            echo "\";
    ";
        }
        // line 9
        echo "    ";
        if (array_key_exists("siteName", $context)) {
            echo "piwik.siteName = \"";
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, (isset($context["siteName"]) ? $context["siteName"] : $this->getContext($context, "siteName")), "js"), "html", null, true);
            echo "\";
    ";
        }
        // line 11
        echo "    ";
        if (array_key_exists("siteMainUrl", $context)) {
            echo "piwik.siteMainUrl = \"";
            echo twig_escape_filter($this->env, twig_escape_filter($this->env, (isset($context["siteMainUrl"]) ? $context["siteMainUrl"] : $this->getContext($context, "siteMainUrl")), "js"), "html", null, true);
            echo "\";
    ";
        }
        // line 13
        echo "    ";
        if (array_key_exists("period", $context)) {
            echo "piwik.period = \"";
            echo twig_escape_filter($this->env, (isset($context["period"]) ? $context["period"] : $this->getContext($context, "period")), "html", null, true);
            echo "\";
    ";
        }
        // line 15
        echo "    ";
        // line 19
        echo "    piwik.currentDateString = \"";
        echo twig_escape_filter($this->env, ((array_key_exists("date", $context)) ? (_twig_default_filter((isset($context["date"]) ? $context["date"] : $this->getContext($context, "date")), ((array_key_exists("endDate", $context)) ? (_twig_default_filter((isset($context["endDate"]) ? $context["endDate"] : $this->getContext($context, "endDate")), "")) : ("")))) : (((array_key_exists("endDate", $context)) ? (_twig_default_filter((isset($context["endDate"]) ? $context["endDate"] : $this->getContext($context, "endDate")), "")) : ("")))), "html", null, true);
        echo "\";
    ";
        // line 20
        if (array_key_exists("startDate", $context)) {
            // line 21
            echo "    piwik.startDateString = \"";
            echo twig_escape_filter($this->env, (isset($context["startDate"]) ? $context["startDate"] : $this->getContext($context, "startDate")), "html", null, true);
            echo "\";
    piwik.endDateString = \"";
            // line 22
            echo twig_escape_filter($this->env, (isset($context["endDate"]) ? $context["endDate"] : $this->getContext($context, "endDate")), "html", null, true);
            echo "\";
    piwik.minDateYear = ";
            // line 23
            echo twig_escape_filter($this->env, (isset($context["minDateYear"]) ? $context["minDateYear"] : $this->getContext($context, "minDateYear")), "html", null, true);
            echo ";
    piwik.minDateMonth = parseInt(\"";
            // line 24
            echo twig_escape_filter($this->env, (isset($context["minDateMonth"]) ? $context["minDateMonth"] : $this->getContext($context, "minDateMonth")), "html", null, true);
            echo "\", 10);
    piwik.minDateDay = parseInt(\"";
            // line 25
            echo twig_escape_filter($this->env, (isset($context["minDateDay"]) ? $context["minDateDay"] : $this->getContext($context, "minDateDay")), "html", null, true);
            echo "\", 10);
    piwik.maxDateYear = ";
            // line 26
            echo twig_escape_filter($this->env, (isset($context["maxDateYear"]) ? $context["maxDateYear"] : $this->getContext($context, "maxDateYear")), "html", null, true);
            echo ";
    piwik.maxDateMonth = parseInt(\"";
            // line 27
            echo twig_escape_filter($this->env, (isset($context["maxDateMonth"]) ? $context["maxDateMonth"] : $this->getContext($context, "maxDateMonth")), "html", null, true);
            echo "\", 10);
    piwik.maxDateDay = parseInt(\"";
            // line 28
            echo twig_escape_filter($this->env, (isset($context["maxDateDay"]) ? $context["maxDateDay"] : $this->getContext($context, "maxDateDay")), "html", null, true);
            echo "\", 10);
    ";
        }
        // line 30
        echo "    ";
        if (array_key_exists("language", $context)) {
            echo "piwik.language = \"";
            echo twig_escape_filter($this->env, (isset($context["language"]) ? $context["language"] : $this->getContext($context, "language")), "html", null, true);
            echo "\";
    ";
        }
        // line 32
        echo "    ";
        if (array_key_exists("config_action_url_category_delimiter", $context)) {
            // line 33
            echo "    piwik.config = {};
    piwik.config.action_url_category_delimiter = \"";
            // line 34
            echo twig_escape_filter($this->env, (isset($context["config_action_url_category_delimiter"]) ? $context["config_action_url_category_delimiter"] : $this->getContext($context, "config_action_url_category_delimiter")), "html", null, true);
            echo "\";
    ";
        }
        // line 36
        echo "</script>
";
    }

    public function getTemplateName()
    {
        return "_jsGlobalVariables.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 36,  127 => 34,  124 => 33,  121 => 32,  113 => 30,  108 => 28,  104 => 27,  100 => 26,  96 => 25,  92 => 24,  88 => 23,  84 => 22,  79 => 21,  77 => 20,  72 => 19,  70 => 15,  62 => 13,  54 => 11,  46 => 9,  38 => 7,  31 => 5,  27 => 4,  23 => 3,  39 => 14,  25 => 6,  66 => 18,  64 => 17,  60 => 16,  58 => 15,  53 => 12,  42 => 15,  37 => 13,  33 => 7,  29 => 5,  26 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }
}
