<?php

/* @CoreHome/_javaScriptDisabled.twig */
class __TwigTemplate_9ff48d3bdd2886c3c174343797d8ac7b3f02d4e28a8cda1f382202262d448fd9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<noscript>
    <div id=\"javascriptDisabled\">";
        // line 2
        echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("CoreHome_JavascriptDisabled", "<a href=\"\">", "</a>"));
        echo "</div>
</noscript>
";
    }

    public function getTemplateName()
    {
        return "@CoreHome/_javaScriptDisabled.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,  134 => 43,  131 => 42,  127 => 35,  124 => 34,  121 => 33,  114 => 20,  112 => 19,  107 => 16,  105 => 15,  100 => 13,  87 => 9,  84 => 8,  81 => 7,  74 => 47,  72 => 46,  68 => 44,  66 => 42,  60 => 39,  55 => 36,  53 => 33,  50 => 32,  41 => 28,  38 => 27,  36 => 26,  32 => 24,  30 => 7,  22 => 2,  56 => 12,  54 => 11,  51 => 10,  47 => 31,  45 => 30,  42 => 6,  40 => 5,  37 => 4,  34 => 3,  29 => 2,);
    }
}
