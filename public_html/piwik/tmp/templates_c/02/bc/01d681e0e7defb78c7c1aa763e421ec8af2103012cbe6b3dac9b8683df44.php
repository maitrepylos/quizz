<?php

/* @VisitsSummary/getSparklines.twig */
class __TwigTemplate_02bc01d681e0e7defb78c7c1aa763e421ec8af2103012cbe6b3dac9b8683df44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        $this->env->loadTemplate("@VisitsSummary/_sparklines.twig")->display($context);
    }

    public function getTemplateName()
    {
        return "@VisitsSummary/getSparklines.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
