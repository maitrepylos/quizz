<?php

/* @SEO/getRank.twig */
class __TwigTemplate_35ca90004f46b50c65f1399d4420f7cc3d6839b62dac7dca56b575e240f034a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id='SeoRanks'>
    <script type=\"text/javascript\" src=\"plugins/SEO/javascripts/rank.js\"></script>

    <form method=\"post\" style=\"padding: 8px;\">
        <div align=\"left\" class=\"mediumtext\">
            ";
        // line 6
        echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("Installation_SetupWebSiteURL"))), "html", null, true);
        echo "
            <input type=\"text\" id=\"seoUrl\" size=\"15\" value=\"";
        // line 7
        echo twig_escape_filter($this->env, (isset($context["urlToRank"]) ? $context["urlToRank"] : $this->getContext($context, "urlToRank")), "html", null, true);
        echo "\" class=\"textbox\"/>
\t\t  <span style=\"padding-left:2px;\"> 
\t\t  <input type=\"submit\" id=\"rankbutton\" value=\"";
        // line 9
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("SEO_Rank")), "html", null, true);
        echo "\"/>
\t\t  </span>
        </div>

        ";
        // line 13
        $context["ajax"] = $this->env->loadTemplate("ajaxMacros.twig");
        // line 14
        echo "        ";
        echo $context["ajax"]->getLoadingDiv("ajaxLoadingSEO");
        echo "

        <div id=\"rankStats\" align=\"left\" style=\"margin-top:10px;\">
            ";
        // line 17
        if (twig_test_empty((isset($context["ranks"]) ? $context["ranks"] : $this->getContext($context, "ranks")))) {
            // line 18
            echo "                ";
            echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_Error")), "html", null, true);
            echo "
            ";
        } else {
            // line 20
            echo "                ";
            ob_start();
            // line 21
            echo "                    <a href=\"http://";
            echo twig_escape_filter($this->env, (isset($context["urlToRank"]) ? $context["urlToRank"] : $this->getContext($context, "urlToRank")), "html", null, true);
            echo "\" target=\"_blank\">";
            echo twig_escape_filter($this->env, (isset($context["urlToRank"]) ? $context["urlToRank"] : $this->getContext($context, "urlToRank")), "html", null, true);
            echo "</a>
                ";
            $context["cleanUrl"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
            // line 23
            echo "                ";
            echo call_user_func_array($this->env->getFilter('translate')->getCallable(), array("SEO_SEORankingsFor", (isset($context["cleanUrl"]) ? $context["cleanUrl"] : $this->getContext($context, "cleanUrl"))));
            echo "
                <table cellspacing=\"2\" style=\"margin:auto;line-height:1.5em;padding-top:10px;\">
                    ";
            // line 25
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["ranks"]) ? $context["ranks"] : $this->getContext($context, "ranks")));
            foreach ($context['_seq'] as $context["_key"] => $context["rank"]) {
                // line 26
                echo "                        <tr>
";
                // line 27
                ob_start();
                if ($this->getAttribute((isset($context["rank"]) ? $context["rank"] : null), "logo_link", array(), "any", true, true)) {
                    echo "<a class=\"linkContent\" href=\"?module=Proxy&action=redirect&url=";
                    echo twig_escape_filter($this->env, twig_urlencode_filter($this->getAttribute((isset($context["rank"]) ? $context["rank"] : $this->getContext($context, "rank")), "logo_link")), "html", null, true);
                    echo "\"
                                                    target=\"_blank\"
                         ";
                    // line 29
                    if ((!twig_test_empty($this->getAttribute((isset($context["rank"]) ? $context["rank"] : $this->getContext($context, "rank")), "logo_tooltip")))) {
                        echo "title=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["rank"]) ? $context["rank"] : $this->getContext($context, "rank")), "logo_tooltip"), "html", null, true);
                        echo "\"";
                    }
                    echo ">";
                }
                $context["seoLink"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
                // line 30
                echo "                            ";
                ob_start();
                echo twig_escape_filter($this->env, (isset($context["seoLink"]) ? $context["seoLink"] : $this->getContext($context, "seoLink")), "html", null, true);
                echo "Majestic</a>";
                $context["majesticLink"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
                // line 31
                echo "                            <td>";
                if ($this->getAttribute((isset($context["rank"]) ? $context["rank"] : null), "logo_link", array(), "any", true, true)) {
                    echo (isset($context["seoLink"]) ? $context["seoLink"] : $this->getContext($context, "seoLink"));
                }
                echo "<img
                                            style='vertical-align:middle;margin-right:6px;' src='";
                // line 32
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["rank"]) ? $context["rank"] : $this->getContext($context, "rank")), "logo"), "html", null, true);
                echo "' border='0'
                                            alt=\"";
                // line 33
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["rank"]) ? $context["rank"] : $this->getContext($context, "rank")), "label"), "html", null, true);
                echo "\">";
                if ($this->getAttribute((isset($context["rank"]) ? $context["rank"] : null), "logo_link", array(), "any", true, true)) {
                    echo "</a>";
                }
                echo " ";
                echo strtr($this->getAttribute((isset($context["rank"]) ? $context["rank"] : $this->getContext($context, "rank")), "label"), array("Majestic" => (isset($context["majesticLink"]) ? $context["majesticLink"] : $this->getContext($context, "majesticLink"))));
                // line 34
                echo "
                            </td>
                            <td>
                                <div style=\"margin-left:15px;\">
                                ";
                // line 38
                if ($this->getAttribute((isset($context["rank"]) ? $context["rank"] : null), "logo_link", array(), "any", true, true)) {
                    echo (isset($context["seoLink"]) ? $context["seoLink"] : $this->getContext($context, "seoLink"));
                }
                // line 39
                echo "                                    ";
                if ($this->getAttribute((isset($context["rank"]) ? $context["rank"] : $this->getContext($context, "rank")), "rank")) {
                    echo $this->getAttribute((isset($context["rank"]) ? $context["rank"] : $this->getContext($context, "rank")), "rank");
                } else {
                    echo "-";
                }
                // line 40
                echo "                                    ";
                if (($this->getAttribute((isset($context["rank"]) ? $context["rank"] : $this->getContext($context, "rank")), "id") == "pagerank")) {
                    echo " /10
                                    ";
                } elseif ((($this->getAttribute((isset($context["rank"]) ? $context["rank"] : $this->getContext($context, "rank")), "id") == "google-index") || ($this->getAttribute((isset($context["rank"]) ? $context["rank"] : $this->getContext($context, "rank")), "id") == "bing-index"))) {
                    // line 41
                    echo " ";
                    echo twig_escape_filter($this->env, call_user_func_array($this->env->getFilter('translate')->getCallable(), array("General_Pages")), "html", null, true);
                    echo "
                                    ";
                }
                // line 43
                echo "                                ";
                if ($this->getAttribute((isset($context["rank"]) ? $context["rank"] : null), "logo_link", array(), "any", true, true)) {
                    echo "</a>";
                }
                // line 44
                echo "                                </div>
                            </td>
                        </tr>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rank'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 48
            echo "
                </table>
            ";
        }
        // line 51
        echo "        </div>
    </form>
</div>
";
    }

    public function getTemplateName()
    {
        return "@SEO/getRank.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 51,  168 => 48,  159 => 44,  154 => 43,  148 => 41,  142 => 40,  135 => 39,  131 => 38,  125 => 34,  117 => 33,  113 => 32,  106 => 31,  100 => 30,  91 => 29,  83 => 27,  80 => 26,  76 => 25,  70 => 23,  62 => 21,  59 => 20,  53 => 18,  51 => 17,  44 => 14,  42 => 13,  35 => 9,  30 => 7,  26 => 6,  19 => 1,);
    }
}
