<?php $iframeHeight = 1500; ?>
<!DOCTYPE html>
<html>
<head>
    <title>Financez votre projet</title>
</head>
<style type="text/css">
    /* http://meyerweb.com/eric/tools/css/reset/
   v2.0 | 20110126
   License: none (public domain)
*/

    html, body, div, span, applet, object, iframe,
    h1, h2, h3, h4, h5, h6, p, blockquote, pre,
    a, abbr, acronym, address, big, cite, code,
    del, dfn, em, img, ins, kbd, q, s, samp,
    small, strike, strong, sub, sup, tt, var,
    b, u, i, center,
    dl, dt, dd, ol, ul, li,
    fieldset, form, label, legend,
    table, caption, tbody, tfoot, thead, tr, th, td,
    article, aside, canvas, details, embed,
    figure, figcaption, footer, header, hgroup,
    menu, nav, output, ruby, section, summary,
    time, mark, audio, video {
        margin: 0;
        padding: 0;
        border: 0;
        font-size: 100%;
        font: inherit;
        vertical-align: baseline;
    }
    /* HTML5 display-role reset for older browsers */
    article, aside, details, figcaption, figure,
    footer, header, hgroup, menu, nav, section {
        display: block;
    }
    body {
        line-height: 1;
    }
    ol, ul {
        list-style: none;
    }
    blockquote, q {
        quotes: none;
    }
    blockquote:before, blockquote:after,
    q:before, q:after {
        content: '';
        content: none;
    }
    table {
        border-collapse: collapse;
        border-spacing: 0;
    }



    body {
        position: relative;
        background: url(images/placeholder_bg_repeat.jpg) repeat-y center top;
    }

    header {
        height: 408px;
        background: url(images/placeholder_header.jpg) no-repeat center top;
    }

    div#content {
        height: <?php echo $iframeHeight - 208; ?>px;
    }

    footer {
        height: 130px;
        background: url(images/placeholder_footer.jpg) no-repeat center bottom;
    }

    iframe {
        position: absolute;
        top: 200px;
        left: 50%;
        margin-left: -430px;
        max-width: 600px;
    }
</style>
<body>
<header>

</header>

<div id="content">
    <iframe src="/financez-votre-projet?colors=EDEDED|A6A6A6|FF9214|C5D636|FF9214" frameborder="0" width="600" height="<?php echo $iframeHeight; ?>"></iframe>
</div>

<footer>

</footer>
</body>
</html>