<?php
/**
 * AST quiz - innovons.be
 * Created by PAPERPIXEL
 * www.paperpixel.net
 */
use Silex\Application;
use Symfony\Component\Debug\Debug;



$loader = require_once __DIR__.'/../App/vendor/autoload.php';
if(!($loader instanceof Composer\Autoload\ClassLoader)) die('problem while instanciating autoloading');
$loader->add('App', dirname(__DIR__.'/../App/'));

$app = require_once __DIR__.'/../App/bootstrap.php';
// Debug::enable();
$app["debug"] = true;
if ( $app['debug'] ) {
    ini_set('error_reporting',E_ALL);
    ini_set('dsplay_error',"stdout");
    error_reporting(E_ALL);
}
/*
$app->error(function(Exception $e){
    echo $e->getMessage();
});
*/
$app->run();