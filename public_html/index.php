<?php
/**
 * AST quiz - innovons.be
 * Created by PAPERPIXEL
 * www.paperpixel.net
 */
use Silex\Application;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

$loader = require_once __DIR__.'/../App/vendor/autoload.php';
if(!($loader instanceof Composer\Autoload\ClassLoader)) die('problem while instanciating autoloading');
$loader->add('App', dirname(__DIR__.'/../App/'));

$app = require_once __DIR__.'/../App/bootstrap.php';


$app->error(
    function (Exception $e, $code) use ($app) {
        if ($e instanceof NotFoundHttpException) {
            return new \Symfony\Component\HttpFoundation\Response(
                $app['twig']->render('common\404.html.twig', array('code' => 404, 'template_name' => 'error')),
                '404'
            );

        }elseif( $e instanceof \Symfony\Component\HttpKernel\Exception\HttpException ){
            $code = $e->getStatusCode();
            return $app['twig']->render('common\404.html.twig', array('code' => $code, 'template_name' => 'error'));
        }else{
            $code = $app['debug'] ? $e->getMessage()."<br />".$e->getFile()."<br />".$e->getLine() : $e->getCode();
            return $app['twig']->render('common\404.html.twig', array('code'=>$code, 'template_name'=>'error'));
        }
    }
);

$app->run();